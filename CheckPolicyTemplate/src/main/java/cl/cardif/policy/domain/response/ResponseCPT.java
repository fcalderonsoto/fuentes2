package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import cl.cardif.policy.domain.response.CheckPolicyTemplateRSP;


public class ResponseCPT {

  @JsonProperty("check_policy_trace_RSP")
  private CheckPolicyTemplateRSP checkPolicyTemplateRSP;

  public ResponseCPT(CheckPolicyTemplateRSP checkPolicyTemplateRSP) {
	    this.checkPolicyTemplateRSP = checkPolicyTemplateRSP;
	  }

	public CheckPolicyTemplateRSP getCheckPolicyTemplateRSP() {
		return checkPolicyTemplateRSP;
	}

}
