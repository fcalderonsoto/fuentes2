package cl.cardif.policy.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckPolicyTemplateREQ {

  @JsonProperty("policyCode")
  private String policyCode;

  @JsonProperty("productCode")
  private String productCode;

  @JsonProperty("planCode")
  private String planCode;

  public String getPolicyCode() {
    return policyCode;
  }

  public String getProductCode() {
    return productCode;
  }

  public String getPlanCode() {
    return planCode;
  }
}
