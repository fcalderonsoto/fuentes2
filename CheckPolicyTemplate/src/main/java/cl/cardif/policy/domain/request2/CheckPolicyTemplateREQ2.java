package cl.cardif.policy.domain.request2;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckPolicyTemplateREQ2 {

  @JsonProperty("policyCode")
  private String policyCode;

  @JsonProperty("productCode")
  private String productCode;

  @JsonProperty("planCode")
  private String planCode;
  
  @JsonProperty("nombrePlantilla")
  private String nombrePlantilla;

  public String getPolicyCode() {
    return policyCode;
  }

  public String getProductCode() {
    return productCode;
  }

  public String getPlanCode() {
    return planCode;
  }
  
  public String getNombrePlantilla() {
	    return nombrePlantilla;
	  }
}
