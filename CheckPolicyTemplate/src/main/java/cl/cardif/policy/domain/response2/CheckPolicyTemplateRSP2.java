package cl.cardif.policy.domain.response2;

import com.fasterxml.jackson.annotation.JsonProperty;

import cl.cardif.policy.domain.response.Message;

public class CheckPolicyTemplateRSP2 {
	 @JsonProperty("Message")
	  private Message message;
	  @JsonProperty("b64Code")
	  private B64Code b64Code;
	  

	  public CheckPolicyTemplateRSP2(
			  final Message message,
			  final B64Code b64Code) {
		  this.message = message;
	    this.b64Code = b64Code;
	  }
	}