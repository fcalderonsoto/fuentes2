package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckPolicyTemplateRSP {
	  @JsonProperty("Message")
	  private Message message;
	  
	  @JsonProperty("Template")
	  private Template template;

	  public CheckPolicyTemplateRSP(
			  final Message message,
			  final Template template) {
	    this.message = message;
	    this.template = template;
	  }
	}
