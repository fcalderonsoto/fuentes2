package cl.cardif.policy.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestCPT {
  @JsonProperty("check_policy_template_REQ")
  private CheckPolicyTemplateREQ CheckPolicyTemplateREQ;

  public CheckPolicyTemplateREQ getCheckPolicyTemplateREQ() {
    return CheckPolicyTemplateREQ;
  }
}
