package cl.cardif.policy.domain.response2;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseCPT2 {

  @JsonProperty("check_policy_trace_RSP")
  private CheckPolicyTemplateRSP2 checkPolicyTemplateRSP2;

  public ResponseCPT2(CheckPolicyTemplateRSP2 checkPolicyTemplateRSP2) {
	    this.checkPolicyTemplateRSP2 = checkPolicyTemplateRSP2;
	  }

	public CheckPolicyTemplateRSP2 getCheckPolicyTemplateRSP2() {
		return checkPolicyTemplateRSP2;
	}

}
