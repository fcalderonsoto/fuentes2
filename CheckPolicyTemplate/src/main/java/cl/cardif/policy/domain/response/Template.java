package cl.cardif.policy.domain.response;

public class Template {

  private String documentCode;
  private String document;

  public Template(String documentCode, String document) {
    this.documentCode = documentCode;
    this.document = document;
  }

  public String getDocumentCode() {
    return documentCode;
  }

  public String getDocument() {
    return document;
  }
}
