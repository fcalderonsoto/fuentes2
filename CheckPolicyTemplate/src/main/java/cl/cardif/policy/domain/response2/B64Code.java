package cl.cardif.policy.domain.response2;

//import com.fasterxml.jackson.annotation.JsonProperty;

public class B64Code {
	  //@JsonProperty("b64Code")
	  private String b64Code;
	  
	  public B64Code(String b64Code) {
		  this.b64Code= b64Code;
	  }

	public String getB64Code() {
		return b64Code;
	}
	  
}
