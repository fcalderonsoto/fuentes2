package cl.cardif.policy.domain.request2;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestCPT2 {
  @JsonProperty("check_policy_template_REQ")
  private CheckPolicyTemplateREQ2 CheckPolicyTemplateREQ2;

  public CheckPolicyTemplateREQ2 getCheckPolicyTemplateREQ2() {
    return CheckPolicyTemplateREQ2;
  }
}
