package cl.cardif.policy.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.cardif.policy.dao.CPTInsertDao;
import cl.cardif.policy.domain.request.RequestCPT;
import cl.cardif.policy.domain.response.Message;
import cl.cardif.policy.domain.response.CheckPolicyTemplateRSP;
import cl.cardif.policy.domain.response.Template;
import cl.cardif.policy.domain.response.ResponseCPT;
import cl.cardif.policy.exception.CPTException;
import cl.cardif.policy.service.CPTService;
import cl.cardif.policy.tools.CPTMessages;
import cl.cardif.policy.tools.CPTTools;

@Service
public class CPTServiceImpl implements CPTService {

  private static Logger logger = LoggerFactory.getLogger(CPTServiceImpl.class);

  @Autowired
  CPTTools CPTTools;

  @Autowired
  CPTInsertDao CPTInsertDao;

  @Override
  public ResponseCPT getTemplate(RequestCPT requestCPT) {
    try {
    	Template template;
    	if (CPTTools.mandatoryInput(requestCPT)) {
    		template = CPTInsertDao.getTemplate(requestCPT.getCheckPolicyTemplateREQ());
   			return new ResponseCPT(new CheckPolicyTemplateRSP(new Message(CPTMessages.OK, 10), template));
      } else {
        logger.error(CPTMessages.ERROR_MANDATORY);
        return new ResponseCPT(new CheckPolicyTemplateRSP(new Message(CPTMessages.ERROR_MANDATORY, 11), null));
      }
    } catch (CPTException e) {
      logger.error(CPTMessages.ERROR_STORE_PROCEDURE + e.getMessage());
      return new ResponseCPT(new CheckPolicyTemplateRSP(new Message(CPTMessages.ERROR_STORE_PROCEDURE + e.getMessage(), 12), null));
    }
  }
}
