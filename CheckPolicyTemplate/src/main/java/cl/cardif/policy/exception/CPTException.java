package cl.cardif.policy.exception;

import org.springframework.http.HttpStatus;

public class CPTException extends RuntimeException {

  private static final long serialVersionUID = 1L;
  private final HttpStatus httpStatus;
  private String message;
  private Exception exception;

  public CPTException(String message, HttpStatus httpStatus, Exception exception) {
    super();
    this.httpStatus = httpStatus;
    this.exception = exception;
    this.setMessage(message);
  }

  public HttpStatus getHttpStatus() {
    return httpStatus;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Exception getException() {
    return exception;
  }
}
