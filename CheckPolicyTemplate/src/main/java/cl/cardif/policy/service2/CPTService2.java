package cl.cardif.policy.service2;

import java.io.UnsupportedEncodingException;

import cl.cardif.policy.domain.request2.RequestCPT2;
import cl.cardif.policy.domain.response2.ResponseCPT2;

public interface CPTService2 {

  ResponseCPT2 getTemplate2(RequestCPT2 requestCPT2) throws UnsupportedEncodingException;
}
