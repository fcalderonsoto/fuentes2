package cl.cardif.policy.service2.impl2;

import java.io.UnsupportedEncodingException;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.ibm.xml.crypto.util.Base64;

import cl.cardif.policy.dao2.CPTInsertDao2;
import cl.cardif.policy.domain.request2.CheckPolicyTemplateREQ2;
import cl.cardif.policy.domain.request2.RequestCPT2;
import cl.cardif.policy.domain.response.Message;
import cl.cardif.policy.domain.response2.B64Code;
import cl.cardif.policy.domain.response2.CheckPolicyTemplateRSP2;
import cl.cardif.policy.domain.response2.ResponseCPT2;
import cl.cardif.policy.exception.CPTException;
//import cl.cardif.policy.exception.CPTException;
import cl.cardif.policy.service2.CPTService2;
import cl.cardif.policy.tools.CPTMessages;
import cl.cardif.policy.tools.CPTTools;

@Service
public class CPTServiceImpl2 implements CPTService2 {

  private static Logger logger = LoggerFactory.getLogger(CPTServiceImpl2.class);

  @Autowired
  CPTTools CPTTools;

  @Autowired
  CPTInsertDao2 CPTInsertDao2;

  @Override
  public ResponseCPT2 getTemplate2(RequestCPT2 requestCPT2) throws UnsupportedEncodingException{
    try {
   	if (CPTTools.mandatoryInput2(requestCPT2)) {
   		String tmpNotFound="Template not found";
   		    String template = modifyTemplate(requestCPT2.getCheckPolicyTemplateREQ2().getPolicyCode(),
   		    		   			requestCPT2.getCheckPolicyTemplateREQ2().getProductCode(),requestCPT2.getCheckPolicyTemplateREQ2().getPlanCode(),requestCPT2.getCheckPolicyTemplateREQ2());
   		    if(template.equals(tmpNotFound)) {
   		     logger.error(CPTMessages.ERROR_STORE_PROCEDURE + tmpNotFound);
   		     return new ResponseCPT2(new CheckPolicyTemplateRSP2(new Message(CPTMessages.ERROR_STORE_PROCEDURE + tmpNotFound, 12), null));
    		}else {
    			return new ResponseCPT2(new CheckPolicyTemplateRSP2(new Message(CPTMessages.OK, 10), new B64Code(Base64.getEncoder().encodeToString(template.getBytes("utf-8")))));
    		}
   		    
     } else {
        logger.error(CPTMessages.ERROR_MANDATORY);
        return new ResponseCPT2(new CheckPolicyTemplateRSP2(new Message(CPTMessages.ERROR_MANDATORY, 11), null));
     }
    } catch (CPTException e) {
      logger.error(CPTMessages.ERROR_STORE_PROCEDURE + e.getMessage());
      return new ResponseCPT2(new CheckPolicyTemplateRSP2(new Message(CPTMessages.ERROR_STORE_PROCEDURE + e.getMessage(), 12), null));
   }     
	  
}
  

  public String modifyTemplate(String policyCode,String productCode, String planCode, CheckPolicyTemplateREQ2 checkPolicyTemplateREQ2) {
		try {
	  String template = CPTInsertDao2.transformTemplate(checkPolicyTemplateREQ2);
		template = template.replace("+policyCode+", policyCode);
		template = template.replace("+productCode+", productCode);
		template = template.replace("+planCode+", planCode);
		//String template ="<html xmlns=\\\"http://www.w3.org/1999/xhtml\\\"> <head> <meta http-equiv=\\\"Content-Type\\\" content=\\\"text/html; charset=utf-8\\\" /> <title> Fallo Plantilla Certificado </title> </head> <body><br><font style=\\\"line-height: 0px;font-family: Calibri;\\\"> Estimados:  </font> <br><br> <p style=\\\"line-height: 100%;font-family: Calibri;\\\"> Se ha realizado solicitud de generaci&oacute;n de p&oacute;liza o certificado y no se ha encontrado plantilla asociada para:  </p> <br>  <font style=\\\"line-height: 150%;font-family: Calibri;\\\"> <b> P&oacute;liza : </b>"+policyCode+"</font> <br>  <font style=\\\"line-height: 150%;font-family: Calibri;\\\"> <b> Producto: </b> "+productCode+" </font> <br>  <font style=\\\"line-height: 150%;font-family: Calibri;\\\"> <b> Plan: </b>"+planCode+" </font> <br> </body> </html>";
			return template;
		}catch(Exception e){
			return "Template not found";
		}
	}


}
	
