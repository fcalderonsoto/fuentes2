package cl.cardif.policy.dao.impl;

import cl.cardif.policy.dao.CPTInsertDao;
import cl.cardif.policy.domain.request.CheckPolicyTemplateREQ;
import cl.cardif.policy.domain.response.Template;
import cl.cardif.policy.exception.CPTException;
import cl.cardif.policy.tools.CPTConstants;
import cl.cardif.policy.tools.CPTTools;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

@Repository
public class CPTInsertDaoImpl implements CPTInsertDao {

  @Autowired
  CPTTools cptTools;

  @Override
  public Template getTemplate(CheckPolicyTemplateREQ checkPolicyTemplateREQ) throws CPTException {
    Template template = null;
    try (Connection conn =
            ((DataSource)
                    ((Context) new InitialContext().lookup(CPTConstants.JAVA_COMP))
                        .lookup(CPTConstants.JNDI_TEMPLATE))
                .getConnection();
        CallableStatement call = conn.prepareCall(CPTConstants.PRC_GET_TEMPLATE)) {
      conn.setAutoCommit(false);
      call.setString(1, checkPolicyTemplateREQ.getPolicyCode());
      call.setString(2, checkPolicyTemplateREQ.getProductCode());
      call.setString(3, checkPolicyTemplateREQ.getPlanCode());
      call.registerOutParameter(4, OracleTypes.VARCHAR);
      call.registerOutParameter(5, OracleTypes.CLOB);
      call.registerOutParameter(6, OracleTypes.NUMBER);
      call.registerOutParameter(7, OracleTypes.VARCHAR);
      call.execute();
      if (call.getInt(6) == 0) {
        template = new Template(
                call.getString(4),
                cptTools.clobToString(call.getClob(5)));
      }else{
    	  throw new CPTException(call.getString(7), HttpStatus.INTERNAL_SERVER_ERROR, null);
      }
      conn.commit();
      conn.setAutoCommit(true);
    } catch (SQLException | NamingException e) {
      throw new CPTException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
    }
    return template;
  }
}
