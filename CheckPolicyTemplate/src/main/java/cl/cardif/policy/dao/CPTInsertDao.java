package cl.cardif.policy.dao;

import cl.cardif.policy.domain.request.CheckPolicyTemplateREQ;
import cl.cardif.policy.domain.response.Template;
import cl.cardif.policy.exception.CPTException;

public interface CPTInsertDao {

  Template getTemplate(CheckPolicyTemplateREQ checkPolicyTemplateREQ) throws CPTException;
}
