package cl.cardif.policy.tools;

public class CPTConstants {

  public static final String JAVA_COMP = "java:comp/env";
  public static final String JNDI_TEMPLATE= "jdbc/TEMPLATE";
  public static final String JNDI_POLIZA= "jdbc/POLIZA";
  
  public static final String COMMA = ",";
  public static final String NUMBER_ZERO_STRING = "0";
  public static final String FORMAT_FATE = "dd/MM/yyyy";

  public static final String PRC_GET_TEMPLATE = "{call PCK_COPIA_POLIZA.PRC_CONSULTAR_PLT(?,?,?,?,?,?,?)}";
  
  public static final String PRC_GET_PLANTILLA = "{call PCK_TRAZAS.PRC_GET_PLANTILLA(?,?,?,?,?)}";

}
