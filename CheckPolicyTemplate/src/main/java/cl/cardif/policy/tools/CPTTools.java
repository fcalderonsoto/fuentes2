package cl.cardif.policy.tools;

import cl.cardif.policy.domain.request.RequestCPT;
import cl.cardif.policy.domain.request2.RequestCPT2;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.SQLException;

@Service
public class CPTTools {

  public boolean mandatoryInput(RequestCPT requestCPT) {
    return requestCPT.getCheckPolicyTemplateREQ().getPolicyCode() != null
        && !requestCPT.getCheckPolicyTemplateREQ().getPolicyCode().isEmpty()
        && requestCPT.getCheckPolicyTemplateREQ().getProductCode() != null
        && !requestCPT.getCheckPolicyTemplateREQ().getProductCode().isEmpty()
        && requestCPT.getCheckPolicyTemplateREQ().getPlanCode() != null
        && !requestCPT.getCheckPolicyTemplateREQ().getPlanCode().isEmpty();
  }
  public boolean mandatoryInput2(RequestCPT2 requestCPT2) {
	    return requestCPT2.getCheckPolicyTemplateREQ2().getPolicyCode() != null
	        && !requestCPT2.getCheckPolicyTemplateREQ2().getPolicyCode().isEmpty()
	        && requestCPT2.getCheckPolicyTemplateREQ2().getProductCode() != null
	        && !requestCPT2.getCheckPolicyTemplateREQ2().getProductCode().isEmpty()
	        && requestCPT2.getCheckPolicyTemplateREQ2().getPlanCode() != null
	        && !requestCPT2.getCheckPolicyTemplateREQ2().getPlanCode().isEmpty()
	    	&& requestCPT2.getCheckPolicyTemplateREQ2().getNombrePlantilla() != null
	    	&& !requestCPT2.getCheckPolicyTemplateREQ2().getNombrePlantilla().isEmpty();

	  }

  public String clobToString(Clob data) throws SQLException {
    StringBuilder sb = new StringBuilder();
    try (Reader reader = data.getCharacterStream();
         BufferedReader br = new BufferedReader(reader)){
      String line;
      while(null != (line = br.readLine())) {
        sb.append(line);
      }
    } catch (SQLException | IOException e) {
      throw new SQLException("Error en la conversion del CLOB");
    }
    return sb.toString();
  }
}
