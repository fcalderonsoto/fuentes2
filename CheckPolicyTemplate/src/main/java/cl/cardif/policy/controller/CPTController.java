package cl.cardif.policy.controller;

import cl.cardif.policy.domain.request.RequestCPT;
import cl.cardif.policy.domain.request2.RequestCPT2;
import cl.cardif.policy.domain.response.ResponseCPT;
import cl.cardif.policy.domain.response2.ResponseCPT2;
import cl.cardif.policy.service.CPTService;
import cl.cardif.policy.service2.CPTService2;

import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class CPTController {

  @Autowired
  CPTService CPTService;
  @Autowired
  CPTService2 CPTService2;

  @PostMapping(
      value = "get-template",
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<ResponseCPT> getTemplate(@RequestBody RequestCPT requestCPT) {
    return new ResponseEntity<>(CPTService.getTemplate(requestCPT), HttpStatus.OK);
  }
  
  @PostMapping(
	      value = "get-template2",
	      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
	      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	  public ResponseEntity<ResponseCPT2> getTemplate2(@RequestBody RequestCPT2 requestCPT2) throws UnsupportedEncodingException {
	    return new ResponseEntity<>(CPTService2.getTemplate2(requestCPT2), HttpStatus.OK);
	  }
}
