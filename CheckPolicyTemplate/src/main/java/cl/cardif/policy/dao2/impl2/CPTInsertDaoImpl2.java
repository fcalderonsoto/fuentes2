package cl.cardif.policy.dao2.impl2;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import cl.cardif.policy.domain.request2.CheckPolicyTemplateREQ2;
import cl.cardif.policy.domain.response2.Template2;
import cl.cardif.policy.exception.CPTException;

import cl.cardif.policy.dao2.CPTInsertDao2;
import cl.cardif.policy.tools.CPTConstants;
import cl.cardif.policy.tools.CPTTools;
import oracle.jdbc.OracleTypes;

@Repository
public class CPTInsertDaoImpl2 implements CPTInsertDao2 {
	 @Autowired
	  CPTTools cptTools;
	 
	 public String transformTemplate(CheckPolicyTemplateREQ2 checkPolicyTemplateREQ2) throws CPTException  {
		 Template2 template2=null;
		 String nombrePlantilla = checkPolicyTemplateREQ2.getNombrePlantilla();
		 try (Connection conn =
		            ((DataSource)
		                    ((Context) new InitialContext().lookup(CPTConstants.JAVA_COMP))
		                        .lookup(CPTConstants.JNDI_POLIZA))
		                .getConnection();
		        CallableStatement call = conn.prepareCall(CPTConstants.PRC_GET_PLANTILLA)) {
			 	conn.setAutoCommit(false);
			 	call.setNull(1,Types.INTEGER );
				call.setString(2,nombrePlantilla );
				call.registerOutParameter(3, OracleTypes.CURSOR);
				call.registerOutParameter(4, OracleTypes.NUMBER);
				call.registerOutParameter(5, OracleTypes.VARCHAR);
				call.execute();
				if (call.getInt(4) == 0) {
					if(call.getObject(3)!= null){
						ResultSet resultSet = (ResultSet) call.getObject(3);
						while (resultSet.next()) {
							template2 = new Template2(resultSet.getString("CUERPO"));
						}
						resultSet.close();
						conn.commit();
						//conn.setAutoCommit(true);
				}   	  
		      }else{
		    	  throw new CPTException(call.getString(5), HttpStatus.INTERNAL_SERVER_ERROR, null);
		      }
		      conn.commit();
		      conn.setAutoCommit(true);
		    } catch (SQLException | NamingException e) {
		      throw new CPTException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
		    }
		 return template2.getCuerpo();
	 }
}
