package cl.cardif.policy.dao2;

import cl.cardif.policy.domain.request2.CheckPolicyTemplateREQ2;
import cl.cardif.policy.exception.CPTException;

public interface CPTInsertDao2 {

	String transformTemplate(CheckPolicyTemplateREQ2 checkPolicyTemplateREQ2)  throws CPTException;
	
}
