package cl.cardif.policy.tools;

public class CPEConstants {

  public static final String JAVA_COMP = "java:comp/env";
  public static final String JNDI_EASI = "jdbc/EASI";
  public static final String JNDI_BCU = "jdbc/BCU";

  public static final String FORMAT_DATE = "dd/MM/yyyy";
  public static final String PRC_CHECK_VALIDITY =
      "{call PCK_COPIA_POLIZAS_CERFITICADOS.PRC_DATOS_POLIZA(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
  public static final String PRC_GET_DATOS = "{call PCK_COPIA_POLIZA.PRC_GET_DATOS(?,?,?,?)}";

  public static final String REGEX_DATE =
      "^(0?[1-9]|[12][0-9]|3[01])[\\/\\-](0?[1-9]|1[012])[\\/\\-]\\d{4}$";
  public static final String REGEX_DNI = "^[0-9Kk]+$";

  public static final int NUMBER_ZERO = 0;
  public static final int NUMBER_ONE = 1;
  public static final int NUMBER_TWO = 2;
  public static final int NUMBER_EIGHT = 8;
  public static final int NUMBER_TEN = 10;
  public static final int NUMBER_ELEVEN = 11;
  public static final String DVK = "K";
  public static final String SLASH = "/";
  public static final String FLAG = "E";
  public static final String EMPTY = "";
}
