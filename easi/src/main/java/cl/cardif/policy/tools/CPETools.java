package cl.cardif.policy.tools;

import cl.cardif.policy.domain.model.InputPolicy;
import cl.cardif.policy.domain.request.RequestCPE;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CPETools {

	public static boolean validarRut(String rut) {

	    boolean validacion = false;
	    try {
	        rut =  rut.toUpperCase();
	        rut = rut.replace(".", "");
	        rut = rut.replace("-", "");
	        int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

	        char dv = rut.charAt(rut.length() - 1);

	        int m = 0, s = 1;
	        for (; rutAux != 0; rutAux /= 10) {
	            s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
	        }
	        if (dv == (char) (s != 0 ? s + 47 : 75)) {
	            validacion = true;
	        }

	    } catch (java.lang.NumberFormatException e) {
	    } catch (Exception e) {
	    }
	    return validacion;
	}
	
	public String dateToString(Date date) {
		if (date != null) {
			DateFormat df = new SimpleDateFormat(CPEConstants.FORMAT_DATE);
			return df.format(date);
		}
		return CPEConstants.EMPTY;
	}

	public String formatDB(BigDecimal value) {
		if (value == null) {
			BigDecimal bd = BigDecimal.ZERO;
			value = bd;
		}
//		return value.setScale(4, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString();
		if(value.setScale(4,BigDecimal.ROUND_HALF_UP).toPlainString().equals("0.0000")) {
			value = BigDecimal.ZERO;
			return value.toPlainString();
		}else {
			return	value.setScale(4,BigDecimal.ROUND_HALF_UP).toPlainString();
		}
		
	}
	public String formatDBTmp(BigDecimal value) {
		if (value == null) {
			BigDecimal bd = BigDecimal.ZERO;
			value = bd;
		}
			return value.toPlainString();
		
	}
	 public String formatNumber(BigDecimal value){
		 if(value != null) {
			 DecimalFormat df = new DecimalFormat("#,###.#", new DecimalFormatSymbols(new Locale("pt", "BR")));
			  return df.format(value.floatValue());
		 }else {
		  	return "";
		  }
	  }
	

	public InputPolicy transform(RequestCPE requestCP) {
		return new InputPolicy(requestCP.getCheckPolicyEasiREQ().getInsuredDni(),
				requestCP.getCheckPolicyEasiREQ().getOperationNumber(),
				requestCP.getCheckPolicyEasiREQ().getPolicyNumber(),
				requestCP.getCheckPolicyEasiREQ().getInsuredDate());
	}

	public boolean mandatoryInput(RequestCPE requestCP) {
		return requestCP.getCheckPolicyEasiREQ().getInsuredDni() != null
				&& !requestCP.getCheckPolicyEasiREQ().getInsuredDni().isEmpty()
				&& requestCP.getCheckPolicyEasiREQ().getInsuredDate() != null
				&& !requestCP.getCheckPolicyEasiREQ().getInsuredDate().isEmpty()
				&& requestCP.getCheckPolicyEasiREQ().getOperationNumber() != null
				&& !requestCP.getCheckPolicyEasiREQ().getOperationNumber().isEmpty()
				&& !(requestCP.getCheckPolicyEasiREQ().getOperationNumber().length() > 50)
				&& requestCP.getCheckPolicyEasiREQ().getPolicyNumber() != null
				&& !requestCP.getCheckPolicyEasiREQ().getPolicyNumber().isEmpty()
				&& !(requestCP.getCheckPolicyEasiREQ().getPolicyNumber().length() > 20)
				&& requestCP.getCheckPolicyEasiREQ().getPolicyType() != null
				&& !requestCP.getCheckPolicyEasiREQ().getPolicyType().isEmpty();
	}
}
