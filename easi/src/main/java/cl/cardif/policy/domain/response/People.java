package cl.cardif.policy.domain.response;

public class People {

  private long code;
  private long insuredCode;
  private String documentNumber;
  private String documentVerificationDigit;
  private String name;
  private long typeSequentialNumber;
  private String typeCode;
  private String type;
  private String address;
  private String addressCommune;
  private String addressCity;
  private String phoneNumber;
  private String email;
  private String birthDate;
  private String addressType;
  private String mobilePhoneNumber;
  private String activity;
  private String percentage;
  private String relationship;
  private String condition;
  private String validity;

  public People(
      long code,
      long insuredCode,
      String documentNumber,
      String documentVerificationDigit,
      String name,
      long typeSequentialNumber,
      String typeCode,
      String type,
      String address,
      String addressCommune,
      String addressCity,
      String phoneNumber,
      String email,
      String birthDate,
      String addressType,
      String mobilePhoneNumber,
      String activity,
      String percentage,
      String relationship,
      String condition,
      String validity) {
    this.code = code;
    this.insuredCode = insuredCode;
    this.documentNumber = documentNumber;
    this.documentVerificationDigit = documentVerificationDigit;
    this.name = name;
    this.typeSequentialNumber = typeSequentialNumber;
    this.typeCode = typeCode;
    this.type = type;
    this.address = address;
    this.addressCommune = addressCommune;
    this.addressCity = addressCity;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.birthDate = birthDate;
    this.addressType = addressType;
    this.mobilePhoneNumber = mobilePhoneNumber;
    this.activity = activity;
    this.percentage = percentage;
    this.relationship = relationship;
    this.condition = condition;
    this.validity = validity;
  }

  public long getCode() {
    return code;
  }

  public long getInsuredCode() {
    return insuredCode;
  }

  public String getDocumentNumber() {
    return documentNumber;
  }

  public String getDocumentVerificationDigit() {
    return documentVerificationDigit;
  }

  public String getName() {
    return name;
  }

  public long getTypeSequentialNumber() {
    return typeSequentialNumber;
  }

  public String getTypeCode() {
    return typeCode;
  }

  public String getType() {
    return type;
  }

  public String getAddress() {
    return address;
  }

  public String getAddressCommune() {
    return addressCommune;
  }

  public String getAddressCity() {
    return addressCity;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public String getEmail() {
    return email;
  }

  public String getBirthDate() {
    return birthDate;
  }

  public String getAddressType() {
    return addressType;
  }

  public String getMobilePhoneNumber() {
    return mobilePhoneNumber;
  }

  public String getActivity() {
    return activity;
  }

  public String getPercentage() {
    return percentage;
  }

public String getRelationship() {
	return relationship;
}

public String getCondition() {
	return condition;
}

public String getValidity() {
	return validity;
}

@Override
public String toString() {
	return "People [code=" + code + ", insuredCode=" + insuredCode + ", documentNumber=" + documentNumber
			+ ", documentVerificationDigit=" + documentVerificationDigit + ", name=" + name + ", typeSequentialNumber="
			+ typeSequentialNumber + ", typeCode=" + typeCode + ", type=" + type + ", address=" + address
			+ ", addressCommune=" + addressCommune + ", addressCity=" + addressCity + ", phoneNumber=" + phoneNumber
			+ ", email=" + email + ", birthDate=" + birthDate + ", addressType=" + addressType + ", mobilePhoneNumber="
			+ mobilePhoneNumber + ", activity=" + activity + ", percentage=" + percentage + ", relationship="
			+ relationship + ", condition=" + condition + ", validity=" + validity + "]";
}
  
}
