package cl.cardif.policy.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckPolicyEasiREQ {

  @JsonProperty("insuredDni")
  private String insuredDni;

  @JsonProperty("operationNumber")
  private String operationNumber;

  @JsonProperty("policyNumber")
  private String policyNumber;

  @JsonProperty("insuredDate")
  private String insuredDate;

  @JsonProperty("policyType")
  private String policyType;

  public String getInsuredDni() {
    return insuredDni;
  }

  public String getOperationNumber() {
    return operationNumber;
  }

  public String getPolicyNumber() {
    return policyNumber;
  }

  public String getInsuredDate() {
    return insuredDate;
  }

  public String getPolicyType() {
    return policyType;
  }
}
