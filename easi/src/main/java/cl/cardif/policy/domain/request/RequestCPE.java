package cl.cardif.policy.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestCPE {
	@JsonProperty("check_policy_REQ")
	private CheckPolicyEasiREQ checkPolicyEasiREQ;

	public CheckPolicyEasiREQ getCheckPolicyEasiREQ() {
		return checkPolicyEasiREQ;
	}

}
