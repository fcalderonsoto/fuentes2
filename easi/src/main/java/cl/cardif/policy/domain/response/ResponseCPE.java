package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseCPE {
  
	@JsonProperty("check_policy_RSP")
	private CheckPolicyEasiRSP checkPolicyEasiRSP;

	@JsonProperty("Message")
	private Message message;

	public ResponseCPE(CheckPolicyEasiRSP checkPolicyEasiRSP, Message message) {
		this.checkPolicyEasiRSP = checkPolicyEasiRSP;
		this.message = message;
	}
}
