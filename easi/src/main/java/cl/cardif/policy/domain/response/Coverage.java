package cl.cardif.policy.domain.response;


public class Coverage {

  private String proposalCode;
  private long personType;
  private String personName;
  private long personCode;
  private long insuredCode;
  private String code;
  private String description;
  private String capital;
  private String insuredRecordPremiumInPesos;
  private String percentage;
  private String netValue;
  private String exemptedValue;
  private String affectedValue;
  private String taxValue;
  private String grossValue;

  public Coverage(
      String proposalCode,
      long personType,
      String personName,
      long personCode,
      long insuredCode,
      String code,
      String description,
      String capital,
      String insuredRecordPremiumInPesos,
      String percentage,
      String netValue,
      String exemptedValue,
      String affectedValue,
      String taxValue,
      String grossValue) {
    this.proposalCode = proposalCode;
    this.personType = personType;
    this.personName = personName;
    this.personCode = personCode;
    this.insuredCode = insuredCode;
    this.code = code;
    this.description = description;
    this.capital = capital;
    this.insuredRecordPremiumInPesos = insuredRecordPremiumInPesos;
    this.percentage = percentage;
    this.netValue = netValue;
    this.exemptedValue = exemptedValue;
    this.affectedValue = affectedValue;
    this.taxValue = taxValue;
    this.grossValue = grossValue;
  }

  public String getProposalCode() {
    return proposalCode;
  }

  public long getPersonType() {
    return personType;
  }

  public long getPersonCode() {
    return personCode;
  }

  public long getInsuredCode() {
    return insuredCode;
  }

  public String getCode() {
    return code;
  }

  public String getDescription() {
    return description;
  }

  public String getCapital() {
    return capital;
  }

  public String getInsuredRecordPremiumInPesos() {
    return insuredRecordPremiumInPesos;
  }

  public String getPercentage() {
    return percentage;
  }

  public String getNetValue() {
    return netValue;
  }

  public String getExemptedValue() {
    return exemptedValue;
  }

  public String getAffectedValue() {
    return affectedValue;
  }

  public String getTaxValue() {
    return taxValue;
  }

  public String getGrossValue() {
    return grossValue;
  }

  public String getPersonName() {
	return personName;
  } 
  
}
