package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CheckPolicyEasiRSP {

  @JsonProperty("validity")
  private long validity;

  @JsonProperty("policyNumber")
  private String policyNumber;

  @JsonProperty("prodCode")
  private String prodCode;

  @JsonProperty("planCode")
  private String planCode;

  @JsonProperty("Policy")
  private Policy policy;

  @JsonProperty("Person")
  private List<People> peopleList;

  @JsonProperty("Coverage")
  private List<Coverage> coverageList;

  public CheckPolicyEasiRSP(
      final long validity,
      final String policyNumber,
      final String prodCode,
      final String planCode,
      final Policy policy,
      final List<People> peopleList,
      final List<Coverage> coverageList) {
    this.validity = validity;
    this.policyNumber = policyNumber;
    this.prodCode = prodCode;
    this.planCode = planCode;
    this.policy = policy;
    this.peopleList = peopleList;
    this.coverageList = coverageList;
  }

}
