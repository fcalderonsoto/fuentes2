package cl.cardif.policy.domain.response;


public class Policy {

  private String operationNumber;
  private String eMail;
  private String authorizeSendByEmail;
  private String partnerDocumentNumber;
  private String partnerName;
  private String number;
  private String certificateCode;
  private String effectiveStartDate;
  private String effectiveEndDate;
  private String calculatedEffectiveDate;
  private String emissionDate;
  private String saleDate;
  private String period;
  private String statusChangeDate;
  private String paymentPeriod;
  private long numberOfInsured;
  private String affectedPremium;
  private String exemptedPremium;
  private String netPremium;
  private String tax;
  private String grossPremium;
  private String monthlyGrossPremium;
  private String productCode;
  private String productDescription;
  private String planCode;
  private String planDescription;
  private String paymentMethod;
  private String certificatePlan;
  private String insuredAddress;
  private String insuredAddressNumber;
  private String insuredAddressCommune;
  private String insuredAddressCity;
  private String insuredPhoneNumber;
  private String insuredEmail;
  private String creditType;
  private long creditPeriod;
  private long creditAmount;
  private String totalAffectedPremium;
  private String totalExemptedPremium;
  private String totalNetPremium;
  private String totalPremiumTax;
  private String totalGrossPremium;
  private String insuredCapital;
  private String freeField;
  private String mutual;
  private String groupPolicy;
  private long telemarketingIndex;
  private String telemarketingCampaign;
  private long maxStayAge;
  private String capital;
  private String amountDue;
  private String insuredType;
  private String lastPaymentMonth;
  private String insuredRecordLastPaymentMonth;
  private String currencyConvertionDate;
  private String operationEndDate;
  private String OPEVstatusDate;
  private String OPECstatusDate;
  private String creditStartDate;
  private String cartolaPremiumInPesos;
  private String cartolaPremiumInUF;
  private String insuredRecordPremiumInPesos;
  private String insuredRecordStatus;
  private String insuredRecordCurrencyConversionDate;
  private String calculatedPremium;
  private String insuredRecordCalculatedTax;
  private String insuredRecordCalculatedPremium;
  private String premiumPesos;
  private String productType;
  private String searchDate;
  private String DataExtra1;
  private String DataExtra2;
  private String DataExtra3;
  private String DataExtra4;
  private String DataExtra5;
  private String DataExtra6;
  private String DataExtra7;
  private String DataExtra8;
  private String DataExtra9;
  private String DataExtra10;

  public Policy(
      String operationNumber,
      String eMail,
      String authorizeSendByEmail,
      String partnerDocumentNumber,
      String partnerName,
      String number,
      String certificateCode,
      String effectiveStartDate,
      String effectiveEndDate,
      String calculatedEffectiveDate,
      String emissionDate,
      String saleDate,
      String period,
      String statusChangeDate,
      String paymentPeriod,
      long numberOfInsured,
      String affectedPremium,
      String exemptedPremium,
      String netPremium,
      String tax,
      String grossPremium,
      String monthlyGrossPremium,
      String productCode,
      String productDescription,
      String planCode,
      String planDescription,
      String paymentMethod,
      String certificatePlan,
      String insuredAddress,
      String insuredAddressNumber,
      String insuredAddressCommune,
      String insuredAddressCity,
      String insuredPhoneNumber,
      String insuredEmail,
      String creditType,
      long creditPeriod,
      long creditAmount,
      String totalAffectedPremium,
      String totalExemptedPremium,
      String totalNetPremium,
      String totalPremiumTax,
      String totalGrossPremium,
      String insuredCapital,
      String freeField,
      String mutual,
      String groupPolicy,
      long telemarketingIndex,
      String telemarketingCampaign,
      long maxStayAge,
      String capital,
      String amountDue,
      String insuredType,
      String lastPaymentMonth,
      String insuredRecordLastPaymentMonth,
      String currencyConvertionDate,
      String operationEndDate,
      String OPEVstatusDate,
      String OPECstatusDate,
      String creditStartDate,
      String cartolaPremiumInPesos,
      String cartolaPremiumInUF,
      String insuredRecordPremiumInPesos,
      String insuredRecordStatus,
      String insuredRecordCurrencyConversionDate,
      String calculatedPremium,
      String insuredRecordCalculatedTax,
      String insuredRecordCalculatedPremium,
      String premiumPesos,
      String productType,
      String searchDate,
      String dataExtra1,
      String dataExtra2,
      String dataExtra3,
      String dataExtra4,
      String dataExtra5,
      String dataExtra6,
      String dataExtra7,
      String dataExtra8,
      String dataExtra9,
      String dataExtra10) {
    this.operationNumber = operationNumber;
    this.eMail = eMail;
    this.authorizeSendByEmail = authorizeSendByEmail;
    this.partnerDocumentNumber = partnerDocumentNumber;
    this.partnerName = partnerName;
    this.number = number;
    this.certificateCode = certificateCode;
    this.effectiveStartDate = effectiveStartDate;
    this.effectiveEndDate = effectiveEndDate;
    this.calculatedEffectiveDate = calculatedEffectiveDate;
    this.emissionDate = emissionDate;
    this.saleDate = saleDate;
    this.period = period;
    this.statusChangeDate = statusChangeDate;
    this.paymentPeriod = paymentPeriod;
    this.numberOfInsured = numberOfInsured;
    this.affectedPremium = affectedPremium;
    this.exemptedPremium = exemptedPremium;
    this.netPremium = netPremium;
    this.tax = tax;
    this.grossPremium = grossPremium;
    this.monthlyGrossPremium = monthlyGrossPremium;
    this.productCode = productCode;
    this.productDescription = productDescription;
    this.planCode = planCode;
    this.planDescription = planDescription;
    this.paymentMethod = paymentMethod;
    this.certificatePlan = certificatePlan;
    this.insuredAddress = insuredAddress;
    this.insuredAddressNumber = insuredAddressNumber;
    this.insuredAddressCommune = insuredAddressCommune;
    this.insuredAddressCity = insuredAddressCity;
    this.insuredPhoneNumber = insuredPhoneNumber;
    this.insuredEmail = insuredEmail;
    this.creditType = creditType;
    this.creditPeriod = creditPeriod;
    this.creditAmount = creditAmount;
    this.totalAffectedPremium = totalAffectedPremium;
    this.totalExemptedPremium = totalExemptedPremium;
    this.totalNetPremium = totalNetPremium;
    this.totalPremiumTax = totalPremiumTax;
    this.totalGrossPremium = totalGrossPremium;
    this.insuredCapital = insuredCapital;
    this.freeField = freeField;
    this.mutual = mutual;
    this.groupPolicy = groupPolicy;
    this.telemarketingIndex = telemarketingIndex;
    this.telemarketingCampaign = telemarketingCampaign;
    this.maxStayAge = maxStayAge;
    this.capital = capital;
    this.amountDue = amountDue;
    this.insuredType = insuredType;
    this.lastPaymentMonth = lastPaymentMonth;
    this.insuredRecordLastPaymentMonth = insuredRecordLastPaymentMonth;
    this.currencyConvertionDate = currencyConvertionDate;
    this.operationEndDate = operationEndDate;
    this.OPEVstatusDate = OPEVstatusDate;
    this.OPECstatusDate = OPECstatusDate;
    this.creditStartDate = creditStartDate;
    this.cartolaPremiumInPesos = cartolaPremiumInPesos;
    this.cartolaPremiumInUF = cartolaPremiumInUF;
    this.insuredRecordPremiumInPesos = insuredRecordPremiumInPesos;
    this.insuredRecordStatus = insuredRecordStatus;
    this.insuredRecordCurrencyConversionDate = insuredRecordCurrencyConversionDate;
    this.calculatedPremium = calculatedPremium;
    this.insuredRecordCalculatedTax = insuredRecordCalculatedTax;
    this.insuredRecordCalculatedPremium = insuredRecordCalculatedPremium;
    this.premiumPesos = premiumPesos;
    this.productType = productType;
    this.searchDate = searchDate;
    DataExtra1 = dataExtra1;
    DataExtra2 = dataExtra2;
    DataExtra3 = dataExtra3;
    DataExtra4 = dataExtra4;
    DataExtra5 = dataExtra5;
    DataExtra6 = dataExtra6;
    DataExtra7 = dataExtra7;
    DataExtra8 = dataExtra8;
    DataExtra9 = dataExtra9;
    DataExtra10 = dataExtra10;
  }

  public String getOperationNumber() {
    return operationNumber;
  }

  public String geteMail() {
    return eMail;
  }

  public String getAuthorizeSendByEmail() {
    return authorizeSendByEmail;
  }

  public String getPartnerDocumentNumber() {
    return partnerDocumentNumber;
  }

  public String getPartnerName() {
    return partnerName;
  }

  public String getNumber() {
    return number;
  }

  public String getCertificateCode() {
    return certificateCode;
  }

  public String getEffectiveStartDate() {
    return effectiveStartDate;
  }

  public String getEffectiveEndDate() {
    return effectiveEndDate;
  }

  public String getCalculatedEffectiveDate() {
    return calculatedEffectiveDate;
  }

  public String getEmissionDate() {
    return emissionDate;
  }

  public String getSaleDate() {
    return saleDate;
  }

  public String getPeriod() {
    return period;
  }

  public String getStatusChangeDate() {
    return statusChangeDate;
  }

  public String getPaymentPeriod() {
    return paymentPeriod;
  }

  public long getNumberOfInsured() {
    return numberOfInsured;
  }

  public String getAffectedPremium() {
    return affectedPremium;
  }

  public String getExemptedPremium() {
    return exemptedPremium;
  }

  public String getNetPremium() {
    return netPremium;
  }

  public String getTax() {
    return tax;
  }

  public String getGrossPremium() {
    return grossPremium;
  }

  public String getMonthlyGrossPremium() {
    return monthlyGrossPremium;
  }

  public String getProductCode() {
    return productCode;
  }

  public String getProductDescription() {
    return productDescription;
  }

  public String getPlanCode() {
    return planCode;
  }

  public String getPlanDescription() {
    return planDescription;
  }

  public String getPaymentMethod() {
    return paymentMethod;
  }

  public String getCertificatePlan() {
    return certificatePlan;
  }

  public String getInsuredAddress() {
    return insuredAddress;
  }

  public String getInsuredAddressNumber() {
    return insuredAddressNumber;
  }

  public String getInsuredAddressCommune() {
    return insuredAddressCommune;
  }

  public String getInsuredAddressCity() {
    return insuredAddressCity;
  }

  public String getInsuredPhoneNumber() {
    return insuredPhoneNumber;
  }

  public String getInsuredEmail() {
    return insuredEmail;
  }

  public String getCreditType() {
    return creditType;
  }

  public long getCreditPeriod() {
    return creditPeriod;
  }

  public long getCreditAmount() {
    return creditAmount;
  }

  public String getTotalAffectedPremium() {
    return totalAffectedPremium;
  }

  public String getTotalExemptedPremium() {
    return totalExemptedPremium;
  }

  public String getTotalNetPremium() {
    return totalNetPremium;
  }

  public String getTotalPremiumTax() {
    return totalPremiumTax;
  }

  public String getTotalGrossPremium() {
    return totalGrossPremium;
  }

  public String getInsuredCapital() {
    return insuredCapital;
  }

  public String getFreeField() {
    return freeField;
  }

  public String getMutual() {
    return mutual;
  }

  public String getGroupPolicy() {
    return groupPolicy;
  }

  public long getTelemarketingIndex() {
    return telemarketingIndex;
  }

  public String getTelemarketingCampaign() {
    return telemarketingCampaign;
  }

  public long getMaxStayAge() {
    return maxStayAge;
  }

  public String getCapital() {
    return capital;
  }

  public String getAmountDue() {
    return amountDue;
  }

  public String getInsuredType() {
    return insuredType;
  }

  public String getLastPaymentMonth() {
    return lastPaymentMonth;
  }

  public String getInsuredRecordLastPaymentMonth() {
    return insuredRecordLastPaymentMonth;
  }

  public String getCurrencyConvertionDate() {
    return currencyConvertionDate;
  }

  public String getOperationEndDate() {
    return operationEndDate;
  }

  public String getOPEVstatusDate() {
    return OPEVstatusDate;
  }

  public String getOPECstatusDate() {
    return OPECstatusDate;
  }

  public String getCreditStartDate() {
    return creditStartDate;
  }

  public String getCartolaPremiumInPesos() {
    return cartolaPremiumInPesos;
  }

  public String getCartolaPremiumInUF() {
    return cartolaPremiumInUF;
  }

  public String getInsuredRecordPremiumInPesos() {
    return insuredRecordPremiumInPesos;
  }

  public String getInsuredRecordStatus() {
    return insuredRecordStatus;
  }

  public String getInsuredRecordCurrencyConversionDate() {
    return insuredRecordCurrencyConversionDate;
  }

  public String getCalculatedPremium() {
    return calculatedPremium;
  }

  public String getInsuredRecordCalculatedTax() {
    return insuredRecordCalculatedTax;
  }

  public String getInsuredRecordCalculatedPremium() {
    return insuredRecordCalculatedPremium;
  }

  public String getPremiumPesos() {
    return premiumPesos;
  }

  public String getProductType() {
    return productType;
  }

  public String getSearchDate() {
    return searchDate;
  }

  public String getDataExtra1() {
    return DataExtra1;
  }

  public String getDataExtra2() {
    return DataExtra2;
  }

  public String getDataExtra3() {
    return DataExtra3;
  }

  public String getDataExtra4() {
    return DataExtra4;
  }

  public String getDataExtra5() {
    return DataExtra5;
  }

  public String getDataExtra6() {
    return DataExtra6;
  }

  public String getDataExtra7() {
    return DataExtra7;
  }

  public String getDataExtra8() {
    return DataExtra8;
  }

  public String getDataExtra9() {
    return DataExtra9;
  }

  public String getDataExtra10() {
    return DataExtra10;
  }
}
