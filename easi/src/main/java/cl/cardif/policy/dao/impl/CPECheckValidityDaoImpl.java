package cl.cardif.policy.dao.impl;

import cl.cardif.policy.dao.CPECheckValidityDao;
import cl.cardif.policy.domain.model.InputPolicy;
import cl.cardif.policy.domain.response.CheckPolicyEasiRSP;
import cl.cardif.policy.domain.response.Coverage;
import cl.cardif.policy.domain.response.People;
import cl.cardif.policy.domain.response.Policy;
import cl.cardif.policy.exception.CPEException;
import cl.cardif.policy.tools.CPEConstants;
import cl.cardif.policy.tools.CPETools;
import oracle.jdbc.OracleTypes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CPECheckValidityDaoImpl implements CPECheckValidityDao {
	//private static Logger logger = LoggerFactory.getLogger(CPECheckValidityDaoImpl.class);
	@Autowired
	CPETools cpeTools;

	@Override
	public CheckPolicyEasiRSP checkEnsureValidityEasi(InputPolicy inputPolicy) throws CPEException {
		CheckPolicyEasiRSP checkPolicyEasiRSP = null;
		try (Connection conn = ((DataSource) ((Context) new InitialContext().lookup(CPEConstants.JAVA_COMP))
				.lookup(CPEConstants.JNDI_EASI)).getConnection();
				CallableStatement call = conn.prepareCall(CPEConstants.PRC_CHECK_VALIDITY)) {
			conn.setAutoCommit(false);
			call.setString(1, inputPolicy.getInsuredDni());
			call.setString(2, inputPolicy.getOperationNumber());
			call.setString(3, inputPolicy.getPolicyNumber());
			call.setString(4, inputPolicy.getInsuredDate());
			call.registerOutParameter(5, OracleTypes.NUMBER);
			call.registerOutParameter(6, OracleTypes.VARCHAR);
			call.registerOutParameter(7, OracleTypes.VARCHAR);
			call.registerOutParameter(8, OracleTypes.VARCHAR);
			call.registerOutParameter(9, OracleTypes.NUMBER);
			call.registerOutParameter(10, OracleTypes.VARCHAR);
			call.registerOutParameter(11, OracleTypes.CURSOR);
			call.registerOutParameter(12, OracleTypes.CURSOR);
			call.registerOutParameter(13, OracleTypes.CURSOR);
			call.execute();
			if (call.getInt(9) == 0) {
				List<Policy> policyList = setPolicies(call);
				List<People> peopleList = setPeople(call);
				List<Coverage> coverageList = setCoverages(call);
				checkPolicyEasiRSP = new CheckPolicyEasiRSP(call.getLong(5), call.getString(6), call.getString(7),
						call.getString(8), !policyList.isEmpty() ? policyList.get(0) : null, peopleList, coverageList);
			} else {
				throw new CPEException(call.getString(10), HttpStatus.INTERNAL_SERVER_ERROR, null);
			}
			conn.commit();
			conn.setAutoCommit(true);
		} catch (SQLException | NamingException e) {
			throw new CPEException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		return checkPolicyEasiRSP;
	}

	private List<Coverage> setCoverages(CallableStatement call) throws SQLException {
		List<Coverage> coverageList = new ArrayList<>();
		Coverage coverage;
		ResultSet rsCoverage = (ResultSet) call.getObject(13);
		while (rsCoverage.next()) {
			coverage = new Coverage(rsCoverage.getString("COD_PROPUESTA"),
					rsCoverage.getLong("COR_TIPOPERSONA"),
					rsCoverage.getString("TIPO_PER"),
					rsCoverage.getLong("COD_PERSONA"),
					rsCoverage.getLong("COD_ASEGURADO"),
					rsCoverage.getString("COD_COBERTURA"),
					rsCoverage.getString("DSC_COBERTURA"),
					rsCoverage.getString("CAPITAL"),
					rsCoverage.getString("RAS_PRIMA_PESOS"),
					rsCoverage.getString("PORCENTAJE"),
					cpeTools.formatDB(rsCoverage.getBigDecimal("VAL_NETO")),
					cpeTools.formatDB(rsCoverage.getBigDecimal("VAL_EXENTA")),
					cpeTools.formatDB(rsCoverage.getBigDecimal("VAL_NETO")),
					cpeTools.formatDB(rsCoverage.getBigDecimal("VAL_IVA")),
					cpeTools.formatDB(rsCoverage.getBigDecimal("VAL_BRUTO")));
			coverageList.add(coverage);
		}
		return coverageList;
	}

	private List<People> setPeople(CallableStatement call) throws SQLException {
		List<People> peopleList = new ArrayList<>();
		People people;
		
		ResultSet rsPeople = (ResultSet) call.getObject(12);
		try (Connection conn2 = ((DataSource) ((Context) new InitialContext().lookup(CPEConstants.JAVA_COMP))
				.lookup(CPEConstants.JNDI_BCU)).getConnection();
				CallableStatement call2 = conn2.prepareCall(CPEConstants.PRC_GET_DATOS)) {			
		while(rsPeople.next()) {
			String nombreFromBcu="";
			String nombre="";
			String rut="";
			conn2.setAutoCommit(false);
			rut= rsPeople.getString("RUT")+"-"+rsPeople.getString("DIG_VERIFICADOR");
			if(CPETools.validarRut(rut)){			
				call2.setString(1,rut);
				call2.registerOutParameter(2, OracleTypes.CURSOR);
				call2.registerOutParameter(3, OracleTypes.NUMBER);
				call2.registerOutParameter(4, OracleTypes.VARCHAR);
				call2.execute();

					if (call2.getInt(3) == 0) {
						ResultSet rsPeopleBCU = (ResultSet) call2.getObject(2);
						while(rsPeopleBCU.next()){
							nombreFromBcu=rsPeopleBCU.getString("NOMBRES")+","+
										  rsPeopleBCU.getString("APELLIDO_PATERNO")+" "+
										  rsPeopleBCU.getString("APELLIDO_MATERNO");
						}
						rsPeopleBCU.close(); 	 
					}
							
				conn2.commit();
				conn2.setAutoCommit(true);				
			}
			if(nombreFromBcu.equals("")) {
				nombre = rsPeople.getString("NOMBRE");
			}else {
				nombre = nombreFromBcu;
			}
			people = new People(rsPeople.getLong("COD_PERSONA"),
					rsPeople.getLong("COD_ASEGURADO"),
					rsPeople.getString("RUT"),
					rsPeople.getString("DIG_VERIFICADOR"),
					nombre,
					rsPeople.getLong("COR_TIPOPERSONA"),
					rsPeople.getString("COD_TIPOPERSONA"),
					rsPeople.getString("TIPO_PER"),
					rsPeople.getString("DOMICILIO"),
					rsPeople.getString("COMUNA"),
					rsPeople.getString("CIUDAD"),
					rsPeople.getString("TELEFONO"),
					rsPeople.getString("EMAIL"),
					rsPeople.getString("FECHA_NAC"),
					rsPeople.getString("TIPO_DOMICILIO"),
					rsPeople.getString("MOVIL"),
					rsPeople.getString("OCUPACION"),
					rsPeople.getString("PORCENTAJE"),
					rsPeople.getString("PARENTESCO"),
					rsPeople.getString("CONDICION"),
					rsPeople.getString("VIGENCIA"));
			peopleList.add(people);
			}
		} catch (SQLException | NamingException e) {
			throw new CPEException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
		}
		rsPeople.close();
		return peopleList;
	}

	private List<Policy> setPolicies(CallableStatement call) throws SQLException {
		List<Policy> policyList = new ArrayList<>();
		Policy policy;
		ResultSet rsPolicies = (ResultSet) call.getObject(11);
		while (rsPolicies.next()) {
			policy = new Policy(rsPolicies.getString("lprNumeroOperacion"),
					rsPolicies.getString("Mail"),
					rsPolicies.getString("AutorizaMail"),
					rsPolicies.getString("LprSocioRut"),
					rsPolicies.getString("SocioNombre"),
					rsPolicies.getString("CodPoliza"),
					rsPolicies.getString("CodCertificado"),
					cpeTools.dateToString(rsPolicies.getDate("FechaVigenciaIni")),
					cpeTools.dateToString(rsPolicies.getDate("FechaVigenciaFin")),
					cpeTools.dateToString(rsPolicies.getDate("FechaVigenciaCalc")),
					cpeTools.dateToString(rsPolicies.getDate("FechaEmision")),
					cpeTools.dateToString(rsPolicies.getDate("FechaVenta")),
					cpeTools.dateToString(rsPolicies.getDate("Periodo")),
					cpeTools.dateToString(rsPolicies.getDate("FechaCambioEstado")),
					rsPolicies.getString("PeriodicidadPago"),
					rsPolicies.getLong("NroAsegurado"),
					rsPolicies.getString("ProdPrimaAfec"),
					rsPolicies.getString("ProdPrimaExc"),
					rsPolicies.getString("ProdPrimaNeta"),
					rsPolicies.getString("ProdIva"),
					rsPolicies.getString("ProdPrimaBruta"),
					rsPolicies.getString("ProdPrbrutaMes"),
					rsPolicies.getString("ProdCodigo"),
					rsPolicies.getString("ProdDescripcion"),
					rsPolicies.getString("PlanCodigo"),
					rsPolicies.getString("PlanDescripcion"),
					rsPolicies.getString("ModalidadPago"),
					rsPolicies.getString("CertPlan"),
					rsPolicies.getString("Domicilio"),
					rsPolicies.getString("DomicilioNumero"),
					rsPolicies.getString("Comuna"),
					rsPolicies.getString("Ciudad"),
					rsPolicies.getString("Telefono"),
					rsPolicies.getString("Email"),
					rsPolicies.getString("TipoCredito"),
					rsPolicies.getLong("PlazoCredito"),
					rsPolicies.getLong("MontoCredito"),
					cpeTools.formatDB(rsPolicies.getBigDecimal("TotTitPrimaAfecta")),
		            cpeTools.formatDB(rsPolicies.getBigDecimal("TotTitPrimaExcenta")),
		            cpeTools.formatDB(rsPolicies.getBigDecimal("TotTitPrimaNeta")),
		            cpeTools.formatDB(rsPolicies.getBigDecimal("TotTitIva")),
		            cpeTools.formatDB(rsPolicies.getBigDecimal("TotTitPrimaBruta")),
		            rsPolicies.getString("CapitalAsegurado"),
					rsPolicies.getString("CampoLibre"),
					rsPolicies.getString("Mutuo"),
					rsPolicies.getString("PolizaColectiva"),
					rsPolicies.getLong("IndiceTmk"),
					rsPolicies.getString("CampanaTmk"),
					rsPolicies.getLong("EdadMaxPermanencia"),
					rsPolicies.getString("Capital"),
					rsPolicies.getString("SaldoInsoluto"),
					rsPolicies.getString("TipoAsegurado"),
					rsPolicies.getString("MesUltimoPago"),
					rsPolicies.getString("MesUltimoRa"),
					rsPolicies.getString("FechaConvMoneda"),
					rsPolicies.getString("FechaFinOpe"),
					rsPolicies.getString("FechaEstadoOpev"),
					rsPolicies.getString("FechaEstadoOpec"),
					cpeTools.dateToString(rsPolicies.getDate("InicioCredito")),
					cpeTools.formatNumber(rsPolicies.getBigDecimal("PrimaCartPesos")),
					cpeTools.formatDB(rsPolicies.getBigDecimal("PrimaCartUf")),
					rsPolicies.getString("RasPrimaPeso"),
					rsPolicies.getString("RasEstado"),
					rsPolicies.getString("RasFechaConvMoneda"),
					rsPolicies.getString("PrimaCalculada"),
					rsPolicies.getString("RasImpuestoPcalc"),
					rsPolicies.getString("RasSobreprimaCalc"),
					rsPolicies.getString("PrimaPesos"),
					rsPolicies.getString("TipoProducto"),
					cpeTools.dateToString(rsPolicies.getDate("FechaBusqueda")),
					rsPolicies.getString("DataExtra1"),
					rsPolicies.getString("DataExtra2"),
					rsPolicies.getString("DataExtra3"),
					rsPolicies.getString("DataExtra4"),
					rsPolicies.getString("DataExtra5"),
					rsPolicies.getString("DataExtra6"),
					rsPolicies.getString("DataExtra7"),
					rsPolicies.getString("DataExtra8"),
					rsPolicies.getString("DataExtra9"),
					rsPolicies.getString("DataExtra10"));
			policyList.add(policy);
		}
		rsPolicies.close();
		return policyList;
	}
}
