package cl.cardif.policy.dao;

import cl.cardif.policy.domain.model.InputPolicy;
import cl.cardif.policy.domain.response.CheckPolicyEasiRSP;
import cl.cardif.policy.exception.CPEException;

public interface CPECheckValidityDao {

  CheckPolicyEasiRSP checkEnsureValidityEasi(InputPolicy inputPolicy) throws CPEException;
}
