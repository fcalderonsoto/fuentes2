package cl.cardif.policy.service;

import cl.cardif.policy.domain.request.RequestCPE;
import cl.cardif.policy.domain.response.ResponseCPE;

public interface CPEService {

  ResponseCPE validate(RequestCPE requestCP);
}
