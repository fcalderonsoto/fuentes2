package cl.cardif.policy.controller;

import cl.cardif.policy.domain.request.RequestCPV;
import cl.cardif.policy.domain.response.ResponseCPV;
import cl.cardif.policy.service.CPVService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class CPVController {

  @Autowired
  CPVService CPVService;

  @PostMapping(
      value = "validate",
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<ResponseCPV> validate(@RequestBody RequestCPV requestCPE) {
    return new ResponseEntity<>(CPVService.validate(requestCPE), HttpStatus.OK);
  }
}
