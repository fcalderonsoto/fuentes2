package cl.cardif.policy.tools;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

import cl.cardif.policy.domain.request.RequestCPV;

@Service
public class CPVTools {

  public boolean mandatoryInput(RequestCPV requestCP) {
    return requestCP.getCheckPolicyValidateREQ().getInsuredDni() != null
        && !requestCP.getCheckPolicyValidateREQ().getInsuredDni().isEmpty()
        && requestCP.getCheckPolicyValidateREQ().getInsuredDate() != null
        && !requestCP.getCheckPolicyValidateREQ().getInsuredDate().isEmpty()
        && requestCP.getCheckPolicyValidateREQ().getOperationNumber() != null
        && !requestCP.getCheckPolicyValidateREQ().getOperationNumber().isEmpty()
        && !(requestCP.getCheckPolicyValidateREQ().getOperationNumber().length()>50)
        && requestCP.getCheckPolicyValidateREQ().getPolicyNumber() != null
        && !requestCP.getCheckPolicyValidateREQ().getPolicyNumber().isEmpty()
    	&& !(requestCP.getCheckPolicyValidateREQ().getPolicyNumber().length() > 20);
        //&& requestCP.getCheckPolicyValidateREQ().getPolicyType() != null
        //&& !requestCP.getCheckPolicyValidateREQ().getPolicyType().isEmpty();
  }

  public boolean validateRutAndDate(RequestCPV requestCP) throws ParseException {
    return Pattern.matches(
            CPVConstants.REGEX_DNI, requestCP.getCheckPolicyValidateREQ().getInsuredDni())
        && validationDNI(requestCP.getCheckPolicyValidateREQ().getInsuredDni())
        && Pattern.matches(
            CPVConstants.REGEX_DATE, requestCP.getCheckPolicyValidateREQ().getInsuredDate())
        && validationDate(requestCP.getCheckPolicyValidateREQ().getInsuredDate())
        && validationDay(requestCP.getCheckPolicyValidateREQ().getInsuredDate());
  }

  private boolean validationDate(String insuredDate) {
    SimpleDateFormat sdf = new SimpleDateFormat(CPVConstants.FORMAT_DATE);
    String[] split = insuredDate.split(CPVConstants.SLASH);
    int year = Integer.parseInt(split[CPVConstants.NUMBER_TWO]);
    int month = Integer.parseInt(split[CPVConstants.NUMBER_ONE]) - CPVConstants.NUMBER_ONE;
    int day = Integer.parseInt(split[CPVConstants.NUMBER_ZERO]);
    Calendar calendar = new GregorianCalendar(year, month, day);
    return insuredDate.equalsIgnoreCase(sdf.format(calendar.getTime()));
  }
  
  private boolean validationDay(String insuredDate) throws ParseException {
	    SimpleDateFormat dateFormat = new SimpleDateFormat(CPVConstants.FORMAT_DATE);
	    java.util.Date startDate = dateFormat.parse("12/12/2019");
	    java.util.Date today = new java.util.Date();
	    String simpleDateFormat = new SimpleDateFormat(CPVConstants.FORMAT_DATE).format(today);
	    java.util.Date endDate = dateFormat.parse(simpleDateFormat);
	    int days = (int) ((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24));
		return days >= 0;
  }

  private boolean validationDNI(String dni) {
    int multiply = CPVConstants.NUMBER_TWO;
    int sum = CPVConstants.NUMBER_ZERO;
    int digitNum;
    String dniUpper = dni.toUpperCase();
    String rut =
        dniUpper.substring(CPVConstants.NUMBER_ZERO, dniUpper.length() - CPVConstants.NUMBER_ONE);
    String dv = dniUpper.substring(dniUpper.length() - CPVConstants.NUMBER_ONE);
    if (rut.isEmpty()) {
      return false;
    } else {
      for (int i = (rut.length() - 1); i >= 0; i--) {
        sum = sum + Integer.parseInt(rut.substring(i, i + CPVConstants.NUMBER_ONE)) * multiply;
        multiply++;
        if (multiply == CPVConstants.NUMBER_EIGHT) {
          multiply = CPVConstants.NUMBER_TWO;
        }
      }
      digitNum = CPVConstants.NUMBER_ELEVEN - (sum % CPVConstants.NUMBER_ELEVEN);
      if (digitNum == CPVConstants.NUMBER_TEN && dv.equals(CPVConstants.DVK)) {
        return true;
      } else if (digitNum == CPVConstants.NUMBER_ELEVEN
          && Integer.parseInt(dv) == CPVConstants.NUMBER_ZERO) {
        return true;
      } else return digitNum == Integer.parseInt(dv);
    }
  }
}
