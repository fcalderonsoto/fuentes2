package cl.cardif.policy.service.impl;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.cardif.policy.domain.request.RequestCPV;
import cl.cardif.policy.domain.response.CheckPolicyValidateRSP;
import cl.cardif.policy.domain.response.Message;
import cl.cardif.policy.domain.response.ResponseCPV;
import cl.cardif.policy.service.CPVService;
import cl.cardif.policy.tools.CPVMessages;
import cl.cardif.policy.tools.CPVTools;

@Service
public class CPVServiceImpl implements CPVService {

  private static Logger logger = LoggerFactory.getLogger(CPVServiceImpl.class);

  @Autowired
  CPVTools CPVTools;

  @Override
  public ResponseCPV validate(RequestCPV requestCPV) {
	  try {
      if (CPVTools.mandatoryInput(requestCPV)) {
        if (CPVTools.validateRutAndDate(requestCPV)) {
        	logger.info(CPVMessages.OK);
            return new ResponseCPV(new CheckPolicyValidateRSP(new Message(CPVMessages.OK, 10)));
        }else {
	        logger.error(CPVMessages.ERROR_VALIDATION_DNI_OR_DATE);
	        return new ResponseCPV(
	            new CheckPolicyValidateRSP(new Message(CPVMessages.ERROR_VALIDATION_DNI_OR_DATE, 11)));
        }
      }else {
	        logger.error(CPVMessages.ERROR_MANDATORY);
	        return new ResponseCPV(
	            new CheckPolicyValidateRSP(new Message(CPVMessages.ERROR_MANDATORY, 12)));
      }
	  }catch (ParseException e) {
	        return new ResponseCPV(
		            new CheckPolicyValidateRSP(new Message(CPVMessages.ERROR_VALIDATION_DNI_OR_DATE, 11)));
	}
  }
}
