package cl.cardif.policy.service;

import cl.cardif.policy.domain.request.RequestCPV;
import cl.cardif.policy.domain.response.ResponseCPV;

public interface CPVService {

  ResponseCPV validate(RequestCPV requestCP);
}
