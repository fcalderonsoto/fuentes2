package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckPolicyValidateRSP {
  @JsonProperty("Message")
  private Message message;

  public CheckPolicyValidateRSP(final Message message) {
    this.message = message;
  }
}
