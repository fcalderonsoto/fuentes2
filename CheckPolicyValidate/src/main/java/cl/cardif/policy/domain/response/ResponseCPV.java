package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseCPV {
  
	@JsonProperty("check_policy_validate_RSP")
	private CheckPolicyValidateRSP checkPolicyValidateRSP;

	public ResponseCPV(CheckPolicyValidateRSP checkPolicyValidateRSP) {
	    this.checkPolicyValidateRSP = checkPolicyValidateRSP;
	}

	public CheckPolicyValidateRSP getCheckPolicyValidateRSP() {
		return checkPolicyValidateRSP;
	}

}
