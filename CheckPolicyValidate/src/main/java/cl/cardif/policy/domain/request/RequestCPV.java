package cl.cardif.policy.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestCPV {
	@JsonProperty("check_policy_validate_REQ")
	private CheckPolicyValidateREQ checkPolicyValidateREQ;

	public CheckPolicyValidateREQ getCheckPolicyValidateREQ() {
		return checkPolicyValidateREQ;
	}

}
