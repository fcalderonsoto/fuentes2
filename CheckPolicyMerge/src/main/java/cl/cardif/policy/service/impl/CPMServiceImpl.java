package cl.cardif.policy.service.impl;

import cl.cardif.policy.dao.CPMGetMerge;
import cl.cardif.policy.domain.model.MetaData;
import cl.cardif.policy.domain.request.RequestCPM;
import cl.cardif.policy.domain.response.CheckPolicyMergeRSP;
import cl.cardif.policy.domain.response.Message;
import cl.cardif.policy.domain.response.MetadataList;
import cl.cardif.policy.domain.response.ResponseCPM;
import cl.cardif.policy.exception.CPMException;
import cl.cardif.policy.service.CPMService;
import cl.cardif.policy.tools.CPMMessages;
import cl.cardif.policy.tools.CPMTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CPMServiceImpl implements CPMService {

  private static Logger logger = LoggerFactory.getLogger(CPMServiceImpl.class);

  @Autowired CPMTools cpmTools;

  @Autowired CPMGetMerge cpmGetMerge;

  @Override
  public ResponseCPM getDocumentData(RequestCPM requestCPM) {
    try {
      if (cpmTools.mandatoryInput(requestCPM.getCheckPolicyMergeREQ())) {
    	MetaData metaData = cpmGetMerge.getDocumentData(requestCPM.getCheckPolicyMergeREQ());
        MetadataList metadataList = cpmTools.generateOutput(metaData, requestCPM.getCheckPolicyMergeREQ());
        return new ResponseCPM(new CheckPolicyMergeRSP(new Message(CPMMessages.OK, 10),metadataList));
      } else {
        logger.error(CPMMessages.ERROR_MANDATORY);
        return new ResponseCPM(new CheckPolicyMergeRSP(new Message(CPMMessages.ERROR_MANDATORY, 11), null));
      }
    } catch (CPMException e) {
      return new ResponseCPM(new CheckPolicyMergeRSP(new Message(e.getMessage(), 12), null));
    }
  }
}
