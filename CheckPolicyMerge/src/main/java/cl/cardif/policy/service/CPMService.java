package cl.cardif.policy.service;

import cl.cardif.policy.domain.request.RequestCPM;
import cl.cardif.policy.domain.response.ResponseCPM;

public interface CPMService {

  ResponseCPM getDocumentData(RequestCPM requestCP);
}
