package cl.cardif.policy.controller;

import cl.cardif.policy.domain.request.RequestCPM;
import cl.cardif.policy.domain.response.ResponseCPM;
import cl.cardif.policy.service.CPMService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class CPMController {

  @Autowired
  CPMService CPMService;

  @PostMapping(
      value = "get-document-data",
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<ResponseCPM> getDocumentData(@RequestBody RequestCPM requestCPE) {
    return new ResponseEntity<>(CPMService.getDocumentData(requestCPE), HttpStatus.OK);
  }
}
