package cl.cardif.policy.dao;

import cl.cardif.policy.domain.model.MetaData;
import cl.cardif.policy.domain.request.CheckPolicyMergeREQ;
import cl.cardif.policy.exception.CPMException;

public interface CPMGetMerge {

  MetaData getDocumentData(CheckPolicyMergeREQ checkPolicyMergeREQ) throws CPMException;
}
