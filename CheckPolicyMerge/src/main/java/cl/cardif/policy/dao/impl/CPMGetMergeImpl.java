package cl.cardif.policy.dao.impl;

import cl.cardif.policy.dao.CPMGetMerge;
import cl.cardif.policy.domain.model.MetaData;
import cl.cardif.policy.domain.model.MetaDataCoverage;
import cl.cardif.policy.domain.model.MetadataPerson;
import cl.cardif.policy.domain.model.MetadataPolicy;
import cl.cardif.policy.domain.request.CheckPolicyMergeREQ;
import cl.cardif.policy.exception.CPMException;
import cl.cardif.policy.tools.CPMConstants;
import oracle.jdbc.OracleTypes;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CPMGetMergeImpl implements CPMGetMerge {

  @Override
  public MetaData getDocumentData(CheckPolicyMergeREQ checkPolicyMergeREQ) throws CPMException {
    MetaData metaData;
    try (Connection conn =
            ((DataSource)
                    ((Context) new InitialContext().lookup(CPMConstants.JAVA_COMP))
                        .lookup(CPMConstants.JNDI_POLI))
                .getConnection();
        CallableStatement call = conn.prepareCall(CPMConstants.PRC_GET_MERGE)) {
      conn.setAutoCommit(false);
      call.registerOutParameter(1, OracleTypes.CURSOR);
      call.registerOutParameter(2, OracleTypes.CURSOR);
      call.registerOutParameter(3, OracleTypes.CURSOR);
      call.registerOutParameter(4, OracleTypes.NUMBER);
      call.registerOutParameter(5, OracleTypes.VARCHAR);
      call.execute();
      if (call.getInt(4) == 0) {
        metaData = new MetaData(
                setPolicy(call),
                setPeople(call),
                setCoverages(call));
      } else {
        throw new CPMException(call.getString(5), HttpStatus.INTERNAL_SERVER_ERROR, null);
      }
      conn.commit();
      conn.setAutoCommit(true);
    } catch (SQLException | NamingException e) {
      throw new CPMException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
    }
    return metaData;
  }

  private List<MetadataPolicy> setPolicy(CallableStatement call) throws SQLException {
    List<MetadataPolicy> policyList = new ArrayList<>();
    MetadataPolicy metadataPolicy;
    ResultSet rsPolicy = (ResultSet) call.getObject(1);
    while (rsPolicy.next()) {
      metadataPolicy =
          new MetadataPolicy(
              rsPolicy.getString(CPMConstants.DAO_TEMPLATE_NAME),
              rsPolicy.getString(CPMConstants.DAO_SERVICE_NAME),
              rsPolicy.getString(CPMConstants.DAO_TYPE));
      policyList.add(metadataPolicy);
    }
    rsPolicy.close();
    return policyList;
  }

  private List<MetadataPerson> setPeople(CallableStatement call) throws SQLException {
    List<MetadataPerson> peopleList = new ArrayList<>();
    MetadataPerson metadataPerson;
    ResultSet rsPeople = (ResultSet) call.getObject(2);
    while (rsPeople.next()) {
      metadataPerson =
          new MetadataPerson(
              rsPeople.getString(CPMConstants.DAO_TEMPLATE_NAME),
              rsPeople.getString(CPMConstants.DAO_SERVICE_NAME),
              rsPeople.getString(CPMConstants.DAO_TYPE),
              rsPeople.getString(CPMConstants.DAO_TYPE_PERSON));
      peopleList.add(metadataPerson);
    }
    rsPeople.close();
    return peopleList;
  }

  private List<MetaDataCoverage> setCoverages(CallableStatement call) throws SQLException {
    List<MetaDataCoverage> coverageList = new ArrayList<>();
    MetaDataCoverage metaDataCoverage;
    ResultSet rsCoverage = (ResultSet) call.getObject(3);
    while (rsCoverage.next()) {
      metaDataCoverage =
          new MetaDataCoverage(
              rsCoverage.getString(CPMConstants.DAO_TEMPLATE_NAME),
              rsCoverage.getString(CPMConstants.DAO_SERVICE_NAME),
              rsCoverage.getString(CPMConstants.DAO_TYPE),
              rsCoverage.getString(CPMConstants.DAO_TYPE_PERSON));
      coverageList.add(metaDataCoverage);
    }
    rsCoverage.close();
    return coverageList;
  }
}
