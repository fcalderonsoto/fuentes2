package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class VectorParameterValue {

  @JsonProperty("vectorParameterKey")
  private String vectorParameterKey;

  @JsonProperty("vectorParameter")
  private List<VectorParameter> vectorParameter;

  public VectorParameterValue(String vectorParameterKey, List<VectorParameter> vectorParameter) {
    this.vectorParameterKey = vectorParameterKey;
    this.vectorParameter = vectorParameter;
  }
}
