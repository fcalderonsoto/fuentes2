package cl.cardif.policy.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestCPM {
	@JsonProperty("check_policy_merge_REQ")
	private CheckPolicyMergeREQ checkPolicyMergeREQ;

	public CheckPolicyMergeREQ getCheckPolicyMergeREQ() {
		return checkPolicyMergeREQ;
	}

}
