package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SectionParameter {

	@JsonProperty("sectionParameterKey")
	private String sectionParameterKey;
	
	@JsonProperty("sectionParameterCondition")
	private String sectionParameterCondition;
	
	public SectionParameter(String sectionParameterKey, String sectionParameterCondition ) {
		this.sectionParameterKey = sectionParameterKey;
		this.sectionParameterCondition=sectionParameterCondition;
	}
}
