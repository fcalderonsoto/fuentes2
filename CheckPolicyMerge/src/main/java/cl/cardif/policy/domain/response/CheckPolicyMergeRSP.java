package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckPolicyMergeRSP {
  @JsonProperty("Message")
  private Message message;

  @JsonProperty("MetadataList")
  private MetadataList metadataList;

  public CheckPolicyMergeRSP(Message message, MetadataList metadataList) {
    this.message = message;
    this.metadataList = metadataList;
  }
}
