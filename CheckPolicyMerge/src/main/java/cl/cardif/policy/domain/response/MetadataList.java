package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MetadataList {

    @JsonProperty("TextParameterList")
    private List<TextParameter> textParameterList;

    @JsonProperty("VectorParameterList")
    private VectorParameterList vectorParameterList;
    
    @JsonProperty("SectionParameterList")
    private SectionParameterList sectionParameterList;

    public MetadataList(List<TextParameter> textParameterList, VectorParameterList vectorParameterList, SectionParameterList sectionParameterList) {
        this.textParameterList = textParameterList;
        this.vectorParameterList = vectorParameterList;
        this.sectionParameterList = sectionParameterList;
    }

}
