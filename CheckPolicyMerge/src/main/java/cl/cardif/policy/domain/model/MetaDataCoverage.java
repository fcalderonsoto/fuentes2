package cl.cardif.policy.domain.model;

public class MetaDataCoverage {
  private String templateTagName;
  private String serviceTagName;
  private String type;
  private String typePerson;

  public MetaDataCoverage(
      final String templateTagName,
      final String serviceTagName,
      final String type,
      final String typePerson) {
    this.templateTagName = templateTagName;
    this.serviceTagName = serviceTagName;
    this.type = type;
    this.typePerson = typePerson;
  }

  public String getTemplateTagName() {
    return templateTagName;
  }

  public String getServiceTagName() {
    return serviceTagName;
  }

  public String getType() {
    return type;
  }

  public String getTypePerson() {
    return typePerson;
  }
}
