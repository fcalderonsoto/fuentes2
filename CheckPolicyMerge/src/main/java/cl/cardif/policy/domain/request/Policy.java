package cl.cardif.policy.domain.request;

public class Policy {

  private String operationNumber;
  private String eMail;
  private String authorizeSendByEmail;
  private String partnerDocumentNumber;
  private String partnerName;
  private String number;
  private String certificateCode;
  private String effectiveStartDate;
  private String effectiveEndDate;
  private String calculatedEffectiveDate;
  private String emissionDate;
  private String saleDate;
  private String period;
  private String statusChangeDate;
  private String paymentPeriod;
  private long numberOfInsured;
  private String affectedPremium;
  private String exemptedPremium;
  private String netPremium;
  private String tax;
  private String grossPremium;
  private String monthlyGrossPremium;
  private String productCode;
  private String productDescription;
  private String planCode;
  private String planDescription;
  private String paymentMethod;
  private String certificatePlan;
  private String insuredAddress;
  private String insuredAddressNumber;
  private String insuredAddressCommune;
  private String insuredAddressCity;
  private String insuredPhoneNumber;
  private String insuredEmail;
  private String creditType;
  private long creditPeriod;
  private String creditAmount;
  private String totalAffectedPremium;
  private String totalExemptedPremium;
  private String totalNetPremium;
  private String totalPremiumTax;
  private String totalGrossPremium;
  private String insuredCapital;
  private String freeField;
  private String mutual;
  private String groupPolicy;
  private long telemarketingIndex;
  private String telemarketingCampaign;
  private long maxStayAge;
  private String capital;
  private String amountDue;
  private String insuredType;
  private String lastPaymentMonth;
  private String insuredRecordLastPaymentMonth;
  private String currencyConvertionDate;
  private String operationEndDate;
  private String opevstatusDate;
  private String opecstatusDate;
  private String creditStartDate;
  private String cartolaPremiumInPesos;
  private String cartolaPremiumInUF;
  private String insuredRecordPremiumInPesos;
  private String insuredRecordStatus;
  private String insuredRecordCurrencyConversionDate;
  private String calculatedPremium;
  private String insuredRecordCalculatedTax;
  private String insuredRecordCalculatedPremium;
  private String premiumPesos;
  private String productType;
  private String searchDate;
  private String extraData1;
  private String extraData2;
  private String extraData3;
  private String extraData4;
  private String extraData5;
  private String extraData6;
  private String extraData7;
  private String extraData8;
  private String extraData9;
  private String extraData10;

  public String getOperationNumber() {
    return operationNumber;
  }

  public String geteMail() {
    return eMail;
  }

  public String getAuthorizeSendByEmail() {
    return authorizeSendByEmail;
  }

  public String getPartnerDocumentNumber() {
    return partnerDocumentNumber;
  }

  public String getPartnerName() {
    return partnerName;
  }

  public String getNumber() {
    return number;
  }

  public String getCertificateCode() {
    return certificateCode;
  }

  public String getEffectiveStartDate() {
    return effectiveStartDate;
  }

  public String getEffectiveEndDate() {
    return effectiveEndDate;
  }

  public String getCalculatedEffectiveDate() {
    return calculatedEffectiveDate;
  }

  public String getEmissionDate() {
    return emissionDate;
  }

  public String getSaleDate() {
    return saleDate;
  }

  public String getPeriod() {
    return period;
  }

  public String getStatusChangeDate() {
    return statusChangeDate;
  }

  public String getPaymentPeriod() {
    return paymentPeriod;
  }

  public long getNumberOfInsured() {
    return numberOfInsured;
  }

  public String getAffectedPremium() {
    return affectedPremium;
  }

  public String getExemptedPremium() {
    return exemptedPremium;
  }

  public String getNetPremium() {
    return netPremium;
  }

  public String getTax() {
    return tax;
  }

  public String getGrossPremium() {
    return grossPremium;
  }

  public String getMonthlyGrossPremium() {
    return monthlyGrossPremium;
  }

  public String getProductCode() {
    return productCode;
  }

  public String getProductDescription() {
    return productDescription;
  }

  public String getPlanCode() {
    return planCode;
  }

  public String getPlanDescription() {
    return planDescription;
  }

  public String getPaymentMethod() {
    return paymentMethod;
  }

  public String getCertificatePlan() {
    return certificatePlan;
  }

  public String getInsuredAddress() {
    return insuredAddress;
  }

  public String getInsuredAddressNumber() {
    return insuredAddressNumber;
  }

  public String getInsuredAddressCommune() {
    return insuredAddressCommune;
  }

  public String getInsuredAddressCity() {
    return insuredAddressCity;
  }

  public String getInsuredPhoneNumber() {
    return insuredPhoneNumber;
  }

  public String getInsuredEmail() {
    return insuredEmail;
  }

  public String getCreditType() {
    return creditType;
  }

  public long getCreditPeriod() {
    return creditPeriod;
  }

  public String getCreditAmount() {
    return creditAmount;
  }

  public String getTotalAffectedPremium() {
    return totalAffectedPremium;
  }

  public String getTotalExemptedPremium() {
    return totalExemptedPremium;
  }

  public String getTotalNetPremium() {
    return totalNetPremium;
  }

  public String getTotalPremiumTax() {
    return totalPremiumTax;
  }

  public String getTotalGrossPremium() {
    return totalGrossPremium;
  }

  public String getInsuredCapital() {
    return insuredCapital;
  }

  public String getFreeField() {
    return freeField;
  }

  public String getMutual() {
    return mutual;
  }

  public String getGroupPolicy() {
    return groupPolicy;
  }

  public long getTelemarketingIndex() {
    return telemarketingIndex;
  }

  public String getTelemarketingCampaign() {
    return telemarketingCampaign;
  }

  public long getMaxStayAge() {
    return maxStayAge;
  }

  public String getCapital() {
    return capital;
  }

  public String getAmountDue() {
    return amountDue;
  }

  public String getInsuredType() {
    return insuredType;
  }

  public String getLastPaymentMonth() {
    return lastPaymentMonth;
  }

  public String getInsuredRecordLastPaymentMonth() {
    return insuredRecordLastPaymentMonth;
  }

  public String getCurrencyConvertionDate() {
    return currencyConvertionDate;
  }

  public String getOperationEndDate() {
    return operationEndDate;
  }

  public String getopevstatusDate() {
    return opevstatusDate;
  }

  public String getopecstatusDate() {
    return opecstatusDate;
  }

  public String getCreditStartDate() {
    return creditStartDate;
  }

  public String getCartolaPremiumInPesos() {
    return cartolaPremiumInPesos;
  }

  public String getCartolaPremiumInUF() {
    return cartolaPremiumInUF;
  }

  public String getInsuredRecordPremiumInPesos() {
    return insuredRecordPremiumInPesos;
  }

  public String getInsuredRecordStatus() {
    return insuredRecordStatus;
  }

  public String getInsuredRecordCurrencyConversionDate() {
    return insuredRecordCurrencyConversionDate;
  }

  public String getCalculatedPremium() {
    return calculatedPremium;
  }

  public String getInsuredRecordCalculatedTax() {
    return insuredRecordCalculatedTax;
  }

  public String getInsuredRecordCalculatedPremium() {
    return insuredRecordCalculatedPremium;
  }

  public String getPremiumPesos() {
    return premiumPesos;
  }

  public String getProductType() {
    return productType;
  }

  public String getSearchDate() {
    return searchDate;
  }

  public String getExtraData1() {
    return extraData1;
  }

  public String getExtraData2() {
    return extraData2;
  }

  public String getExtraData3() {
    return extraData3;
  }

  public String getExtraData4() {
    return extraData4;
  }

  public String getExtraData5() {
    return extraData5;
  }

  public String getExtraData6() {
    return extraData6;
  }

  public String getExtraData7() {
    return extraData7;
  }

  public String getExtraData8() {
    return extraData8;
  }

  public String getExtraData9() {
    return extraData9;
  }

  public String getExtraData10() {
    return extraData10;
  }
}
