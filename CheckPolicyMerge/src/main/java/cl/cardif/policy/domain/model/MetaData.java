package cl.cardif.policy.domain.model;

import java.util.List;

public class MetaData {

  private List<MetadataPolicy> policy;
  private List<MetadataPerson> people;
  private List<MetaDataCoverage> coverages;

  public MetaData(
      final List<MetadataPolicy> policy,
      final List<MetadataPerson> people,
      final List<MetaDataCoverage> coverages) {
    this.policy = policy;
    this.people = people;
    this.coverages = coverages;
  }

  public List<MetadataPolicy> getPolicy() {
    return policy;
  }

  public List<MetadataPerson> getPeople() {
    return people;
  }

  public List<MetaDataCoverage> getCoverages() {
    return coverages;
  }
}
