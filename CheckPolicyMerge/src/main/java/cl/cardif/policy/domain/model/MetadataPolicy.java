package cl.cardif.policy.domain.model;

public class MetadataPolicy {

  private String templateTagName;
  private String serviceTagName;
  private String type;

  public MetadataPolicy(
      final String templateTagName,
      final String serviceTagName,
      final String type) {
    this.templateTagName = templateTagName;
    this.serviceTagName = serviceTagName;
    this.type = type;
  }

  public String getTemplateTagName() {
    return templateTagName;
  }

  public String getServiceTagName() {
    return serviceTagName;
  }

  public String getType() {
    return type;
  }

}
