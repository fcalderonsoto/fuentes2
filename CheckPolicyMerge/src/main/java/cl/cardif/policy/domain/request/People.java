package cl.cardif.policy.domain.request;

public class People {

  private long code;
  private long insuredCode;
  private String documentNumber;
  private String documentVerificationDigit;
  private String name;
  private long typeSequentialNumber;
  private String typeCode;
  private String type;
  private String address;
  private String addressCommune;
  private String addressCity;
  private String phoneNumber;
  private String email;
  private String birthDate;
  private String addressType;
  private String mobilePhoneNumber;
  private String activity;
  private String percentage;
  private String relationship;
  private String condition;
  private String validity;

  public long getCode() {
    return code;
  }

  public long getInsuredCode() {
    return insuredCode;
  }

  public String getDocumentNumber() {
    return documentNumber;
  }

  public String getDocumentVerificationDigit() {
    return documentVerificationDigit;
  }

  public String getName() {
    return name;
  }

  public long getTypeSequentialNumber() {
    return typeSequentialNumber;
  }

  public String getTypeCode() {
    return typeCode;
  }

  public String getType() {
    return type;
  }

  public String getAddress() {
    return address;
  }

  public String getAddressCommune() {
    return addressCommune;
  }

  public String getAddressCity() {
    return addressCity;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public String getEmail() {
    return email;
  }

  public String getBirthDate() {
    return birthDate;
  }

  public String getAddressType() {
    return addressType;
  }

  public String getMobilePhoneNumber() {
    return mobilePhoneNumber;
  }

  public String getActivity() {
    return activity;
  }

  public String getPercentage() {
    return percentage;
  }

  public String getRelationship() {
    return relationship;
  }

  public String getCondition() {
    return condition;
  }

  public String getValidity() {
    return validity;
  }
}
