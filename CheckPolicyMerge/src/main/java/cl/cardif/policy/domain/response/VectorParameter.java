package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VectorParameter {

  @JsonProperty("vectorParameterIndex")
  private String vectorParameterIndex;

  @JsonProperty("vectorParameterValue")
  private String vectorParameterValue;

  public VectorParameter(String vectorParameterIndex, String vectorParameterValue) {
    this.vectorParameterIndex = vectorParameterIndex;
    this.vectorParameterValue = vectorParameterValue;
  }
}
