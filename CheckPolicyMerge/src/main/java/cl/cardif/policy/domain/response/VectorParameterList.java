package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class VectorParameterList {

  @JsonProperty("VectorParameterValue")
  private List<VectorParameterValue> vectorParameterValueList;

  public VectorParameterList(List<VectorParameterValue> vectorParameterValueList) {
    this.vectorParameterValueList = vectorParameterValueList;
  }
}
