package cl.cardif.policy.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CheckPolicyMergeREQ {

  @JsonProperty("Policy")
  private Policy policy;

  @JsonProperty("Person")
  private List<People> peopleList;

  @JsonProperty("Coverage")
  private List<Coverage> coverageList;

  public Policy getPolicy() {
    return policy;
  }

  public List<People> getPeopleList() {
    return peopleList;
  }

  public List<Coverage> getCoverageList() {
    return coverageList;
  }
}
