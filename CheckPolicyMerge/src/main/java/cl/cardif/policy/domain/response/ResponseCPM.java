package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseCPM {

  @JsonProperty("check_policy_merge_RSP")
  private CheckPolicyMergeRSP checkPolicyMergeRSP;

  public ResponseCPM(CheckPolicyMergeRSP checkPolicyMergeRSP) {
    this.checkPolicyMergeRSP = checkPolicyMergeRSP;
  }

}
