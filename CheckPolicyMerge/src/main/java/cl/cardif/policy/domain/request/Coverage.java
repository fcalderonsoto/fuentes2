package cl.cardif.policy.domain.request;

import java.math.BigDecimal;

public class Coverage {

  private String proposalCode;
  private long personType;
  private String personName;
  private long personCode;
  private long insuredCode;
  private String code;
  private String description;
  private BigDecimal capital;
  private String insuredRecordPremiumInPesos;
  private String percentage;
  private BigDecimal netValue;
  private BigDecimal exemptedValue;
  private BigDecimal affectedValue;
  private BigDecimal taxValue;
  private BigDecimal grossValue;

  public String getPersonName() {
    return personName;
  }

  public String getProposalCode() {
    return proposalCode;
  }

  public long getPersonType() {
    return personType;
  }

  public long getPersonCode() {
    return personCode;
  }

  public long getInsuredCode() {
    return insuredCode;
  }

  public String getCode() {
    return code;
  }

  public String getDescription() {
    return description;
  }

  public BigDecimal getCapital() {
    return capital;
  }

  public String getInsuredRecordPremiumInPesos() {
    return insuredRecordPremiumInPesos;
  }

  public String getPercentage() {
    return percentage;
  }

  public BigDecimal getNetValue() {
    return netValue;
  }

  public BigDecimal getExemptedValue() {
    return exemptedValue;
  }

  public BigDecimal getAffectedValue() {
    return affectedValue;
  }

  public BigDecimal getTaxValue() {
    return taxValue;
  }

  public BigDecimal getGrossValue() {
    return grossValue;
  }
}
