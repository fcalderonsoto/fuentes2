package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TextParameter {

	@JsonProperty("key")
	private String key;

	@JsonProperty("value")
	private String value;

	@JsonProperty("fieldType")
	private String fieldType;

	public TextParameter(String key, String value, String fieldType) {
		this.key = key;
		this.value = value;
		this.fieldType = fieldType;
	}

}