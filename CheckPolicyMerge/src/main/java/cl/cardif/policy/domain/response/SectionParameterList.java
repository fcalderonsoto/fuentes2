package cl.cardif.policy.domain.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SectionParameterList {

	 @JsonProperty("SectionParameter")
	  private List<SectionParameter> sectionParameter;

	  public SectionParameterList(List<SectionParameter> sectionParameter) {
	    this.sectionParameter = sectionParameter;
	  }
}
