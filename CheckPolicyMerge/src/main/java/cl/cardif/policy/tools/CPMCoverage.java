package cl.cardif.policy.tools;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.cardif.policy.domain.model.MetaData;
import cl.cardif.policy.domain.request.CheckPolicyMergeREQ;
import cl.cardif.policy.domain.request.Coverage;
import cl.cardif.policy.domain.response.VectorParameter;
import cl.cardif.policy.domain.response.VectorParameterValue;

@Service
public class CPMCoverage {

  @Autowired CPMMaps cpmMaps;

  public void generateCoverage(
      MetaData metaData,
      CheckPolicyMergeREQ checkPolicyMergeREQ,
      List<VectorParameterValue> vectorParameterValueList) {
    List<Coverage> insuredMain = new ArrayList<>();
    List<Coverage> insuredAdditional = new ArrayList<>();
    List<Coverage> insuredAdditionalTemp;
    IntStream.range(0, checkPolicyMergeREQ.getCoverageList().size())
        .forEach(
            k -> {
            	addMainInsuredAndAdditional(k, checkPolicyMergeREQ, insuredMain, insuredAdditional);
            });
    if (!insuredMain.isEmpty()) {
      createListCoverage(
          metaData, insuredMain, vectorParameterValueList, CPMConstants.MAIN_INSURED);
    }
    if (!insuredAdditional.isEmpty()) {
      for (int i = 1; i < 11; i++) {
        insuredAdditionalTemp = new ArrayList<>();
        for (int j = 0; j < insuredAdditional.size(); j++) {
          if (insuredAdditional.get(j).getPersonType() - 1 == i) {
            insuredAdditionalTemp.add(insuredAdditional.get(j));
          }
        }
        if(!insuredAdditionalTemp.isEmpty()) {
        createListCoverage(
            metaData,
            insuredAdditionalTemp,
            vectorParameterValueList,
            CPMConstants.ADDITIONAL_INSURED);
        }
      }
    }
  }
 public void addMainInsuredAndAdditional (int k,CheckPolicyMergeREQ checkPolicyMergeREQ,  List<Coverage> insuredMain,List<Coverage> insuredAdditional) {
	 if (checkPolicyMergeREQ
             .getCoverageList()
             .get(k)
             .getPersonName()
             .equalsIgnoreCase(CPMConstants.MAIN_INSURED)) {
           insuredMain.add(checkPolicyMergeREQ.getCoverageList().get(k));
         } else if (checkPolicyMergeREQ
             .getCoverageList()
             .get(k)
             .getPersonName()
             .equalsIgnoreCase(CPMConstants.ADDITIONAL_INSURED)) {
           insuredAdditional.add(checkPolicyMergeREQ.getCoverageList().get(k));
         }
 }
  private void createListCoverage(
      MetaData metaData,
      List<Coverage> insured,
      List<VectorParameterValue> vectorParameterValueList,
      String typePerson) {
    for (int i = 0; i < metaData.getCoverages().size(); i++) {
      if (metaData.getCoverages().get(i).getType().equalsIgnoreCase(CPMConstants.LIST)
          && metaData.getCoverages().get(i).getTypePerson().equalsIgnoreCase(typePerson)) {
    	  addVectorParameterListCoverage(i, metaData, insured, vectorParameterValueList, typePerson);
      }
    }
  }
  private void addVectorParameterListCoverage(int i, MetaData metaData,List<Coverage> insured,List<VectorParameterValue> vectorParameterValueList, String typePerson) {
	  VectorParameterValue vectorParameterValue;
	  for (Field f : insured.get(0).getClass().getDeclaredFields()) {
          if (metaData.getCoverages().get(i).getTemplateTagName() != null
              && f.getName().equalsIgnoreCase(metaData.getCoverages().get(i).getServiceTagName())
              && metaData.getCoverages().get(i).getTypePerson().equalsIgnoreCase(typePerson)) {
            vectorParameterValue =
                new VectorParameterValue(
                    typePerson.equalsIgnoreCase(CPMConstants.ADDITIONAL_INSURED)
                    	?createTagAdditionalInsured(
                    			metaData.getCoverages().get(i).getTemplateTagName(),
                    			insured.get(0).getPersonType())
                    			:metaData.getCoverages().get(i).getTemplateTagName(),
                    vectorParameterCoverage(insured, f.getName()));
            vectorParameterValueList.add(vectorParameterValue);
          }
        }
  }
  private String createTagAdditionalInsured(String templateTagName, long typePerson) {
      return templateTagName.replace(CPMConstants.TAG_ADDITIONAL, String.valueOf(typePerson - 1));
  }

  private List<VectorParameter> vectorParameterCoverage(List<Coverage> coverageList, String field) {
    List<VectorParameter> vectorParameterList = new ArrayList<>();
    VectorParameter vectorParameter;
    List<Map<String, String>> listMapCoverage = cpmMaps.createMapCoverage(coverageList);
    for (int i = 0; i < listMapCoverage.size(); i++) {
      vectorParameter =
          new VectorParameter(
              String.valueOf(i + 1),
              listMapCoverage.get(i).get(field) != null
                  ? listMapCoverage.get(i).get(field)
                  : CPMConstants.EMPTY);
      vectorParameterList.add(vectorParameter);
    }
    return vectorParameterList;
  }
}
