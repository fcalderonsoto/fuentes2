package cl.cardif.policy.tools;

import cl.cardif.policy.domain.model.MetaData;
import cl.cardif.policy.domain.request.CheckPolicyMergeREQ;
import cl.cardif.policy.domain.response.TextParameter;

import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

@Service
public class CPMPolicy {
    public void generatePolicy(
            MetaData metaData,
            CheckPolicyMergeREQ checkPolicyMergeREQ,
            List<TextParameter> textParameterList,
            int i,
            Map<String, String> mapPolicy) {
        TextParameter textParameter;
        for (Field f : checkPolicyMergeREQ.getPolicy().getClass().getDeclaredFields()) {
            if (metaData.getPolicy().get(i).getTemplateTagName() != null
                    && f.getName().equalsIgnoreCase(metaData.getPolicy().get(i).getServiceTagName())) {
                textParameter =
                        new TextParameter(
                                metaData.getPolicy().get(i).getTemplateTagName(),
                                mapPolicy.get(f.getName()) != null
                                        ? mapPolicy.get(f.getName())
                                        : CPMConstants.EMPTY,
                                metaData.getPolicy().get(i).getType());
                textParameterList.add(textParameter);
              
            }
        }
    }
}
