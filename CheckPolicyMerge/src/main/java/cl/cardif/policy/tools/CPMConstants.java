package cl.cardif.policy.tools;

public class CPMConstants {

  public static final String JAVA_COMP = "java:comp/env";
  public static final String JNDI_POLI = "jdbc/POLIZA";

  public static final String FORMAT_DATE = "dd/MM/yyyy";
  public static final String PRC_GET_MERGE = "{call PCK_TRAZAS.PRC_GET_HOMOLOGACION(?,?,?,?,?)}";

  public static final String REGEX_DATE =
      "^(0?[1-9]|[12][0-9]|3[01])[\\/\\-](0?[1-9]|1[012])[\\/\\-]\\d{4}$";
  public static final String REGEX_DNI = "^[0-9Kk]+$";

  public static final String EMPTY = "";
  public static final int NUMBER_ZERO = 0;
  public static final int NUMBER_ONE = 1;
  public static final int NUMBER_TWO = 2;
  public static final int NUMBER_EIGHT = 8;
  public static final int NUMBER_TEN = 10;
  public static final int NUMBER_ELEVEN = 11;
  public static final String DVK = "K";
  public static final String SLASH = "/";
  public static final String MAIN_INSURED = "ASEGURADO TITULAR";
  public static final String PAYER = "PAGADOR";
  public static final String CONTRACTOR = "CONTRATANTE";
  public static final String ADDITIONAL_INSURED = "ASEGURADOS ADICIONALES";
  public static final String MAIN_BENEFICIARIES = "BENEFICIARIOS DEL TITULAR";
  public static final String BENEFICIARIES_ADDITIONAL = "BENEFICIARIOS ADICIONAL";
  
  public static final String DAO_TEMPLATE_NAME = "NOMBRE_TAG_PLANTILLA";
  public static final String DAO_SERVICE_NAME = "NOMBRE_TAG_SERVICIO";
  public static final String DAO_TYPE = "TIPO";
  public static final String DAO_TYPE_PERSON = "TIPO_PER";
  public static final String TAG_ADDITIONAL = "XXX";
  public static final String TEXT = "TEXTO";
  public static final String LIST = "LISTA";
}
