package cl.cardif.policy.tools;

import cl.cardif.policy.domain.request.Coverage;
import cl.cardif.policy.domain.request.People;
import cl.cardif.policy.domain.request.Policy;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CPMMaps {
	 String insuredCode="insuredCode";
	 String percentage="percentage";
	 String capital = "capital";
  public Map<String, String> createMapPerson(People people) {
	 
    Map<String, String> map = new HashMap<>();
    if( people.getCode()!=0) {
    	map.put("code", String.valueOf(people.getCode()));
    	
    }else {
    	map.put("code", "");	
    }
    
    map.put(insuredCode, String.valueOf(people.getInsuredCode()));
    map.put("documentNumber", people.getDocumentNumber());
    map.put("documentVerificationDigit", people.getDocumentVerificationDigit());
    map.put("name", people.getName());
    map.put("typeSequentialNumber", String.valueOf(people.getTypeSequentialNumber()));
    map.put("typeCode", people.getTypeCode());
    map.put("type", people.getType());
    map.put("address", people.getAddress());
    map.put("addressCommune", people.getAddressCommune());
    map.put("addressCity", people.getAddressCity());
    map.put("phoneNumber", people.getPhoneNumber());
    map.put("email", people.getEmail());
    map.put("birthDate", people.getBirthDate());
    map.put("addressType", people.getAddressType());
    map.put("mobilePhoneNumber", people.getMobilePhoneNumber());
    map.put("activity", people.getActivity());
    map.put(percentage, people.getPercentage());
    map.put("relationShip", people.getRelationship());
    map.put("condition", people.getCondition());
    map.put("validity", people.getValidity());
    return map;
  }

  public List<Map<String, String>> createMapPersonBF(List<People> people) {
	    List<Map<String, String>> listMap = new ArrayList<>();
	    for (People person : people) {
	    	Map<String, String> map = new HashMap<>();
	    	map.put("code", String.valueOf(person.getCode()));
	 	    map.put(insuredCode, String.valueOf(person.getInsuredCode()));
	 	    map.put("documentNumber", person.getDocumentNumber());
	 	    map.put("documentVerificationDigit", person.getDocumentVerificationDigit());
	 	    map.put("name", person.getName());
	 	    map.put("typeSequentialNumber", String.valueOf(person.getTypeSequentialNumber()));
	 	    map.put("typeCode", person.getTypeCode());
	 	    map.put("type", person.getType());
	 	    map.put("address", person.getAddress());
	 	    map.put("addressCommune", person.getAddressCommune());
	 	    map.put("addressCity", person.getAddressCity());
	 	    map.put("phoneNumber", person.getPhoneNumber());
	 	    map.put("email", person.getEmail());
	 	    map.put("birthDate", person.getBirthDate());
	 	    map.put("addressType", person.getAddressType());
	 	    map.put("mobilePhoneNumber", person.getMobilePhoneNumber());
	 	    map.put("activity", person.getActivity());
	 	    map.put(percentage, person.getPercentage());
	 	    map.put("relationship", person.getRelationship());
	 	    map.put("condition", person.getCondition());
	 	    map.put("validity", person.getValidity());
	 	   listMap.add(map);
	    }
	    return listMap;
	  }
  
  public List<Map<String, String>> createMapCoverage(List<Coverage> coverageList) {
    List<Map<String, String>> listMap = new ArrayList<>();
    for (Coverage coverage : coverageList) {
      Map<String, String> map = new HashMap<>();
      map.put("proposalCode", coverage.getProposalCode());
      map.put("personType", String.valueOf(coverage.getPersonType()));
      map.put("personCode", String.valueOf(coverage.getPersonCode()));
      map.put(insuredCode, String.valueOf(coverage.getInsuredCode()));
      map.put("code", coverage.getCode());
      map.put("description", coverage.getDescription());
      if(coverage.getCapital().stripTrailingZeros().doubleValue() != 0) {
    	  map.put(capital, String.valueOf(coverage.getCapital())); 
      }else {
    	  map.put(capital,"");
      }
      
      map.put(
          "insuredRecordPremiumInPesos", String.valueOf(coverage.getInsuredRecordPremiumInPesos()));
      map.put("percentage", coverage.getPercentage());
      map.put("netValue", String.valueOf(coverage.getNetValue()));
      map.put("exemptedValue", String.valueOf(coverage.getExemptedValue()));
      map.put("affectedValue", String.valueOf(coverage.getAffectedValue()));
      map.put("taxValue", String.valueOf(coverage.getTaxValue()));
      if(coverage.getGrossValue().stripTrailingZeros().doubleValue() != 0) {
    	  map.put("grossValue", String.valueOf(coverage.getGrossValue()));
      }else {
    	  map.put("grossValue","");
      }
      
      listMap.add(map);
    }
    return listMap;
  }

  public Map<String, String> createMapPolicy(Policy policy) {
    Map<String, String> map = new HashMap<>();
    map.put("operationNumber", policy.getOperationNumber());
    map.put("eMail", policy.geteMail());
    map.put("authorizeSendByEmail", policy.getAuthorizeSendByEmail());
    map.put("partnerDocumentNumber", policy.getPartnerDocumentNumber());
    map.put("partnerName", policy.getPartnerName());
    map.put("number", policy.getNumber());
    map.put("certificateCode", policy.getCertificateCode());
    map.put("effectiveStartDate", policy.getEffectiveStartDate());
    map.put("effectiveEndDate", policy.getEffectiveEndDate());
    map.put("calculatedEffectiveDate", policy.getCalculatedEffectiveDate());
    map.put("emissionDate", policy.getEmissionDate());
    map.put("saleDate", policy.getSaleDate());
    map.put("period", policy.getPeriod());
    map.put("statusChangeDate", policy.getStatusChangeDate());
    map.put("paymentPeriod", policy.getPaymentPeriod());
    map.put("numberOfInsured", String.valueOf(policy.getNumberOfInsured()));
    map.put("affectedPremium", policy.getAffectedPremium());
    map.put("exemptedPremium", policy.getExemptedPremium());
    map.put("netPremium", policy.getNetPremium());
    map.put("tax", policy.getTax());
    map.put("grossPremium", policy.getGrossPremium());
    map.put("monthlyGrossPremium", policy.getMonthlyGrossPremium());
    map.put("productCode", policy.getProductCode());
    map.put("productDescription", policy.getProductDescription());
    map.put("planCode", policy.getPlanCode());
    map.put("planDescription", policy.getPlanDescription());
    map.put("paymentMethod", policy.getPaymentMethod());
    map.put("certificatePlan", policy.getCertificatePlan());
    map.put("insuredAddress", policy.getInsuredAddress());
    map.put("insuredAddressNumber", policy.getInsuredAddressNumber());
    map.put("insuredAddressCommune", policy.getInsuredAddressCommune());
    map.put("insuredAddressCity", policy.getInsuredAddressCity());
    map.put("insuredPhoneNumber", policy.getInsuredPhoneNumber());
    map.put("insuredEmail", policy.getInsuredEmail());
    map.put("creditType", policy.getCreditType());
    map.put("creditPeriod", String.valueOf(policy.getCreditPeriod()));
    map.put("creditAmount", String.valueOf(policy.getCreditAmount()));
    map.put("totalAffectedPremium", String.valueOf(policy.getTotalAffectedPremium()));
    map.put("totalExemptedPremium", String.valueOf(policy.getTotalExemptedPremium()));
    map.put("totalNetPremium", String.valueOf(policy.getTotalNetPremium()));
    map.put("totalPremiumTax", String.valueOf(policy.getTotalPremiumTax()));
    map.put("totalGrossPremium", String.valueOf(policy.getTotalGrossPremium()));
    map.put("insuredCapital", policy.getInsuredCapital());
    map.put("freeField", policy.getFreeField());
    map.put("mutual", policy.getMutual());
    map.put("groupPolicy", policy.getGroupPolicy());
    map.put("telemarketingIndex", String.valueOf(policy.getTelemarketingIndex()));
    map.put("telemarketingCampaign", policy.getTelemarketingCampaign());
    map.put("maxStayAge", String.valueOf(policy.getMaxStayAge()));
    map.put("capital", policy.getCapital());
    map.put("amountDue", policy.getAmountDue());
    map.put("insuredType", policy.getInsuredType());
    map.put("lastPaymentMonth", policy.getLastPaymentMonth());
    map.put("insuredRecordLastPaymentMonth", policy.getInsuredRecordLastPaymentMonth());
    map.put("currencyConvertionDate", policy.getCurrencyConvertionDate());
    map.put("operationEndDate", policy.getOperationEndDate());
    map.put("opevstatusDate", policy.getopevstatusDate());
    map.put("opecstatusDate", policy.getopecstatusDate());
    map.put("creditStartDate", policy.getCreditStartDate());
    map.put("cartolaPremiumInPesos", String.valueOf(policy.getCartolaPremiumInPesos()));
    map.put("cartolaPremiumInUF", String.valueOf(policy.getCartolaPremiumInUF()));
    map.put("insuredRecordPremiumInPesos", policy.getInsuredRecordPremiumInPesos());
    map.put("insuredRecordStatus", policy.getInsuredRecordStatus());
    map.put("insuredRecordCurrencyConversionDate", policy.getInsuredRecordCurrencyConversionDate());
    map.put("calculatedPremium", policy.getCalculatedPremium());
    map.put("insuredRecordCalculatedTax", policy.getInsuredRecordCalculatedTax());
    map.put("insuredRecordCalculatedPremium", policy.getInsuredRecordCalculatedPremium());
    map.put("premiumPesos", policy.getPremiumPesos());
    map.put("productType", policy.getProductType());
    map.put("searchDate", policy.getSearchDate());
    map.put("extraData1", policy.getExtraData1());
    map.put("extraData2", policy.getExtraData2());
    map.put("extraData3", policy.getExtraData3());
    map.put("extraData4", policy.getExtraData4());
    map.put("extraData5", policy.getExtraData5());
    map.put("extraData6", policy.getExtraData6());
    map.put("extraData7", policy.getExtraData7());
    map.put("extraData8", policy.getExtraData8());
    map.put("extraData9", policy.getExtraData9());
    map.put("extraData10", policy.getExtraData10());
    return map;
  }
}
