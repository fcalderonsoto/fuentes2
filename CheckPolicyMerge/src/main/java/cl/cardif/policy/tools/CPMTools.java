package cl.cardif.policy.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.cardif.policy.domain.model.MetaData;
import cl.cardif.policy.domain.request.CheckPolicyMergeREQ;
import cl.cardif.policy.domain.response.MetadataList;
import cl.cardif.policy.domain.response.SectionParameter;
import cl.cardif.policy.domain.response.SectionParameterList;
import cl.cardif.policy.domain.response.TextParameter;
import cl.cardif.policy.domain.response.VectorParameterList;
import cl.cardif.policy.domain.response.VectorParameterValue;

@Service
public class CPMTools {

  @Autowired CPMMaps cpmMaps;

  @Autowired CPMCoverage cpmCoverage;

  @Autowired CPMPerson cpmPerson;

  @Autowired CPMPolicy cpmPolicy;

  public boolean mandatoryInput(CheckPolicyMergeREQ checkPolicyMergeREQ) {
    return checkPolicyMergeREQ.getPolicy() != null
        && !checkPolicyMergeREQ.getCoverageList().isEmpty();
  }

  public MetadataList generateOutput(MetaData metaData, CheckPolicyMergeREQ checkPolicyMergeREQ) {
    MetadataList metadataList;
    List<TextParameter> textParameterList = new ArrayList<>();
    List<VectorParameterValue> vectorParameterValueList = new ArrayList<>();
    List<SectionParameter> sectionParameterValueList = new ArrayList<>();
    VectorParameterList vectorParameterList;
    vectorParameterList = new VectorParameterList(vectorParameterValueList);
    SectionParameterList sectionParameterList;
    sectionParameterList = new SectionParameterList(sectionParameterValueList);
    for (int i = 0; i < metaData.getPolicy().size(); i++) {
      Map<String, String> mapPolicy = cpmMaps.createMapPolicy(checkPolicyMergeREQ.getPolicy());
      if (metaData.getPolicy().get(i).getType().equalsIgnoreCase(CPMConstants.TEXT)) {
        cpmPolicy.generatePolicy(metaData, checkPolicyMergeREQ, textParameterList, i, mapPolicy);
      }
    }
    if(checkPolicyMergeREQ.getPeopleList()!=null) {
    	cpmPerson.generatePerson(metaData, textParameterList,checkPolicyMergeREQ,vectorParameterValueList,sectionParameterValueList);
    }	 
    cpmCoverage.generateCoverage(metaData, checkPolicyMergeREQ, vectorParameterValueList);
    metadataList = new MetadataList(textParameterList, vectorParameterList, sectionParameterList);
    return metadataList;
  }
}
