package cl.cardif.policy.tools;

import cl.cardif.policy.domain.model.MetaData;
import cl.cardif.policy.domain.request.CheckPolicyMergeREQ;
import cl.cardif.policy.domain.request.People;
import cl.cardif.policy.domain.response.SectionParameter;
import cl.cardif.policy.domain.response.TextParameter;
import cl.cardif.policy.domain.response.VectorParameter;
import cl.cardif.policy.domain.response.VectorParameterValue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

@Service
public class CPMPerson {

    @Autowired
    CPMMaps cpmMaps;

    public void generatePerson(
            MetaData metaData,
            List<TextParameter> textParameterList,
            CheckPolicyMergeREQ checkPolicyMergeREQ,
            List<VectorParameterValue> vectorParameterValueList,
            List<SectionParameter> sectionParameterValueList) {
    	int countAdditionalInsured=0;
    	for(int k=0;k<checkPolicyMergeREQ.getPeopleList().size();k++){
    		 if (checkPolicyMergeREQ.getPeopleList().get(k).getType().equalsIgnoreCase(CPMConstants.MAIN_INSURED)) {
    	            createPeopleText(metaData, checkPolicyMergeREQ.getPeopleList().get(k), textParameterList, CPMConstants.MAIN_INSURED);
    	        } else if (checkPolicyMergeREQ.getPeopleList().get(k).getType().equalsIgnoreCase(CPMConstants.PAYER)) {
    	            createPeopleText(metaData, checkPolicyMergeREQ.getPeopleList().get(k), textParameterList, CPMConstants.PAYER);
    	        } else if (checkPolicyMergeREQ.getPeopleList().get(k).getType().equalsIgnoreCase(CPMConstants.CONTRACTOR)) {
    	            createPeopleText(metaData, checkPolicyMergeREQ.getPeopleList().get(k), textParameterList, CPMConstants.CONTRACTOR);
    	        } else if (checkPolicyMergeREQ.getPeopleList().get(k).getType().equalsIgnoreCase(CPMConstants.ADDITIONAL_INSURED)) {
    	        	countAdditionalInsured = countAdditionalInsured+1;
    	            createPeopleText(metaData, checkPolicyMergeREQ.getPeopleList().get(k), textParameterList, CPMConstants.ADDITIONAL_INSURED);
    	        } else if (checkPolicyMergeREQ.getPeopleList().get(k).getType().equalsIgnoreCase(CPMConstants.MAIN_BENEFICIARIES)) {
    	            createListPeople(metaData, checkPolicyMergeREQ.getPeopleList().get(k), vectorParameterValueList, CPMConstants.MAIN_BENEFICIARIES);
    	        }
    		 	
    	}
    	createListPeopleBenef(metaData, checkPolicyMergeREQ, vectorParameterValueList, sectionParameterValueList);
    	
    	if(countAdditionalInsured<10) {
    		int tagTmp=10-countAdditionalInsured;
    		for(int a=0;a<countAdditionalInsured;a++){
        		generateSectionParameter(sectionParameterValueList,tagTmp);
        		tagTmp = tagTmp+1;
        	}
    	}
    	
    	
    }
    
    public void createListPeopleBenef(
    	      MetaData metaData,
    	      //People people,
    	      CheckPolicyMergeREQ checkPolicyMergeREQ,
    	      List<VectorParameterValue> vectorParameterValueList,
    	      List<SectionParameter> sectionParameterValueList) {
    	    List<People> insuredAdditional = new ArrayList<>();
    	    List<People> benefAdditional = new ArrayList<>();
    	    IntStream.range(0, checkPolicyMergeREQ.getPeopleList().size())
    	        .forEach(
    	            k -> {
    	            	addMainInsuredAndAdditional(k, checkPolicyMergeREQ, benefAdditional, insuredAdditional);
    	            });
    	    if (!benefAdditional.isEmpty()) {
    	    	callCreateList(metaData,vectorParameterValueList, sectionParameterValueList, benefAdditional, insuredAdditional);
	    	     
	    	    }
    	  }
    public void callCreateList(MetaData metaData,List<VectorParameterValue> vectorParameterValueList,
    		List<SectionParameter> sectionParameterValueList,
    		List<People> benefAdditional,List<People> insuredAdditional
    		) {
    	  List<People> benefAdditionalTemp;
  	    People ase;
    	int counterBeneficiariesAdditional=0;
        for (int i = 0; i < insuredAdditional.size(); i++) {
	    	  benefAdditionalTemp = new ArrayList<>();
	        for (int j = 0; j < benefAdditional.size(); j++) {
	        	if(insuredAdditional.get(i).getCode() == benefAdditional.get(j).getInsuredCode()){
	        		benefAdditionalTemp.add(benefAdditional.get(j));
	        		counterBeneficiariesAdditional=counterBeneficiariesAdditional+1;
		    	      }
	        	}
	    	        if(!benefAdditionalTemp.isEmpty()) {
      			    ase = insuredAdditional.get(i);
	    	        	createListPeopleBF(
	    	            metaData,
	    	            benefAdditionalTemp,
	    	            vectorParameterValueList,
	    	            CPMConstants.BENEFICIARIES_ADDITIONAL,
	    	            insuredAdditional,
	    	            ase);   	    	        	
	    	        }    	       
	        }
        if(counterBeneficiariesAdditional<10){
        	int tagTmp = 10-counterBeneficiariesAdditional;
        	for(int z=0;z<counterBeneficiariesAdditional;z++) {
        		generateSectionParameterList(sectionParameterValueList, tagTmp);
	        	tagTmp = tagTmp+1;
        	}	    	        	
        }
    }
    public void addMainInsuredAndAdditional (int k,CheckPolicyMergeREQ checkPolicyMergeREQ,  List<People> benefAdditional,List<People> insuredAdditional) {
    	   if (checkPolicyMergeREQ.getPeopleList()
	                  .get(k)
	                  .getType()
	                  .equalsIgnoreCase(CPMConstants.ADDITIONAL_INSURED)) {
	            	  insuredAdditional.add(checkPolicyMergeREQ.getPeopleList().get(k));
	              } else if (checkPolicyMergeREQ.getPeopleList()
 	                  .get(k)
 	                  .getType()
	                  .equalsIgnoreCase(CPMConstants.BENEFICIARIES_ADDITIONAL)) {
	            	  benefAdditional.add(checkPolicyMergeREQ.getPeopleList().get(k));
	              }
    }
    

    private void createListPeopleBF(
            MetaData metaData,
            List<People> people,
            List<VectorParameterValue> vectorParameterValueList,
            String typePerson,
            List<People> insuredAdditional,
            People ase) {
        for (int i = 0; i < metaData.getPeople().size(); i++) {
            if (metaData.getPeople().get(i).getType().equalsIgnoreCase("LISTA")
                    && metaData.getPeople().get(i).getTypePerson().equalsIgnoreCase(typePerson)) {     
            	addVectorParameterListPeopleBF(i, metaData,people,vectorParameterValueList, typePerson, insuredAdditional, ase);                    
            }
        }
       
    }
    
    private void addVectorParameterListPeopleBF(int i, MetaData metaData, List<People> people,
    		List<VectorParameterValue> vectorParameterValueList,String typePerson
    		,List<People> insuredAdditional,People ase) {
    	VectorParameterValue vectorParameterValue;
    	for (Field f : people.get(0).getClass().getDeclaredFields()) {
            if (metaData.getPeople().get(i).getTemplateTagName() != null
                    && f.getName().equalsIgnoreCase(metaData.getPeople().get(i).getServiceTagName())
                    && metaData.getPeople().get(i).getTypePerson().equalsIgnoreCase(typePerson)) {                      	          
                			vectorParameterValue =
                                    new VectorParameterValue(
                                    		typePerson.equalsIgnoreCase(CPMConstants.BENEFICIARIES_ADDITIONAL)
                                    		    ? createTagAdditionalInsuredBF(
                                    		    		metaData.getPeople().get(i).getTemplateTagName(),
                                    		    		ase)
                                    		    		: metaData.getPeople().get(i).getTemplateTagName(),
                                    		    		vectorParameterPersonBF(insuredAdditional, f.getName(),people));                       			
                            vectorParameterValueList.add(vectorParameterValue);
                		
            }
        }
    }
    
    private void createListPeople(
            MetaData metaData,
            People people,
            List<VectorParameterValue> vectorParameterValueList,
            String typePerson) {
        VectorParameterValue vectorParameterValue;
        for (int i = 0; i < metaData.getPeople().size(); i++) {
            if (metaData.getPeople().get(i).getType().equalsIgnoreCase("LISTA")
                    && metaData.getPeople().get(i).getTypePerson().equalsIgnoreCase(typePerson)) {
                for (Field f : people.getClass().getDeclaredFields()) {
                    if (metaData.getPeople().get(i).getTemplateTagName() != null
                            && f.getName().equalsIgnoreCase(metaData.getPeople().get(i).getServiceTagName())
                            && metaData.getPeople().get(i).getTypePerson().equalsIgnoreCase(typePerson)) {
                        vectorParameterValue =
                                new VectorParameterValue(
                                        metaData.getPeople().get(i).getTemplateTagName(),
                                        vectorParameterPerson(people, f.getName()));
                        vectorParameterValueList.add(vectorParameterValue);
                    }
                }
            }
        }
    }

    private void createPeopleText(
            MetaData metaData, People people, List<TextParameter> textParameterList,String typePerson) {
        Map<String, String> mapPerson = cpmMaps.createMapPerson(people);
        for (int i = 0; i < metaData.getPeople().size(); i++) {
            if (metaData.getPeople().get(i).getType().equalsIgnoreCase("TEXTO")
                    && metaData.getPeople().get(i).getTypePerson().equalsIgnoreCase(typePerson)) {
                generateTextPerson(metaData, people, textParameterList, i, mapPerson,typePerson);
            }
        }
    }
    
    private void generateTextPerson(
            MetaData metaData,
            People people,
            List<TextParameter> textParameterList,
            int i,
            Map<String, String> mapPerson,
            String typePerson) {
        TextParameter textParameter;
        for (Field f : people.getClass().getDeclaredFields()) {
            if (metaData.getPeople().get(i).getTemplateTagName() != null
                    && f.getName().equalsIgnoreCase(metaData.getPeople().get(i).getServiceTagName())) {
                textParameter =
                        new TextParameter(
                        		typePerson.equalsIgnoreCase(CPMConstants.ADDITIONAL_INSURED)
                                        ? createTagAdditionalInsured(
                                        metaData.getPeople().get(i).getTemplateTagName(), people)
                                        : metaData.getPeople().get(i).getTemplateTagName(),
                                mapPerson.get(f.getName()) != null
                                        ? mapPerson.get(f.getName())
                                        : CPMConstants.EMPTY,
                                metaData.getPeople().get(i).getType());
                textParameterList.add(textParameter);
        
            }
        }
        
    }

    private void generateSectionParameterList(List<SectionParameter> sectionParameterValueList,int tagTmp) {
    	String templateTagName="Lista_InfoCoAsegXXX_InfoBeneficiario_Nombre";
    	SectionParameter sectionParameter= new SectionParameter(createTagSectionParameter(templateTagName,tagTmp), "0");
    		sectionParameterValueList.add(sectionParameter);
    		
    }
    
    private void generateSectionParameter(List<SectionParameter> sectionParameterValueList,int tagTmp) {
    	String templateTagName="InfoCoAsegXXX_Nombre";
    	SectionParameter sectionParameter= new SectionParameter(createTagSectionParameter(templateTagName,tagTmp), "0");
    		sectionParameterValueList.add(sectionParameter);
    		
    }
    
    private String createTagSectionParameter(String templateTagName, int tagTmp) {
        return templateTagName.replace(
                CPMConstants.TAG_ADDITIONAL, String.valueOf(tagTmp+1));
    }
    
    private String createTagAdditionalInsured(String templateTagName, People people) {
        return templateTagName.replace(
                CPMConstants.TAG_ADDITIONAL, String.valueOf(people.getTypeSequentialNumber() - 1));
    }
    private String createTagAdditionalInsuredBF(String templateTagName, People people) {
    	return templateTagName.replace(
         	                CPMConstants.TAG_ADDITIONAL, String.valueOf(people.getTypeSequentialNumber() - 1));  
    }
        
    private List<VectorParameter> vectorParameterPerson(People people, String field) {
        List<VectorParameter> vectorParameterList = new ArrayList<>();
        VectorParameter vectorParameter;
        Map<String, String> mapPerson = cpmMaps.createMapPerson(people);
        vectorParameter =
                new VectorParameter(
                        String.valueOf(1),
                        mapPerson.get(field) != null ? mapPerson.get(field) : CPMConstants.EMPTY);
        vectorParameterList.add(vectorParameter);
        return vectorParameterList;
    }
    
    private List<VectorParameter> vectorParameterPersonBF(List<People> insuredAdditional, String field,List<People> people) {
        List<VectorParameter> vectorParameterList = new ArrayList<>();
        VectorParameter vectorParameter;
        List<Map<String, String>> listMapPerson = cpmMaps.createMapPersonBF(people);
        for(int t=0; t< insuredAdditional.size();t++) {
        	for(int z = 0;z< listMapPerson.size();z++ ) {
        		if(insuredAdditional.get(t).getCode()==people.get(z).getInsuredCode()) { 
        			vectorParameter =
                             new VectorParameter(
                                     String.valueOf(z+1),
                                     listMapPerson.get(z).get(field) != null?
                                    		 listMapPerson.get(z).get(field) :
                                    			 CPMConstants.EMPTY);
                     vectorParameterList.add(vectorParameter); 	
        		}
        	}	
        }	   
        return vectorParameterList;
    }
}
 