package cl.cardif.service.impl;

import cl.cardif.dao.GEInsertDao;
import cl.cardif.domain.request.RequestGE;
import cl.cardif.domain.response.GenericErrorRESP;
import cl.cardif.domain.response.Message;
import cl.cardif.domain.response.ResponseGE;
import cl.cardif.exception.GEException;
import cl.cardif.service.GEService;
import cl.cardif.tools.GEMessages;
import cl.cardif.tools.GETools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GEServiceImpl implements GEService {

  private static Logger logger = LoggerFactory.getLogger(GEServiceImpl.class);

  @Autowired GETools geTools;

  @Autowired GEInsertDao geInsertDao;

  @Override
  public ResponseGE insertError(RequestGE requestGE) {
    try {
      if (geTools.validateRequest(requestGE)) {
        if (geInsertDao.insertError(requestGE) == 0) {
          return new ResponseGE(new GenericErrorRESP(new Message(GEMessages.OK, 10)));
        } else {
          logger.error(GEMessages.ERROR_CALL_SP);
          return new ResponseGE(new GenericErrorRESP(new Message(GEMessages.ERROR_CALL_SP, 13)));
        }
      } else {
        logger.error(GEMessages.ERROR_REQUEST);
        return new ResponseGE(new GenericErrorRESP(new Message(GEMessages.ERROR_REQUEST, 12)));
      }
    } catch (GEException e) {
      logger.error(e.getMessage());
      return new ResponseGE(new GenericErrorRESP(new Message(e.getMessage(), 11)));
    }
  }
}
