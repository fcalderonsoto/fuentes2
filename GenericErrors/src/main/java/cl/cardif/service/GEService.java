package cl.cardif.service;

import cl.cardif.domain.request.RequestGE;
import cl.cardif.domain.response.ResponseGE;

@FunctionalInterface
public interface GEService {

  ResponseGE insertError(RequestGE requestGE);
}
