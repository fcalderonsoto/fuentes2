package cl.cardif.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.cardif.domain.request.RequestGE;
import cl.cardif.domain.response.ResponseGE;
import cl.cardif.service.GEService;

@RestController
@RequestMapping("/")
public class GEController {

  @Autowired
  GEService geService;

  @PostMapping(
      value = "insert-error",
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<ResponseGE> insert(@RequestBody RequestGE requestGE){
    return new ResponseEntity<>(geService.insertError(requestGE), HttpStatus.OK);
  }

}
