package cl.cardif.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GenericErrorREQ {

  @JsonProperty("systemId")
  private Integer systemId;

  @JsonProperty("errorCode")
  private Integer errorCode;
  
  @JsonProperty("transactionId")
  private Integer transactionId;

  @JsonProperty("transactionInput")
  private String transactionInput;

  @JsonProperty("errorOrigin")
  private String errorOrigin;

  @JsonProperty("errorDescription")
  private String errorDescription;

  @JsonProperty("serverId")
  private Integer serverId;

  @JsonProperty("host")
  private String host;

  public Integer getSystemId() {
    return systemId;
  }

  public Integer getErrorCode() {
   return errorCode;
 }
  
  public Integer getTransactionId() {
    return transactionId;
  }

  public String getTransactionInput() {
    return transactionInput;
  }

  public String getErrorOrigin() {
    return errorOrigin;
  }

  public String getErrorDescription() {
    return errorDescription;
  }

  public Integer getServerId() {
    return serverId;
  }

  public String getHost() {
    return host;
  }
}
