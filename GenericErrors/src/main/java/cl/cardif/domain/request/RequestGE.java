package cl.cardif.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestGE {
  @JsonProperty("generic_Error_REQ")
  private GenericErrorREQ genericErrorREQ;

  public GenericErrorREQ getGenericErrorREQ() {
    return genericErrorREQ;
  }
}
