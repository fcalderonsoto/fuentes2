package cl.cardif.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GenericErrorRESP {
  @JsonProperty("Message")
  private Message message;

	public GenericErrorRESP(
			final Message message) {
		this.message = message;
	}
}
