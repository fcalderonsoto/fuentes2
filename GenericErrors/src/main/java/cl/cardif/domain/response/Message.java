package cl.cardif.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Message {
  @JsonProperty("message")
  private String message;

  @JsonProperty("code")
  private int code;

  public Message(String message, int code) {
    this.message = message;
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public int getCode() {
    return code;
  }
}
