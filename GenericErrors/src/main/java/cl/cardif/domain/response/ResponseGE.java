package cl.cardif.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseGE {
  
  @JsonProperty("Generic_Error_RSP")
  private GenericErrorRESP genericErrorRESP;

  public ResponseGE(
          final GenericErrorRESP genericErrorRESP) {
    this.genericErrorRESP = genericErrorRESP;
  }

  public GenericErrorRESP getGenericErrorRESP() {
    return genericErrorRESP;
  }
}
