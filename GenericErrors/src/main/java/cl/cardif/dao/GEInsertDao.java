package cl.cardif.dao;

import cl.cardif.domain.request.RequestGE;

@FunctionalInterface
public interface GEInsertDao {

  long insertError(RequestGE requestGE);
}
