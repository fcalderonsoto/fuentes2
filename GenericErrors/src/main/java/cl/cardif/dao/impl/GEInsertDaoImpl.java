package cl.cardif.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import cl.cardif.dao.GEInsertDao;
import cl.cardif.domain.request.RequestGE;
import cl.cardif.exception.GEException;
import cl.cardif.tools.GEConstants;
import cl.cardif.tools.GEMessages;
import cl.cardif.tools.GETools;
import oracle.jdbc.OracleTypes;

@Repository
public class GEInsertDaoImpl implements GEInsertDao {

  @Autowired
  GETools GETools;

  public long insertError(RequestGE requestGE) {
    long status;
    try (Connection conn = ((DataSource)((Context) new InitialContext().lookup(GEConstants.JAVA_COMP))
                        .lookup(GEConstants.JNDI_ERRORS))
                .getConnection();
        CallableStatement call = conn.prepareCall(GEConstants.INSERT_ERROR)) {
      conn.setAutoCommit(false);
      call.setLong(1, requestGE.getGenericErrorREQ().getSystemId());
      call.setLong(2, requestGE.getGenericErrorREQ().getTransactionId());
      call.setString(3, requestGE.getGenericErrorREQ().getTransactionInput());
      call.setString(4, requestGE.getGenericErrorREQ().getErrorOrigin());
      call.setLong(5, requestGE.getGenericErrorREQ().getErrorCode());
      call.setString(6, requestGE.getGenericErrorREQ().getErrorDescription());
      call.setLong(7, requestGE.getGenericErrorREQ().getServerId());
      call.setString(8, requestGE.getGenericErrorREQ().getHost());
      call.registerOutParameter(9, OracleTypes.NUMBER);
      call.registerOutParameter(10, OracleTypes.VARCHAR);
      call.execute();
      if(call.getLong(9) == 0) {
          status = call.getLong(9);
      }else {
    	  throw new GEException(call.getString(10), HttpStatus.INTERNAL_SERVER_ERROR, null);
      }
      conn.commit();
      conn.setAutoCommit(true);
    } catch (SQLException | NamingException e) {
        throw new GEException(
            GEMessages.ERROR_STORE_PROCEDURE + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
    }
    return status;
  }
}
