package cl.cardif.tools;

import org.springframework.stereotype.Service;

import cl.cardif.domain.request.RequestGE;

@Service
public class GETools {

  public boolean validateRequest(RequestGE requestGE) {
    return requestGE != null
        && requestGE.getGenericErrorREQ() != null
        && requestGE.getGenericErrorREQ().getSystemId() != null
        && requestGE.getGenericErrorREQ().getSystemId().longValue() != 0
        && requestGE.getGenericErrorREQ().getTransactionId() != null
        && requestGE.getGenericErrorREQ().getTransactionId().longValue() != 0
        && requestGE.getGenericErrorREQ().getTransactionInput() != null
        && !requestGE.getGenericErrorREQ().getTransactionInput().isEmpty()
        && requestGE.getGenericErrorREQ().getErrorOrigin() != null
        && !requestGE.getGenericErrorREQ().getErrorOrigin().isEmpty()
        && requestGE.getGenericErrorREQ().getErrorDescription() != null
        && !requestGE.getGenericErrorREQ().getErrorDescription().isEmpty()
        && requestGE.getGenericErrorREQ().getServerId() != null
        && requestGE.getGenericErrorREQ().getServerId().longValue() != 0
        && requestGE.getGenericErrorREQ().getHost() != null
        && !requestGE.getGenericErrorREQ().getHost().isEmpty();
  }

}
