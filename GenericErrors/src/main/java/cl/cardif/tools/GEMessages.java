package cl.cardif.tools;

public class GEMessages {

  public static final String OK = "Servicio se ejecuto correctamente";
  public static final String ERROR_CALL_SP = "El Procedimiento Almacenado responde de forma incorrecta";
  public static final String ERROR_REQUEST = "Llamado al servicio de forma incorrecta";
  public static final String ERROR_STORE_PROCEDURE = "Error en el Procedimiento Almacenado : ";
}
