package cl.cardif.tools;

public class GEConstants {

  public static final String JAVA_COMP = "java:comp/env";
  public static final String JNDI_ERRORS = "jdbc/ERRORS";
  public static final String INSERT_ERROR = "{call PCK_GENERIC_ERRORS.PRC_INSERT_ERROR(?,?,?,?,?,?,?,?,?,?)}";

}
