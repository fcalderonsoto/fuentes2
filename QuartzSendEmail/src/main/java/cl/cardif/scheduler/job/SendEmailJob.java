package cl.cardif.scheduler.job;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cl.cardif.configuration.BusinessException;
import cl.cardif.dao.SendDAO;
import cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1.SendEmailStandardRSPType;
import cl.cardif.soap.EmailStandardClient;
import cl.cardif.util.Body;
import cl.cardif.util.CIUSEConstants;
import cl.cardif.util.DateUtil;
import cl.cardif.util.Planilla;

@Component
public class SendEmailJob implements Job {

  @Autowired SendDAO sendDAO;

  @Autowired EmailStandardClient emailStandardClient;

  private Logger logger = LoggerFactory.getLogger(SendEmailJob.class);

  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {
    try {
    	DateUtil dateUtil = new DateUtil();
    	dateUtil.createDates();
    	Boolean validacion = dateUtil.getCronValidationDay();
    	logger.info("Inicio Validaciòn Monday");
    	if(validacion.equals(true)) {
    		  logger.info("Inicio Quartz");
    	      SendEmailStandardRSPType sendEmailStandardRSPType;
    	      dateUtil.createDates();
    	      List<Body> body = sendDAO.getBody(dateUtil.getDateInicio(), dateUtil.getDateTermino());
    	      logger.info("FECHA INICIO: "+dateUtil.getDateInicio());
    	      logger.info("FECHA TERMINO: "+dateUtil.getDateTermino());
    	      Planilla planilla = sendDAO.getPlanilla();
    	        logger.info("Llamada al Servicio SendEmail"); 
    	        sendEmailStandardRSPType = emailStandardClient.sendEmail(body,planilla);  
    	        if (sendEmailStandardRSPType
    	                .getResponseHeader()
    	                .getResult()
    	                .getStatus()
    	                .equalsIgnoreCase(CIUSEConstants.OK)) {
    	          logger.info("Llamado al sendEmail");
    	      }      
    	      logger.info("Fin Quartz");
    	}else {
    	  logger.info("NO ES LUNES");
    	} 
    	
    } catch (BusinessException e) {
      throw new JobExecutionException("CarInsuranceSendEmail Error", e);
    }
  }
}
