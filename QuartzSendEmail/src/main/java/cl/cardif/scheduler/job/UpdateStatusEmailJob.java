package cl.cardif.scheduler.job;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cl.cardif.configuration.BusinessException;
//import cl.cardif.dao.StatusDAO;
import cl.cardif.emailstandard.schema.ebm.emailstandard.status.v1.StatusEmailStandardRSPType;
import cl.cardif.soap.EmailStandardClient;
import cl.cardif.util.CIUSEConstants;

@Component
public class UpdateStatusEmailJob implements Job {

  @Autowired int retriesEmails;

  //@Autowired StatusDAO statusDAO;

  @Autowired EmailStandardClient emailStandardClient;

  private Logger logger = LoggerFactory.getLogger(UpdateStatusEmailJob.class);

  @Override
  public void execute(JobExecutionContext context) throws JobExecutionException {
    /*try {
      logger.info("Inicio Quartz");
      StatusEmailStandardRSPType statusEmailStandardRSPType;
      logger.info("Variable de reintentos : " + retriesEmails);
      logger.info("Llamada getStatus");
      List<String> listEmails = statusDAO.getStatus(retriesEmails);
      logger.info("Lista de emails : " + listEmails.toString());
      for (String listEmail : listEmails) {
        logger.info("Llamada al Servicio Email Standard con id: " + listEmail);
        statusEmailStandardRSPType = emailStandardClient.getStatus(listEmail);
        if (statusEmailStandardRSPType
                .getResponseHeader()
                .getResult()
                .getStatus()
                .equalsIgnoreCase(CIUSEConstants.OK)) {
          logger.info("Llamado al updateStatus con id: " + listEmail);
          statusDAO.updateStatus(
                  Integer.parseInt(
                          statusEmailStandardRSPType
                                  .getBody()
                                  .getStatusEmailStandardResponse()
                                  .getEmailID()),
                  statusEmailStandardRSPType.getBody().getStatusEmailStandardResponse().getStatusID());
          logger.info("Se actualizo el id : " + listEmail);
        }
      }
      logger.info("Fin Quartz");
    } catch (BusinessException e) {
      throw new JobExecutionException("CarInsuranceUpdateStatusEmail Error", e);
    }*/
  }
}
