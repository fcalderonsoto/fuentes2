package cl.cardif.scheduler.config;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.quartz.JobDetail;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;

import cl.cardif.scheduler.job.SendEmailJob;
import cl.cardif.util.DateUtil;


@Configuration
public class ScheduleConfig {
  Logger logger = LoggerFactory.getLogger(getClass());
  @Autowired DataSource quartzDs;
  @Autowired int cronInterval;
  @Autowired private ApplicationContext applicationContext;
  DateUtil dateutil = new DateUtil();

  @PostConstruct
  public void init() {
    logger.info("---Schedule init---");
  }

  @Bean
  public SpringBeanJobFactory springBeanJobFactory() {
    AutoWireSpringJobFactory jobFactory = new AutoWireSpringJobFactory();
    logger.debug("Configuring Job factory");
    logger.info("Configuring Job factory");

    jobFactory.setApplicationContext(applicationContext);
    return jobFactory;
  }

  @Bean
  public SchedulerFactoryBean scheduler(Trigger extSettlementTrigger, JobDetail job) {

    SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();
    schedulerFactory.setDataSource(quartzDs);

    logger.debug("Setting the Scheduler up");
    logger.info("Setting the Scheduler up");
    schedulerFactory.setJobFactory(springBeanJobFactory());
    schedulerFactory.setJobDetails(job);

    schedulerFactory.setConfigLocation(new ClassPathResource("quartz.properties"));

    schedulerFactory.setTriggers(extSettlementTrigger);

    return schedulerFactory;
  }

  @Bean
  public JobDetailFactoryBean extSettlementJob() {
    JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
    logger.info("Setting JOB");
    jobDetailFactory.setJobClass(SendEmailJob.class);
    jobDetailFactory.setName("Qrtz_Send_Email_Job");
    jobDetailFactory.setDescription("Job Reporte semanal generación de pólizas y certificados online");
    jobDetailFactory.setDurability(true);
    return jobDetailFactory;
  }

  
@Bean
  public SimpleTriggerFactoryBean extSettlementTrigger(JobDetail extSettlementJob) {

    SimpleTriggerFactoryBean trigger = new SimpleTriggerFactoryBean();
    trigger.setJobDetail(extSettlementJob);
    
    logger.info("Configuring trigger to fire every {} day", cronInterval);
    
	trigger.setRepeatInterval((long)cronInterval * 60 * 1000 * 10);
	trigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);
	trigger.setName("Qrtz_Send_Email_Trigger");	
    return trigger;
  }

}
