package cl.cardif.configuration;

import org.springframework.http.HttpStatus;

public enum MessageError {
	INTERNAL_ERROR(HttpStatus.OK,"01", "Error interno inesperado"),
	INPUT_JSON_INVALID(HttpStatus.BAD_REQUEST,"02","Formato de entrada de json inválido."),
	OUTPUT_JSON_INVALID(HttpStatus.OK,"03","El objeto de respuesta no pudo ser serializado a un JSON"), 
	SQL_PROBLEM(HttpStatus.OK,"04","Hubo un error al realizar una consulta SQL o llamar un procedure"), 
	CONNECTION_DATABASE(HttpStatus.OK,"05","Hubo un error al intentar cerrar la connección"),
	NO_DATA_FOUND(HttpStatus.OK,"06","Datos de entrada vacios o inválidos"),
	PARSE_BASE64_ERROR(HttpStatus.OK,"07","Problema al convertir un string en base 64"),
	OK(HttpStatus.OK,"10","OK");
	
	private HttpStatus 	status;
	private String 		code;
	private String 		message;
	
	private MessageError(HttpStatus status, String code, String message) {
		this.status=status;
		this.code=code;
		this.message=message;
	}

	public HttpStatus getStatus() {
		return status;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
		
}
