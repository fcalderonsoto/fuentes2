package cl.cardif.configuration;

public class BusinessException extends Exception {

	private static final long serialVersionUID = -296040625548500419L;
	private final String code;

	public BusinessException(Exception e, MessageError messageError) {
		super(messageError.getMessage() + "\n" + e.getMessage(), e);
		this.code=messageError.getCode();
	}

	public BusinessException(MessageError me) {
		super(me.getMessage());
		this.code=me.getCode();
	}

	public String getCode() {
		return code;
	}

}
