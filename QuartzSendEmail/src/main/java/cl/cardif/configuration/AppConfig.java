package cl.cardif.configuration;

import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.net.ssl.SSLContext;
import javax.sql.DataSource;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import cl.cardif.emailstandard.v1.EmailStandardPortType;
import cl.cardif.emailstandard.v1.EmailStandardService;

@Configuration
@EnableScheduling
public class AppConfig {
  static Logger logger = LoggerFactory.getLogger(AppConfig.class);

  @Bean(name = "polizaDs")
  DataSource getPolizaDs() throws NamingException {
    InitialContext context = new InitialContext();
    return (DataSource) context.lookup("java:comp/env/jdbc/POLIZA");
  }
  
  @Bean(name = "quartzDs")
  DataSource getQuartzDs() throws NamingException {
    InitialContext context = new InitialContext();
    return (DataSource) context.lookup("java:comp/env/jdbc/GENERIC_QUARTZ");
  }

  @Bean(name = "cronInterval")
  public int cronInterval() throws NamingException {
    InitialContext context = new InitialContext();
    return (int) context.lookup("java:comp/env/CronInterval");
  }

  @Bean(name = "retriesEmails")
  public int retriesEmails() throws NamingException {
    InitialContext context = new InitialContext();
    return (int) context.lookup("java:comp/env/RetriesEmails");
  }

  @Bean(name = "emailStatusURL")
  public String emailStatusURL() throws NamingException {
    InitialContext context = new InitialContext();
    return (String) context.lookup("java:comp/env/EmailStatusURL");
  }

  @Bean
  public CloseableHttpClient getHttpClient() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
    SSLContext sslContext = SSLContextBuilder.create().loadTrustMaterial(new TrustSelfSignedStrategy()).build();
    SSLConnectionSocketFactory connectionFactory = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);
    HttpClientBuilder builder = HttpClientBuilder.create();
    return builder.useSystemProperties().setSSLSocketFactory(connectionFactory).build();
  }

  @Bean
  public URL emailStatusWsdl() {
    return getClass().getResource("/wsdl/EmailStandard/EmailStandard.wsdl");
  }

  @Bean
  public EmailStandardPortType emailStandardPort(String emailStatusURL, URL emailStatusWsdl) throws NamingException {
    logger.info("emailStandardPort");
    logger.info(emailStatusWsdl != null ? emailStatusWsdl.toString() : "resorce not found");
    final QName SERVICE_NAME = new QName("http://cardif.cl/Service/EBSC/EmailStandard/v1", "EmailStandardService");

    EmailStandardService ss = new EmailStandardService(emailStatusWsdl, SERVICE_NAME);
    EmailStandardPortType port = ss.getEmailStandardPort();

    BindingProvider bindingProvider = (BindingProvider) port;
    bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, emailStatusURL);
    return port;
  }
}
