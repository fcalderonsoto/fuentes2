
package cl.cardif.emailstandard.v1;

import javax.xml.ws.WebFault;


/**
 * This class was generated by Apache CXF 3.2.1
 * 2019-10-09T11:45:16.582-03:00
 * Generated source version: 3.2.1
 */

@WebFault(name = "status_EmailStandard_FRSP", targetNamespace = "http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0")
public class StatusFaultMessage extends Exception {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -5176161014417219138L;
	
	private transient cl.cardif.emailstandard.schema.ebm.emailstandard.status.v1.StatusEmailStandardFRSPType statusEmailStandardFRSP;

	public StatusFaultMessage() {
		super();
	}

	public StatusFaultMessage(String message) {
		super(message);
	}

	public StatusFaultMessage(String message, Throwable cause) {
		super(message, cause);
	}

    public StatusFaultMessage(String message, cl.cardif.emailstandard.schema.ebm.emailstandard.status.v1.StatusEmailStandardFRSPType statusEmailStandardFRSP) {
        super(message);
        this.statusEmailStandardFRSP = statusEmailStandardFRSP;
    }

    public StatusFaultMessage(String message, cl.cardif.emailstandard.schema.ebm.emailstandard.status.v1.StatusEmailStandardFRSPType statusEmailStandardFRSP, Throwable cause) {
        super(message, cause);
        this.statusEmailStandardFRSP = statusEmailStandardFRSP;
    }

    public cl.cardif.emailstandard.schema.ebm.emailstandard.status.v1.StatusEmailStandardFRSPType getFaultInfo() {
        return this.statusEmailStandardFRSP;
    }
}
