
package cl.cardif.emailstandard.schema.eso.messageheader.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 * 				It represents important metadata related to the
 * 				execution of a certain service operation, during its
 * 				request.
 * 			
 * 
 * <p>Clase Java para RequestHeader_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequestHeader_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://cardif.cl/Schema/ESO/MessageHeader/v1.0}MessageHeader_Type"&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestHeader_Type")
public class RequestHeaderType
    extends MessageHeaderType
{


}
