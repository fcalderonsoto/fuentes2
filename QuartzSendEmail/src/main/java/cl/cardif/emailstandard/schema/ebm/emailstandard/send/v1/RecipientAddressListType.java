
package cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RecipientAddressList_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RecipientAddressList_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="RecipientAddressElement" type="{http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0}RecipientAddressElement_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecipientAddressList_Type", propOrder = {
    "recipientAddressElement"
})
public class RecipientAddressListType {

    @XmlElement(name = "RecipientAddressElement")
    protected List<RecipientAddressElementType> recipientAddressElement;

    /**
     * Gets the value of the recipientAddressElement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the recipientAddressElement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRecipientAddressElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RecipientAddressElementType }
     * 
     * 
     */
    public List<RecipientAddressElementType> getRecipientAddressElement() {
        if (recipientAddressElement == null) {
            recipientAddressElement = new ArrayList<RecipientAddressElementType>();
        }
        return this.recipientAddressElement;
    }

}
