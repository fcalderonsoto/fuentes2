
package cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SendEmailStandardRequest_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SendEmailStandardRequest_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0}RecipientAddressList"/&gt;
 *         &lt;element ref="{http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0}CarbonCopyList" minOccurs="0"/&gt;
 *         &lt;element ref="{http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0}BlindCarbonCopyList" minOccurs="0"/&gt;
 *         &lt;element name="subject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="body" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0}AttachmentsList" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendEmailStandardRequest_Type", propOrder = {
    "recipientAddressList",
    "carbonCopyList",
    "blindCarbonCopyList",
    "subject",
    "body",
    "attachmentsList"
})
public class SendEmailStandardRequestType {

    @XmlElement(name = "RecipientAddressList", required = true)
    protected RecipientAddressListType recipientAddressList;
    @XmlElement(name = "CarbonCopyList")
    protected CarbonCopyListType carbonCopyList;
    @XmlElement(name = "BlindCarbonCopyList")
    protected BlindCarbonCopyListType blindCarbonCopyList;
    protected String subject;
    protected String body;
    @XmlElement(name = "AttachmentsList")
    protected AttachmentsListType attachmentsList;

    /**
     * Obtiene el valor de la propiedad recipientAddressList.
     * 
     * @return
     *     possible object is
     *     {@link RecipientAddressListType }
     *     
     */
    public RecipientAddressListType getRecipientAddressList() {
        return recipientAddressList;
    }

    /**
     * Define el valor de la propiedad recipientAddressList.
     * 
     * @param value
     *     allowed object is
     *     {@link RecipientAddressListType }
     *     
     */
    public void setRecipientAddressList(RecipientAddressListType value) {
        this.recipientAddressList = value;
    }

    /**
     * Obtiene el valor de la propiedad carbonCopyList.
     * 
     * @return
     *     possible object is
     *     {@link CarbonCopyListType }
     *     
     */
    public CarbonCopyListType getCarbonCopyList() {
        return carbonCopyList;
    }

    /**
     * Define el valor de la propiedad carbonCopyList.
     * 
     * @param value
     *     allowed object is
     *     {@link CarbonCopyListType }
     *     
     */
    public void setCarbonCopyList(CarbonCopyListType value) {
        this.carbonCopyList = value;
    }

    /**
     * Obtiene el valor de la propiedad blindCarbonCopyList.
     * 
     * @return
     *     possible object is
     *     {@link BlindCarbonCopyListType }
     *     
     */
    public BlindCarbonCopyListType getBlindCarbonCopyList() {
        return blindCarbonCopyList;
    }

    /**
     * Define el valor de la propiedad blindCarbonCopyList.
     * 
     * @param value
     *     allowed object is
     *     {@link BlindCarbonCopyListType }
     *     
     */
    public void setBlindCarbonCopyList(BlindCarbonCopyListType value) {
        this.blindCarbonCopyList = value;
    }

    /**
     * Obtiene el valor de la propiedad subject.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Define el valor de la propiedad subject.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubject(String value) {
        this.subject = value;
    }

    /**
     * Obtiene el valor de la propiedad body.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBody() {
        return body;
    }

    /**
     * Define el valor de la propiedad body.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBody(String value) {
        this.body = value;
    }

    /**
     * Obtiene el valor de la propiedad attachmentsList.
     * 
     * @return
     *     possible object is
     *     {@link AttachmentsListType }
     *     
     */
    public AttachmentsListType getAttachmentsList() {
        return attachmentsList;
    }

    /**
     * Define el valor de la propiedad attachmentsList.
     * 
     * @param value
     *     allowed object is
     *     {@link AttachmentsListType }
     *     
     */
    public void setAttachmentsList(AttachmentsListType value) {
        this.attachmentsList = value;
    }

}
