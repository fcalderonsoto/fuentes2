
package cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import cl.cardif.emailstandard.schema.eso.messageheader.v1.ResponseHeaderType;


/**
 * <p>Clase Java para Send_EmailStandard_RSP_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Send_EmailStandard_RSP_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://cardif.cl/Schema/ESO/MessageHeader/v1.0}ResponseHeader"/&gt;
 *         &lt;element name="Body"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0}SendEmailStandardResponse"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Send_EmailStandard_RSP_Type", propOrder = {
    "responseHeader",
    "body"
})
public class SendEmailStandardRSPType {

    @XmlElement(name = "ResponseHeader", namespace = "http://cardif.cl/Schema/ESO/MessageHeader/v1.0", required = true)
    protected ResponseHeaderType responseHeader;
    @XmlElement(name = "Body", required = true)
    protected SendEmailStandardRSPType.Body body;

    /**
     * Obtiene el valor de la propiedad responseHeader.
     *
     * @return
     *     possible object is
     *     {@link ResponseHeaderType }
     *
     */
    public ResponseHeaderType getResponseHeader() {
        return responseHeader;
    }

    /**
     * Define el valor de la propiedad responseHeader.
     *
     * @param value
     *     allowed object is
     *     {@link ResponseHeaderType }
     *
     */
    public void setResponseHeader(ResponseHeaderType value) {
        this.responseHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad body.
     *
     * @return
     *     possible object is
     *     {@link SendEmailStandardRSPType.Body }
     *
     */
    public SendEmailStandardRSPType.Body getBody() {
        return body;
    }

    /**
     * Define el valor de la propiedad body.
     *
     * @param value
     *     allowed object is
     *     {@link SendEmailStandardRSPType.Body }
     *
     */
    public void setBody(SendEmailStandardRSPType.Body value) {
        this.body = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0}SendEmailStandardResponse"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sendEmailStandardResponse"
    })
    public static class Body {

        @XmlElement(name = "SendEmailStandardResponse", required = true)
        protected SendEmailStandardResponseType sendEmailStandardResponse;

        /**
         * Obtiene el valor de la propiedad sendEmailStandardResponse.
         * 
         * @return
         *     possible object is
         *     {@link SendEmailStandardResponseType }
         *     
         */
        public SendEmailStandardResponseType getSendEmailStandardResponse() {
            return sendEmailStandardResponse;
        }

        /**
         * Define el valor de la propiedad sendEmailStandardResponse.
         * 
         * @param value
         *     allowed object is
         *     {@link SendEmailStandardResponseType }
         *     
         */
        public void setSendEmailStandardResponse(SendEmailStandardResponseType value) {
            this.sendEmailStandardResponse = value;
        }

    }

}
