
package cl.cardif.emailstandard.schema.ebm.emailstandard.status.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import cl.cardif.emailstandard.schema.ebo.error.v1.ErrorType;
import cl.cardif.emailstandard.schema.eso.messageheader.v1.ResponseHeaderType;


/**
 * <p>Clase Java para Status_EmailStandard_FRSP_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Status_EmailStandard_FRSP_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://cardif.cl/Schema/ESO/MessageHeader/v1.0}ResponseHeader"/&gt;
 *         &lt;element name="Body"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://cardif.cl/Schema/EBO/Error/v1.0}Error"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Status_EmailStandard_FRSP_Type", propOrder = {
    "responseHeader",
    "body"
})
public class StatusEmailStandardFRSPType {

    @XmlElement(name = "ResponseHeader", namespace = "http://cardif.cl/Schema/ESO/MessageHeader/v1.0", required = true)
    protected ResponseHeaderType responseHeader;
    @XmlElement(name = "Body", required = true)
    protected StatusEmailStandardFRSPType.Body body;

    /**
     * Obtiene el valor de la propiedad responseHeader.
     *
     * @return
     *     possible object is
     *     {@link ResponseHeaderType }
     *
     */
    public ResponseHeaderType getResponseHeader() {
        return responseHeader;
    }

    /**
     * Define el valor de la propiedad responseHeader.
     *
     * @param value
     *     allowed object is
     *     {@link ResponseHeaderType }
     *
     */
    public void setResponseHeader(ResponseHeaderType value) {
        this.responseHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad body.
     *
     * @return
     *     possible object is
     *     {@link StatusEmailStandardFRSPType.Body }
     *
     */
    public StatusEmailStandardFRSPType.Body getBody() {
        return body;
    }

    /**
     * Define el valor de la propiedad body.
     *
     * @param value
     *     allowed object is
     *     {@link StatusEmailStandardFRSPType.Body }
     *
     */
    public void setBody(StatusEmailStandardFRSPType.Body value) {
        this.body = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://cardif.cl/Schema/EBO/Error/v1.0}Error"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "error"
    })
    public static class Body {

        @XmlElement(name = "Error", namespace = "http://cardif.cl/Schema/EBO/Error/v1.0", required = true)
        protected ErrorType error;

        /**
         * Obtiene el valor de la propiedad error.
         * 
         * @return
         *     possible object is
         *     {@link ErrorType }
         *     
         */
        public ErrorType getError() {
            return error;
        }

        /**
         * Define el valor de la propiedad error.
         * 
         * @param value
         *     allowed object is
         *     {@link ErrorType }
         *     
         */
        public void setError(ErrorType value) {
            this.error = value;
        }

    }

}
