
package cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EmailStandardInformation_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EmailStandardInformation_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="emailID" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="responseID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="emailError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmailStandardInformation_Type", propOrder = {
    "emailID",
    "responseID",
    "emailError"
})
public class EmailStandardInformationType {

    protected Long emailID;
    protected Integer responseID;
    protected String emailError;

    /**
     * Obtiene el valor de la propiedad emailID.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getEmailID() {
        return emailID;
    }

    /**
     * Define el valor de la propiedad emailID.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setEmailID(Long value) {
        this.emailID = value;
    }

    /**
     * Obtiene el valor de la propiedad responseID.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getResponseID() {
        return responseID;
    }

    /**
     * Define el valor de la propiedad responseID.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setResponseID(Integer value) {
        this.responseID = value;
    }

    /**
     * Obtiene el valor de la propiedad emailError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailError() {
        return emailError;
    }

    /**
     * Define el valor de la propiedad emailError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailError(String value) {
        this.emailError = value;
    }

}
