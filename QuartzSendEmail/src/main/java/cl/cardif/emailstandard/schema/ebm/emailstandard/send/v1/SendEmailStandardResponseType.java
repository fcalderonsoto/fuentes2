
package cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SendEmailStandardResponse_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SendEmailStandardResponse_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0}EmailStandardInformation"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendEmailStandardResponse_Type", propOrder = {
    "emailStandardInformation"
})
public class SendEmailStandardResponseType {

    @XmlElement(name = "EmailStandardInformation", required = true)
    protected EmailStandardInformationType emailStandardInformation;

    /**
     * Obtiene el valor de la propiedad emailStandardInformation.
     * 
     * @return
     *     possible object is
     *     {@link EmailStandardInformationType }
     *     
     */
    public EmailStandardInformationType getEmailStandardInformation() {
        return emailStandardInformation;
    }

    /**
     * Define el valor de la propiedad emailStandardInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailStandardInformationType }
     *     
     */
    public void setEmailStandardInformation(EmailStandardInformationType value) {
        this.emailStandardInformation = value;
    }

}
