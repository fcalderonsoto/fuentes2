
package cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CarbonCopyElement_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CarbonCopyElement_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="carbonCopyAddress" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CarbonCopyElement_Type", propOrder = {
    "carbonCopyAddress"
})
public class CarbonCopyElementType {

    @XmlElement(required = true)
    protected String carbonCopyAddress;

    /**
     * Obtiene el valor de la propiedad carbonCopyAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCarbonCopyAddress() {
        return carbonCopyAddress;
    }

    /**
     * Define el valor de la propiedad carbonCopyAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCarbonCopyAddress(String value) {
        this.carbonCopyAddress = value;
    }

}
