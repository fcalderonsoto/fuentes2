
package cl.cardif.emailstandard.schema.ebm.emailstandard.status.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.cardif.emailstandard.schema.ebm.emailstandard.status.v1 package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _StatusEmailStandardREQ_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0", "status_EmailStandard_REQ");
    private final static QName _StatusEmailStandardRequest_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0", "StatusEmailStandardRequest");
    private final static QName _StatusEmailStandardRSP_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0", "status_EmailStandard_RSP");
    private final static QName _StatusEmailStandardResponse_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0", "StatusEmailStandardResponse");
    private final static QName _StatusEmailStandardFRSP_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0", "status_EmailStandard_FRSP");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.cardif.emailstandard.schema.ebm.emailstandard.status.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link StatusEmailStandardFRSPType }
     * 
     */
    public StatusEmailStandardFRSPType createStatusEmailStandardFRSPType() {
        return new StatusEmailStandardFRSPType();
    }

    /**
     * Create an instance of {@link StatusEmailStandardRSPType }
     * 
     */
    public StatusEmailStandardRSPType createStatusEmailStandardRSPType() {
        return new StatusEmailStandardRSPType();
    }

    /**
     * Create an instance of {@link StatusEmailStandardREQType }
     * 
     */
    public StatusEmailStandardREQType createStatusEmailStandardREQType() {
        return new StatusEmailStandardREQType();
    }

    /**
     * Create an instance of {@link StatusEmailStandardRequestType }
     * 
     */
    public StatusEmailStandardRequestType createStatusEmailStandardRequestType() {
        return new StatusEmailStandardRequestType();
    }

    /**
     * Create an instance of {@link StatusEmailStandardResponseType }
     * 
     */
    public StatusEmailStandardResponseType createStatusEmailStandardResponseType() {
        return new StatusEmailStandardResponseType();
    }

    /**
     * Create an instance of {@link StatusEmailStandardFRSPType.Body }
     * 
     */
    public StatusEmailStandardFRSPType.Body createStatusEmailStandardFRSPTypeBody() {
        return new StatusEmailStandardFRSPType.Body();
    }

    /**
     * Create an instance of {@link StatusEmailStandardRSPType.Body }
     * 
     */
    public StatusEmailStandardRSPType.Body createStatusEmailStandardRSPTypeBody() {
        return new StatusEmailStandardRSPType.Body();
    }

    /**
     * Create an instance of {@link StatusEmailStandardREQType.Body }
     * 
     */
    public StatusEmailStandardREQType.Body createStatusEmailStandardREQTypeBody() {
        return new StatusEmailStandardREQType.Body();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusEmailStandardREQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0", name = "status_EmailStandard_REQ")
    public JAXBElement<StatusEmailStandardREQType> createStatusEmailStandardREQ(StatusEmailStandardREQType value) {
        return new JAXBElement<StatusEmailStandardREQType>(_StatusEmailStandardREQ_QNAME, StatusEmailStandardREQType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusEmailStandardRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0", name = "StatusEmailStandardRequest")
    public JAXBElement<StatusEmailStandardRequestType> createStatusEmailStandardRequest(StatusEmailStandardRequestType value) {
        return new JAXBElement<StatusEmailStandardRequestType>(_StatusEmailStandardRequest_QNAME, StatusEmailStandardRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusEmailStandardRSPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0", name = "status_EmailStandard_RSP")
    public JAXBElement<StatusEmailStandardRSPType> createStatusEmailStandardRSP(StatusEmailStandardRSPType value) {
        return new JAXBElement<StatusEmailStandardRSPType>(_StatusEmailStandardRSP_QNAME, StatusEmailStandardRSPType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusEmailStandardResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0", name = "StatusEmailStandardResponse")
    public JAXBElement<StatusEmailStandardResponseType> createStatusEmailStandardResponse(StatusEmailStandardResponseType value) {
        return new JAXBElement<StatusEmailStandardResponseType>(_StatusEmailStandardResponse_QNAME, StatusEmailStandardResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StatusEmailStandardFRSPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0", name = "status_EmailStandard_FRSP")
    public JAXBElement<StatusEmailStandardFRSPType> createStatusEmailStandardFRSP(StatusEmailStandardFRSPType value) {
        return new JAXBElement<StatusEmailStandardFRSPType>(_StatusEmailStandardFRSP_QNAME, StatusEmailStandardFRSPType.class, null, value);
    }

}
