
package cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para BlindCarbonCopyElement_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BlindCarbonCopyElement_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="blindCarbonCopyAddress" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BlindCarbonCopyElement_Type", propOrder = {
    "blindCarbonCopyAddress"
})
public class BlindCarbonCopyElementType {

    @XmlElement(required = true)
    protected String blindCarbonCopyAddress;

    /**
     * Obtiene el valor de la propiedad blindCarbonCopyAddress.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBlindCarbonCopyAddress() {
        return blindCarbonCopyAddress;
    }

    /**
     * Define el valor de la propiedad blindCarbonCopyAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBlindCarbonCopyAddress(String value) {
        this.blindCarbonCopyAddress = value;
    }

}
