
package cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CarbonCopyList_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CarbonCopyList_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CarbonCopyElement" type="{http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0}CarbonCopyElement_Type" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CarbonCopyList_Type", propOrder = {
    "carbonCopyElement"
})
public class CarbonCopyListType {

    @XmlElement(name = "CarbonCopyElement")
    protected List<CarbonCopyElementType> carbonCopyElement;

    /**
     * Gets the value of the carbonCopyElement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the carbonCopyElement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCarbonCopyElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CarbonCopyElementType }
     * 
     * 
     */
    public List<CarbonCopyElementType> getCarbonCopyElement() {
        if (carbonCopyElement == null) {
            carbonCopyElement = new ArrayList<CarbonCopyElementType>();
        }
        return this.carbonCopyElement;
    }

}
