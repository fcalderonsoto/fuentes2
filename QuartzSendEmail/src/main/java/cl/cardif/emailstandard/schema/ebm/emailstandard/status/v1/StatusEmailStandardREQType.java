
package cl.cardif.emailstandard.schema.ebm.emailstandard.status.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import cl.cardif.emailstandard.schema.eso.messageheader.v1.RequestHeaderType;


/**
 * <p>Clase Java para Status_EmailStandard_REQ_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Status_EmailStandard_REQ_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://cardif.cl/Schema/ESO/MessageHeader/v1.0}RequestHeader"/&gt;
 *         &lt;element name="Body"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0}StatusEmailStandardRequest"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Status_EmailStandard_REQ_Type", propOrder = {
    "requestHeader",
    "body"
})
public class StatusEmailStandardREQType {

    @XmlElement(name = "RequestHeader", namespace = "http://cardif.cl/Schema/ESO/MessageHeader/v1.0", required = true)
    protected RequestHeaderType requestHeader;
    @XmlElement(name = "Body", required = true)
    protected StatusEmailStandardREQType.Body body;

    /**
     * Obtiene el valor de la propiedad requestHeader.
     *
     * @return
     *     possible object is
     *     {@link RequestHeaderType }
     *
     */
    public RequestHeaderType getRequestHeader() {
        return requestHeader;
    }

    /**
     * Define el valor de la propiedad requestHeader.
     *
     * @param value
     *     allowed object is
     *     {@link RequestHeaderType }
     *
     */
    public void setRequestHeader(RequestHeaderType value) {
        this.requestHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad body.
     *
     * @return
     *     possible object is
     *     {@link StatusEmailStandardREQType.Body }
     *
     */
    public StatusEmailStandardREQType.Body getBody() {
        return body;
    }

    /**
     * Define el valor de la propiedad body.
     *
     * @param value
     *     allowed object is
     *     {@link StatusEmailStandardREQType.Body }
     *
     */
    public void setBody(StatusEmailStandardREQType.Body value) {
        this.body = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://cardif.cl/Schema/EBM/EmailStandard/status/v1.0}StatusEmailStandardRequest"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "statusEmailStandardRequest"
    })
    public static class Body {

        @XmlElement(name = "StatusEmailStandardRequest", required = true)
        protected StatusEmailStandardRequestType statusEmailStandardRequest;

        /**
         * Obtiene el valor de la propiedad statusEmailStandardRequest.
         * 
         * @return
         *     possible object is
         *     {@link StatusEmailStandardRequestType }
         *     
         */
        public StatusEmailStandardRequestType getStatusEmailStandardRequest() {
            return statusEmailStandardRequest;
        }

        /**
         * Define el valor de la propiedad statusEmailStandardRequest.
         * 
         * @param value
         *     allowed object is
         *     {@link StatusEmailStandardRequestType }
         *     
         */
        public void setStatusEmailStandardRequest(StatusEmailStandardRequestType value) {
            this.statusEmailStandardRequest = value;
        }

    }

}
