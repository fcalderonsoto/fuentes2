
package cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1 package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SendEmailStandardREQ_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", "send_EmailStandard_REQ");
    private final static QName _SendEmailStandardRequest_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", "SendEmailStandardRequest");
    private final static QName _RecipientAddressList_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", "RecipientAddressList");
    private final static QName _AttachmentsList_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", "AttachmentsList");
    private final static QName _BlindCarbonCopyList_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", "BlindCarbonCopyList");
    private final static QName _CarbonCopyList_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", "CarbonCopyList");
    private final static QName _SendEmailStandardRSP_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", "send_EmailStandard_RSP");
    private final static QName _SendEmailStandardResponse_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", "SendEmailStandardResponse");
    private final static QName _EmailStandardInformation_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", "EmailStandardInformation");
    private final static QName _SendEmailStandardFRSP_QNAME = new QName("http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", "send_EmailStandard_FRSP");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendEmailStandardFRSPType }
     * 
     */
    public SendEmailStandardFRSPType createSendEmailStandardFRSPType() {
        return new SendEmailStandardFRSPType();
    }

    /**
     * Create an instance of {@link SendEmailStandardRSPType }
     * 
     */
    public SendEmailStandardRSPType createSendEmailStandardRSPType() {
        return new SendEmailStandardRSPType();
    }

    /**
     * Create an instance of {@link SendEmailStandardREQType }
     * 
     */
    public SendEmailStandardREQType createSendEmailStandardREQType() {
        return new SendEmailStandardREQType();
    }

    /**
     * Create an instance of {@link SendEmailStandardRequestType }
     * 
     */
    public SendEmailStandardRequestType createSendEmailStandardRequestType() {
        return new SendEmailStandardRequestType();
    }

    /**
     * Create an instance of {@link RecipientAddressListType }
     * 
     */
    public RecipientAddressListType createRecipientAddressListType() {
        return new RecipientAddressListType();
    }

    /**
     * Create an instance of {@link AttachmentsListType }
     * 
     */
    public AttachmentsListType createAttachmentsListType() {
        return new AttachmentsListType();
    }

    /**
     * Create an instance of {@link BlindCarbonCopyListType }
     * 
     */
    public BlindCarbonCopyListType createBlindCarbonCopyListType() {
        return new BlindCarbonCopyListType();
    }

    /**
     * Create an instance of {@link CarbonCopyListType }
     * 
     */
    public CarbonCopyListType createCarbonCopyListType() {
        return new CarbonCopyListType();
    }

    /**
     * Create an instance of {@link SendEmailStandardResponseType }
     * 
     */
    public SendEmailStandardResponseType createSendEmailStandardResponseType() {
        return new SendEmailStandardResponseType();
    }

    /**
     * Create an instance of {@link EmailStandardInformationType }
     * 
     */
    public EmailStandardInformationType createEmailStandardInformationType() {
        return new EmailStandardInformationType();
    }

    /**
     * Create an instance of {@link RecipientAddressElementType }
     * 
     */
    public RecipientAddressElementType createRecipientAddressElementType() {
        return new RecipientAddressElementType();
    }

    /**
     * Create an instance of {@link AttachmentsType }
     * 
     */
    public AttachmentsType createAttachmentsType() {
        return new AttachmentsType();
    }

    /**
     * Create an instance of {@link BlindCarbonCopyElementType }
     * 
     */
    public BlindCarbonCopyElementType createBlindCarbonCopyElementType() {
        return new BlindCarbonCopyElementType();
    }

    /**
     * Create an instance of {@link CarbonCopyElementType }
     * 
     */
    public CarbonCopyElementType createCarbonCopyElementType() {
        return new CarbonCopyElementType();
    }

    /**
     * Create an instance of {@link SendEmailStandardFRSPType.Body }
     * 
     */
    public SendEmailStandardFRSPType.Body createSendEmailStandardFRSPTypeBody() {
        return new SendEmailStandardFRSPType.Body();
    }

    /**
     * Create an instance of {@link SendEmailStandardRSPType.Body }
     * 
     */
    public SendEmailStandardRSPType.Body createSendEmailStandardRSPTypeBody() {
        return new SendEmailStandardRSPType.Body();
    }

    /**
     * Create an instance of {@link SendEmailStandardREQType.Body }
     * 
     */
    public SendEmailStandardREQType.Body createSendEmailStandardREQTypeBody() {
        return new SendEmailStandardREQType.Body();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmailStandardREQType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", name = "send_EmailStandard_REQ")
    public JAXBElement<SendEmailStandardREQType> createSendEmailStandardREQ(SendEmailStandardREQType value) {
        return new JAXBElement<SendEmailStandardREQType>(_SendEmailStandardREQ_QNAME, SendEmailStandardREQType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmailStandardRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", name = "SendEmailStandardRequest")
    public JAXBElement<SendEmailStandardRequestType> createSendEmailStandardRequest(SendEmailStandardRequestType value) {
        return new JAXBElement<SendEmailStandardRequestType>(_SendEmailStandardRequest_QNAME, SendEmailStandardRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecipientAddressListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", name = "RecipientAddressList")
    public JAXBElement<RecipientAddressListType> createRecipientAddressList(RecipientAddressListType value) {
        return new JAXBElement<RecipientAddressListType>(_RecipientAddressList_QNAME, RecipientAddressListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AttachmentsListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", name = "AttachmentsList")
    public JAXBElement<AttachmentsListType> createAttachmentsList(AttachmentsListType value) {
        return new JAXBElement<AttachmentsListType>(_AttachmentsList_QNAME, AttachmentsListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BlindCarbonCopyListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", name = "BlindCarbonCopyList")
    public JAXBElement<BlindCarbonCopyListType> createBlindCarbonCopyList(BlindCarbonCopyListType value) {
        return new JAXBElement<BlindCarbonCopyListType>(_BlindCarbonCopyList_QNAME, BlindCarbonCopyListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CarbonCopyListType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", name = "CarbonCopyList")
    public JAXBElement<CarbonCopyListType> createCarbonCopyList(CarbonCopyListType value) {
        return new JAXBElement<CarbonCopyListType>(_CarbonCopyList_QNAME, CarbonCopyListType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmailStandardRSPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", name = "send_EmailStandard_RSP")
    public JAXBElement<SendEmailStandardRSPType> createSendEmailStandardRSP(SendEmailStandardRSPType value) {
        return new JAXBElement<SendEmailStandardRSPType>(_SendEmailStandardRSP_QNAME, SendEmailStandardRSPType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmailStandardResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", name = "SendEmailStandardResponse")
    public JAXBElement<SendEmailStandardResponseType> createSendEmailStandardResponse(SendEmailStandardResponseType value) {
        return new JAXBElement<SendEmailStandardResponseType>(_SendEmailStandardResponse_QNAME, SendEmailStandardResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EmailStandardInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", name = "EmailStandardInformation")
    public JAXBElement<EmailStandardInformationType> createEmailStandardInformation(EmailStandardInformationType value) {
        return new JAXBElement<EmailStandardInformationType>(_EmailStandardInformation_QNAME, EmailStandardInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmailStandardFRSPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0", name = "send_EmailStandard_FRSP")
    public JAXBElement<SendEmailStandardFRSPType> createSendEmailStandardFRSP(SendEmailStandardFRSPType value) {
        return new JAXBElement<SendEmailStandardFRSPType>(_SendEmailStandardFRSP_QNAME, SendEmailStandardFRSPType.class, null, value);
    }

}
