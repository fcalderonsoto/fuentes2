
package cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import cl.cardif.emailstandard.schema.eso.messageheader.v1.RequestHeaderType;


/**
 * <p>Clase Java para Send_EmailStandard_REQ_Type complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Send_EmailStandard_REQ_Type"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://cardif.cl/Schema/ESO/MessageHeader/v1.0}RequestHeader"/&gt;
 *         &lt;element name="Body"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element ref="{http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0}SendEmailStandardRequest"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Send_EmailStandard_REQ_Type", propOrder = {
    "requestHeader",
    "body"
})
public class SendEmailStandardREQType {

    @XmlElement(name = "RequestHeader", namespace = "http://cardif.cl/Schema/ESO/MessageHeader/v1.0", required = true)
    protected RequestHeaderType requestHeader;
    @XmlElement(name = "Body", required = true)
    protected SendEmailStandardREQType.Body body;

    /**
     * Obtiene el valor de la propiedad requestHeader.
     *
     * @return
     *     possible object is
     *     {@link RequestHeaderType }
     *
     */
    public RequestHeaderType getRequestHeader() {
        return requestHeader;
    }

    /**
     * Define el valor de la propiedad requestHeader.
     *
     * @param value
     *     allowed object is
     *     {@link RequestHeaderType }
     *
     */
    public void setRequestHeader(RequestHeaderType value) {
        this.requestHeader = value;
    }

    /**
     * Obtiene el valor de la propiedad body.
     *
     * @return
     *     possible object is
     *     {@link SendEmailStandardREQType.Body }
     *
     */
    public SendEmailStandardREQType.Body getBody() {
        return body;
    }

    /**
     * Define el valor de la propiedad body.
     *
     * @param value
     *     allowed object is
     *     {@link SendEmailStandardREQType.Body }
     *
     */
    public void setBody(SendEmailStandardREQType.Body value) {
        this.body = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element ref="{http://cardif.cl/Schema/EBM/EmailStandard/send/v1.0}SendEmailStandardRequest"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sendEmailStandardRequest"
    })
    public static class Body {

        @XmlElement(name = "SendEmailStandardRequest", required = true)
        protected SendEmailStandardRequestType sendEmailStandardRequest;

        /**
         * Obtiene el valor de la propiedad sendEmailStandardRequest.
         * 
         * @return
         *     possible object is
         *     {@link SendEmailStandardRequestType }
         *     
         */
        public SendEmailStandardRequestType getSendEmailStandardRequest() {
            return sendEmailStandardRequest;
        }

        /**
         * Define el valor de la propiedad sendEmailStandardRequest.
         * 
         * @param value
         *     allowed object is
         *     {@link SendEmailStandardRequestType }
         *     
         */
        public void setSendEmailStandardRequest(SendEmailStandardRequestType value) {
            this.sendEmailStandardRequest = value;
        }

    }

}
