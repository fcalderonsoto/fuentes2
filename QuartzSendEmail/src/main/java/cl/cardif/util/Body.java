package cl.cardif.util;

public class Body {
	

	private String estado;
	private String nivel;
	private String cantidad;
	
	public Body(
		String estado,
		String nivel,
		String cantidad) {
		
		this.estado = estado;
		this.nivel = nivel;
		this.cantidad = cantidad;
		
		
	}
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNivel() {
		return nivel;
	}

	public void setNivel(String nivel) {
		this.nivel = nivel;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
}
