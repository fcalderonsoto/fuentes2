package cl.cardif.util;


public class Planilla {

	private String nombre;
	private String descripcion;
	private String cuerpo;
	private String ruta;
	private String[] destinatarios;
	private String fecha;

	public Planilla(String nombre, String descripcion, String cuerpo, String ruta, String[] destinatarios,
			String fecha) {

		this.nombre = nombre;
		this.descripcion = descripcion;
		this.cuerpo = cuerpo;
		this.ruta = ruta;
		this.destinatarios = destinatarios;
		this.fecha= fecha;
		

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}

	public String getRuta() {
		return ruta;
	}

	public void setRuta(String ruta) {
		this.ruta = ruta;
	}

	public String[] getDestinatarios() {
		return destinatarios;
	}

	public void setDestinatarios(String[] destinatarios) {
		this.destinatarios = destinatarios;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

}
