package cl.cardif.util;

import java.util.Calendar;
import java.sql.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtil {
	Logger logger = LoggerFactory.getLogger(getClass());
	private Date dateInicio;
	private Date dateTermino;
	private Date cronExecutionDay;
	private Boolean cronValidationDay;

	public Date getDateInicio() {
		return dateInicio;
	}

	public Date getDateTermino() {
		return dateTermino;
	}

	public void createDates() {
		Calendar sisDate = new GregorianCalendar();
				
		int mes = sisDate.get(Calendar.MONTH);
		int dia = sisDate.get(Calendar.DAY_OF_MONTH);
		int year = sisDate.get(Calendar.YEAR);

		boolean monday = sisDate.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY;
		if(monday) {
			Calendar cronDate = Calendar.getInstance();
			cronDate.set(Calendar.YEAR, year);
			cronDate.set(Calendar.MONTH, mes);
		    cronDate.set(Calendar.DAY_OF_MONTH,1);
			this.cronExecutionDay = new Date(cronDate.getTimeInMillis());
			this.cronValidationDay=true;
		}else {
			this.cronValidationDay=true;
		}
		
		Calendar fechaTermino = Calendar.getInstance();
		fechaTermino.set(Calendar.YEAR, year);
		fechaTermino.set(Calendar.MONTH, mes);
		fechaTermino.set(Calendar.DAY_OF_MONTH, dia);
		fechaTermino.add(Calendar.DAY_OF_MONTH, -1);
				
		this.dateTermino = new Date(fechaTermino.getTimeInMillis());
		
		Calendar fechaInicio = fechaTermino;
		fechaInicio.add(Calendar.DAY_OF_MONTH, -6);

		this.dateInicio = new Date(fechaTermino.getTimeInMillis());
		
	}

	public Date getCronExecutionDay() {
		return cronExecutionDay;
	}

	public void setCronExecutionDay(Date cronExecutionDay) {
		this.cronExecutionDay = cronExecutionDay;
	}

	public Boolean getCronValidationDay() {
		return cronValidationDay;
	}

	public void setCronValidationDay(Boolean cronValidationDay) {
		this.cronValidationDay = cronValidationDay;
	}

}
