package cl.cardif.soap;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.cardif.configuration.BusinessException;
import cl.cardif.configuration.MessageError;
import cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1.AttachmentsListType;
import cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1.AttachmentsType;
import cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1.BlindCarbonCopyElementType;
import cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1.BlindCarbonCopyListType;
import cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1.CarbonCopyElementType;
import cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1.CarbonCopyListType;
import cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1.RecipientAddressElementType;
import cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1.RecipientAddressListType;
import cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1.SendEmailStandardREQType;
import cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1.SendEmailStandardRSPType;
import cl.cardif.emailstandard.schema.ebm.emailstandard.send.v1.SendEmailStandardRequestType;
import cl.cardif.emailstandard.schema.eso.messageheader.v1.ChannelType;
import cl.cardif.emailstandard.schema.eso.messageheader.v1.ConsumerType;
import cl.cardif.emailstandard.schema.eso.messageheader.v1.CountryType;
import cl.cardif.emailstandard.schema.eso.messageheader.v1.RequestHeaderType;
import cl.cardif.emailstandard.schema.eso.messageheader.v1.ServiceType;
import cl.cardif.emailstandard.schema.eso.messageheader.v1.TraceType;
import cl.cardif.emailstandard.v1.EmailStandardPortType;
import cl.cardif.emailstandard.v1.SendFaultMessage;
import cl.cardif.util.Body;
import cl.cardif.util.Planilla;

@Service
public class EmailStandardClient {

  Logger logger = LoggerFactory.getLogger(getClass());

  @Autowired EmailStandardPortType port;

  public SendEmailStandardRSPType sendEmail(List<Body> body, Planilla planilla) throws BusinessException{

    try {
    	RecipientAddressListType recipient= new RecipientAddressListType();
  	    	
    	CarbonCopyListType cblist = new CarbonCopyListType();
        CarbonCopyElementType cE = new CarbonCopyElementType();
        cE.setCarbonCopyAddress("");
        cblist.getCarbonCopyElement().add(cE);
        
        BlindCarbonCopyListType bC = new BlindCarbonCopyListType();
        BlindCarbonCopyElementType bE = new BlindCarbonCopyElementType();
        bE.setBlindCarbonCopyAddress("");
        bC.getBlindCarbonCopyElement().add(bE);
        
        AttachmentsListType att = new AttachmentsListType();
        AttachmentsType attE = new AttachmentsType();
        attE.setAttachment("");
        attE.setDocumentType("");
        att.getAttachments().add(attE);
    	
      SendEmailStandardREQType req = new SendEmailStandardREQType();

      req.setRequestHeader(new RequestHeaderType());

      req.getRequestHeader().setConsumer(new ConsumerType());
      req.getRequestHeader().setChannel(new ChannelType());
      req.getRequestHeader().setCountry(new CountryType());
      req.getRequestHeader().setTrace(new TraceType());

      req.getRequestHeader().getConsumer().setCode(1);
      req.getRequestHeader().getConsumer().setName("Cardif");

     
      req.getRequestHeader().getTrace().setService(new ServiceType());
      req.getRequestHeader().getTrace().getService().setCode("1");
      req.getRequestHeader().getTrace().getService().setName("mail");
      req.getRequestHeader().getTrace().getService().setOperation("send");


      req.getRequestHeader().getCountry().setCode(1);
      req.getRequestHeader().getCountry().setName("LAM");

      req.getRequestHeader().getChannel().setCode("NI");
      req.getRequestHeader().getChannel().setMode("NI");
      req.setBody(new SendEmailStandardREQType.Body());
      req.getBody().setSendEmailStandardRequest(new SendEmailStandardRequestType());
     
      String[] destinatario = planilla.getDestinatarios();
      for(int x = 0; x<destinatario.length;x++) {
    	  RecipientAddressElementType elementoRecipient = new RecipientAddressElementType();
    	 elementoRecipient.setRecipientAddress(destinatario[x]);
      	recipient.getRecipientAddressElement().add(elementoRecipient); 
      }     
      req.getBody().getSendEmailStandardRequest().setRecipientAddressList(recipient); 
      req.getBody().getSendEmailStandardRequest().setCarbonCopyList(cblist);
      req.getBody().getSendEmailStandardRequest().setBlindCarbonCopyList(bC);
      req.getBody().getSendEmailStandardRequest().setSubject(planilla.getDescripcion());
      if(!body.isEmpty()) {
    	  String cuerpo = "<tr style=\"border: 1px solid black;\" ><td style=\"border: 1px solid black;font-weight: bold\">Estado</td><td style=\"border: 1px solid black;font-weight: bold\">Nivel</td><td style=\"border: 1px solid black;font-weight: bold\">Cantidad</td></tr>";
    	  String cuerpoTmp ="";
    	  for(int x =0 ; x<body.size();x++) {
    		  cuerpoTmp =cuerpoTmp+ "<tr style= \"border: 1px solid black;\"> <td style= \"border: 1px solid black;\">"+body.get(x).getEstado()+"</td><td style= \"border: 1px solid black;\">"+body.get(x).getNivel()+"</td><td style= \"border: 1px solid black;\">"+body.get(x).getCantidad() +"</td> </tr>";
          }
    	  cuerpo ="<table style=\"max-width: 600px; margin: 0 auto;border-collapse: collapse;\"\r\n" + 
    	  		"width=\"600\" align=\"left\"><tbody><tr style=\"height: 50px;\"><td style=\"padding: 3% 4.5% 0px; height: 50px;\"><p>Estimados :</p>\r\n" + 
    	  		"<p>Durante la última semana se han generado pólizas y\r\n" + 
    	  		"certificados :</p><table style=\"max-width: 600px; margin: 0 auto; border-collapse: collapse;\"\r\n" + 
    	  		"width=\"600\" align=\"center\">"+cuerpo + cuerpoTmp+"</table><p>Para mayor detalle de las pólizas y certificados\r\n" + 
    	  		"generados o con generación fallida dirigirse a:"+ planilla.getRuta()+"</p></td></tr></tbody></table>";
    	  req.getBody().getSendEmailStandardRequest().setBody(cuerpo);
      }else {
    	  req.getBody().getSendEmailStandardRequest().setBody(""); 
      }
      req.getBody().getSendEmailStandardRequest().setAttachmentsList(att);
      return port.send(req);

    } catch (SendFaultMessage e) {
      logger.error(e.getMessage(), e);
      throw new BusinessException(MessageError.INTERNAL_ERROR);
    }
  }
}
