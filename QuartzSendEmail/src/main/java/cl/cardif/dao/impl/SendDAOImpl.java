package cl.cardif.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cl.cardif.dao.SendDAO;
import cl.cardif.util.Body;
import cl.cardif.util.CIUSEConstants;
import cl.cardif.util.Planilla;
import oracle.jdbc.OracleTypes;

@Repository
public class SendDAOImpl implements SendDAO {

	@Autowired
	DataSource polizaDs;
	private Logger logger = LoggerFactory.getLogger(SendDAOImpl.class);

	public List<Body> getBody(Date fechaInicio, Date fechaTermino) {
		List<Body> bodyList = new ArrayList<>();
		Body cuerpo;
		try (Connection conn = polizaDs.getConnection();
				CallableStatement call = conn.prepareCall(CIUSEConstants.RPT_RESUMEN)) {
			conn.setAutoCommit(false);
			call.setDate(1, fechaInicio);
			call.setDate(2, fechaTermino);
			call.registerOutParameter(3, OracleTypes.CURSOR);
			call.registerOutParameter(4, OracleTypes.NUMBER);
			call.registerOutParameter(5, OracleTypes.VARCHAR);
			logger.info("llamada executeQuery");
			call.execute();
			if (call.getObject(3) != null) {
				ResultSet resultSet = (ResultSet) call.getObject(3);
				while (resultSet.next()) {
					cuerpo = new Body(resultSet.getString("ESTADO") != null ? resultSet.getString("ESTADO") : "-",
							resultSet.getString("NIVEL") != null ? resultSet.getString("NIVEL") : "-",
							resultSet.getString("CANTIDAD") != null ? resultSet.getString("CANTIDAD") : "-");
					bodyList.add(cuerpo);

				}
				resultSet.close();
				conn.commit();
				//conn.setAutoCommit(true);
			}
		} catch (SQLException e) {
			logger.error("", e);
		}
		return bodyList;
	}

	@Override
	public Planilla getPlanilla() {
		Planilla planilla = null;
		String[] destinatarios;
		try (Connection conn = polizaDs.getConnection();
				CallableStatement call = conn.prepareCall(CIUSEConstants.PRC_GET_PLANTILLA)) {
			conn.setAutoCommit(false);
			call.setInt(1, 1 );
			call.setString(2, "REP_TRAZA_COPIAPOLIZA");
			call.registerOutParameter(3, OracleTypes.CURSOR);
			call.registerOutParameter(4, OracleTypes.NUMBER);
			call.registerOutParameter(5, OracleTypes.VARCHAR);
			logger.info("llamada executeQuery getPlanilla");
			call.execute();
			if(call.getObject(3)!= null){
				ResultSet resultSet = (ResultSet) call.getObject(3);
				while (resultSet.next()) {
					destinatarios = resultSet.getString("DESTINATARIOS").split(";");
					planilla = new Planilla(resultSet.getString("NOMBRE"),
							resultSet.getString("DESCRIPCION"),
							resultSet.getString("CUERPO"),
							resultSet.getString("RUTA_REPORTE"),
							destinatarios,
							resultSet.getString("FECHA"));
				}
				resultSet.close();
				conn.commit();
				conn.setAutoCommit(true);
			}
		} catch (SQLException e) {
			logger.error("", e);
		}
		return planilla;
	}

}
