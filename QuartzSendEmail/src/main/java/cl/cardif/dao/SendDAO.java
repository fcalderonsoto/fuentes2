package cl.cardif.dao;


import java.sql.Date;
import java.util.List;

import cl.cardif.util.Body;
import cl.cardif.util.Planilla;

public interface SendDAO {
  
  List<Body> getBody(Date fechaInicio, Date fechaTermino);
  Planilla getPlanilla();
  
}
