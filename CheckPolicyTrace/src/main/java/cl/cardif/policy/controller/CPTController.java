package cl.cardif.policy.controller;

import cl.cardif.policy.domain.request.RequestCPT;
import cl.cardif.policy.domain.response.ResponseCPT;
import cl.cardif.policy.service.CPTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class CPTController {

  @Autowired
  CPTService CPTService;

  @PostMapping(
      value = "insert-trace",
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<ResponseCPT> insert(@RequestBody RequestCPT requestCPT) {
    return new ResponseEntity<>(CPTService.insert(requestCPT), HttpStatus.OK);
  }
}
