package cl.cardif.policy.tools;

public class CPTMessages {

  public static final String OK = "Servicio se ejecuto correctamente";
  public static final String ERROR_MANDATORY = "Error en los campos de entrada obligatorios";
  public static final String ERROR_STORE_PROCEDURE = "Error en el Procedimiento Almacenado : ";
  public static final String ERROR_STORE_PROCEDURE_2 = "Error en la respuesta del Procedimiento Almacenado";
}
