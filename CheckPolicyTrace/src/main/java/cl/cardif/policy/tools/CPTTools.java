package cl.cardif.policy.tools;

import cl.cardif.policy.domain.request.RequestCPT;
import org.springframework.stereotype.Service;

@Service
public class CPTTools {

  public boolean mandatoryInput(RequestCPT requestCPT) {
    return requestCPT.getCheckPolicyTraceREQ().getInsuredDni() != null
        && !requestCPT.getCheckPolicyTraceREQ().getInsuredDni().isEmpty()
        && requestCPT.getCheckPolicyTraceREQ().getInsuredDate() != null
        && !requestCPT.getCheckPolicyTraceREQ().getInsuredDate().isEmpty()
        && requestCPT.getCheckPolicyTraceREQ().getOperationNumber() != null
        && !requestCPT.getCheckPolicyTraceREQ().getOperationNumber().isEmpty()
        && requestCPT.getCheckPolicyTraceREQ().getPolicyNumber() != null
        && !requestCPT.getCheckPolicyTraceREQ().getPolicyNumber().isEmpty()
        && requestCPT.getCheckPolicyTraceREQ().getLevelId() != null
        && requestCPT.getCheckPolicyTraceREQ().getLevelId().longValue() >= 0
        && requestCPT.getCheckPolicyTraceREQ().getStateId() != null
        && requestCPT.getCheckPolicyTraceREQ().getStateId().longValue() >= 0
        && requestCPT.getCheckPolicyTraceREQ().getDescription() != null
        && !requestCPT.getCheckPolicyTraceREQ().getDescription().isEmpty();
  }
}
