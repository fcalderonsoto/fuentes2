package cl.cardif.policy.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestCPT {
  @JsonProperty("check_policy_trace_REQ")
  private CheckPolicyTraceREQ CheckPolicyTraceREQ;

  public CheckPolicyTraceREQ getCheckPolicyTraceREQ() {
    return CheckPolicyTraceREQ;
  }
}
