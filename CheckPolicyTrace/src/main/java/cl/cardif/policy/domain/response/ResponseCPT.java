package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;


public class ResponseCPT {

  @JsonProperty("check_policy_trace_RSP")
  private CheckPolicyTraceRSP checkPolicyTraceRSP;

  public ResponseCPT(CheckPolicyTraceRSP checkPolicyTraceRSP) {
	    this.checkPolicyTraceRSP = checkPolicyTraceRSP;
	  }

	public CheckPolicyTraceRSP getCheckPolicyTraceRSP() {
		return checkPolicyTraceRSP;
	}

}
