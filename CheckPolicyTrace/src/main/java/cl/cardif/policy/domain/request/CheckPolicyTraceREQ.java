package cl.cardif.policy.domain.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckPolicyTraceREQ {

  @JsonProperty("insuredDni")
  private String insuredDni;

  @JsonProperty("operationNumber")
  private String operationNumber;

  @JsonProperty("policyNumber")
  private String policyNumber;

  @JsonProperty("insuredDate")
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
  private String insuredDate;

  @JsonProperty("levelId")
  private Integer levelId;

  @JsonProperty("stateId")
  private Integer stateId;

  @JsonProperty("description")
  private String description;

  public Integer getLevelId() {
    return levelId;
  }

  public Integer getStateId() {
    return stateId;
  }

  public String getDescription() {
    return description;
  }

  public String getInsuredDni() {
    return insuredDni;
  }

  public String getOperationNumber() {
    return operationNumber;
  }

  public String getPolicyNumber() {
    return policyNumber;
  }

  public String getInsuredDate() {
    return insuredDate;
  }

}
