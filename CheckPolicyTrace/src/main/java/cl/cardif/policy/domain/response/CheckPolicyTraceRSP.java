package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CheckPolicyTraceRSP {
  @JsonProperty("Message")
  private Message message;
  
  @JsonProperty("transactionId")
  private long transactionId;

  public CheckPolicyTraceRSP(
		  final Message message,
		  final long transactionId) {
    this.message = message;
    this.transactionId = transactionId;
  }
}
