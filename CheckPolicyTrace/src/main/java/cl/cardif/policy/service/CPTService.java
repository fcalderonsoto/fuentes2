package cl.cardif.policy.service;

import cl.cardif.policy.domain.request.RequestCPT;
import cl.cardif.policy.domain.response.ResponseCPT;

public interface CPTService {

  ResponseCPT insert(RequestCPT requestCPT);
}
