package cl.cardif.policy.service.impl;

import cl.cardif.policy.dao.CPTInsertDao;
import cl.cardif.policy.domain.request.RequestCPT;
import cl.cardif.policy.domain.response.CheckPolicyTraceRSP;
import cl.cardif.policy.domain.response.Message;
import cl.cardif.policy.domain.response.ResponseCPT;
import cl.cardif.policy.exception.CPTException;
import cl.cardif.policy.service.CPTService;
import cl.cardif.policy.tools.CPTMessages;
import cl.cardif.policy.tools.CPTTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CPTServiceImpl implements CPTService {

  private static Logger logger = LoggerFactory.getLogger(CPTServiceImpl.class);

  @Autowired CPTTools CPTTools;

  @Autowired CPTInsertDao CPTInsertDao;

  @Override
  public ResponseCPT insert(RequestCPT requestCPT) {
    try {
      long status = 0;
      if (CPTTools.mandatoryInput(requestCPT)) {
        status = CPTInsertDao.insertTrace(requestCPT.getCheckPolicyTraceREQ());
        return new ResponseCPT(new CheckPolicyTraceRSP(new Message(CPTMessages.OK, 10), status));
      } else {
        logger.error(CPTMessages.ERROR_MANDATORY);
        return new ResponseCPT(
            new CheckPolicyTraceRSP(new Message(CPTMessages.ERROR_MANDATORY, 11), 0));
      }
    } catch (CPTException e) {
      logger.error(CPTMessages.ERROR_STORE_PROCEDURE + e.getMessage());
      return new ResponseCPT(
          new CheckPolicyTraceRSP(
              new Message(CPTMessages.ERROR_STORE_PROCEDURE + e.getMessage(), 12), 0));
    }
  }
}
