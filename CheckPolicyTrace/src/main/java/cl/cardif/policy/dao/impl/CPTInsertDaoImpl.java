package cl.cardif.policy.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import cl.cardif.policy.dao.CPTInsertDao;
import cl.cardif.policy.domain.request.CheckPolicyTraceREQ;
import cl.cardif.policy.exception.CPTException;
import cl.cardif.policy.tools.CPTConstants;
import oracle.jdbc.OracleTypes;

@Repository
public class CPTInsertDaoImpl implements CPTInsertDao {

  @Override
  public long insertTrace(CheckPolicyTraceREQ checkPolicyTraceREQ) throws CPTException {
	long transactionId = 0;
    try (Connection conn =
            ((DataSource)
                    ((Context) new InitialContext().lookup(CPTConstants.JAVA_COMP))
                        .lookup(CPTConstants.JNDI_POLI))
                .getConnection();
        CallableStatement call = conn.prepareCall(CPTConstants.PRC_INSERT_TRACE)) {
      conn.setAutoCommit(false);
      call.setString(1, checkPolicyTraceREQ.getInsuredDni());
      call.setString(2, checkPolicyTraceREQ.getOperationNumber());
      call.setString(3, checkPolicyTraceREQ.getPolicyNumber());
      call.setString(4, checkPolicyTraceREQ.getInsuredDate());
      call.setLong(5, checkPolicyTraceREQ.getLevelId());
      call.setLong(6, checkPolicyTraceREQ.getStateId());
      call.setString(7, checkPolicyTraceREQ.getDescription());
      call.registerOutParameter(8, OracleTypes.NUMBER);
      call.registerOutParameter(9, OracleTypes.VARCHAR);
      call.registerOutParameter(10, OracleTypes.NUMBER);
      call.execute();
      if(call.getInt(8) == 0) {
    	  transactionId = call.getInt(10);
      }else{
    	  throw new CPTException(call.getString(9), HttpStatus.INTERNAL_SERVER_ERROR, null);
      }
      conn.commit();
      conn.setAutoCommit(true);
    } catch (SQLException | NamingException e) {
      throw new CPTException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
    }
    return transactionId;
  }
}
