package cl.cardif.policy.dao;

import cl.cardif.policy.domain.request.CheckPolicyTraceREQ;
import cl.cardif.policy.exception.CPTException;

public interface CPTInsertDao {

	long insertTrace(CheckPolicyTraceREQ checkPolicyTraceREQ) throws CPTException;
}
