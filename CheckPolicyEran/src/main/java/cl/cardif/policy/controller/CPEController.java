package cl.cardif.policy.controller;

import cl.cardif.policy.domain.request.RequestCPE;
import cl.cardif.policy.domain.response.ResponseCPE;
import cl.cardif.policy.service.CPEService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class CPEController {

  @Autowired CPEService cpeService;

  @PostMapping(
      value = "check-policy",
      consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<ResponseCPE> validate(@RequestBody RequestCPE requestCPE) {
    return new ResponseEntity<>(cpeService.validate(requestCPE), HttpStatus.OK);
  }
}
