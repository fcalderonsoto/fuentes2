package cl.cardif.policy.tools;


import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import cl.cardif.policy.domain.model.InputPolicy;
import cl.cardif.policy.domain.request.RequestCPE;

@Service
public class CPETools {
  public String dateToString(Date date) {
    if (date != null) {
      DateFormat df = new SimpleDateFormat(CPEConstants.FORMAT_DATE);
      return df.format(date);
    }
    return CPEConstants.EMPTY;
  }
	public String formatDB(BigDecimal value) {
		if (value == null) {
			BigDecimal bd = BigDecimal.ZERO;
			value = bd;
		}
//		return value.setScale(4, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString();
		if(value.setScale(4).toPlainString().equals("0.0000")) {
			value = BigDecimal.ZERO;
			return value.toPlainString();
		}else {
			return	value.setScale(4).toPlainString();
		}
		
	}
	public String formatDBTmp(BigDecimal value) {
		if (value == null) {
			BigDecimal bd = BigDecimal.ZERO;
			value = bd;
		}
			return value.toPlainString();
		
	}
	 public String formatNumber(BigDecimal value){
		 if(value != null) {
			 DecimalFormat df = new DecimalFormat("#,###.#", new DecimalFormatSymbols(new Locale("pt", "BR")));
			  return df.format(value.floatValue());
		 }else {
		  	return "";
		  }
	  }
  
  public InputPolicy transform(RequestCPE requestCP) {
    return new InputPolicy(
        requestCP.getCheckPolicyEranREQ().getInsuredDni(),
        requestCP.getCheckPolicyEranREQ().getOperationNumber(),
        requestCP.getCheckPolicyEranREQ().getPolicyNumber(),
        requestCP.getCheckPolicyEranREQ().getInsuredDate());
  }
  
  public boolean mandatoryInput(RequestCPE requestCP) {
	    return requestCP.getCheckPolicyEranREQ().getInsuredDni() != null
	        && !requestCP.getCheckPolicyEranREQ().getInsuredDni().isEmpty()
	        && requestCP.getCheckPolicyEranREQ().getInsuredDate() != null
	        && !requestCP.getCheckPolicyEranREQ().getInsuredDate().isEmpty()
	        && requestCP.getCheckPolicyEranREQ().getOperationNumber() != null
	        && !requestCP.getCheckPolicyEranREQ().getOperationNumber().isEmpty()
	        && !(requestCP.getCheckPolicyEranREQ().getOperationNumber().length()>50)
	        && requestCP.getCheckPolicyEranREQ().getPolicyNumber() != null
	        && !requestCP.getCheckPolicyEranREQ().getPolicyNumber().isEmpty()
	        && !(requestCP.getCheckPolicyEranREQ().getPolicyNumber().length() > 20)
	        && requestCP.getCheckPolicyEranREQ().getPolicyType() != null
	        && !requestCP.getCheckPolicyEranREQ().getPolicyType().isEmpty();	       
	  }
}
