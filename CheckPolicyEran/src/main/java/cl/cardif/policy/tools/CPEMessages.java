package cl.cardif.policy.tools;

public class CPEMessages {

  public static final String OK = "Servicio se ejecuto correctamente";
  public static final String ERROR_FLAG = "Error en la bandera de tipo de poliza";
  public static final String ERROR_VALIDATION_DNI_OR_DATE = "Error en la validacion del rut asegurado o en la fecha vigencia";
  public static final String ERROR_MANDATORY = "Error en los campos de entrada obligatorios";
  public static final String ERROR_DATE = "Error en la fecha de vigencia";
  public static final String ERROR_STORE_PROCEDURE = "Error en el Procedimiento Almacenado : ";
}
