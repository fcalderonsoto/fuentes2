package cl.cardif.policy.domain.response;

public class People {

  private String documentNumber;
  private String documentVerificationDigit;
  private String name;
  private String typeCode;
  private String type;
  private String address;
  private String addressCommune;
  private String addressCity;
  private String phoneNumber;
  private String email;
  private String birthDate;
  private String addressType;
  private String mobilePhoneNumber;

  public People(
      String documentNumber,
      String documentVerificationDigit,
      String name,
      String typeCode,
      String type,
      String address,
      String addressCommune,
      String addressCity,
      String phoneNumber,
      String email,
      String birthDate,
      String addressType,
      String mobilePhoneNumber
      ) {
    this.documentNumber = documentNumber;
    this.documentVerificationDigit = documentVerificationDigit;
    this.name = name;
    this.typeCode = typeCode;
    this.type = type;
    this.address = address;
    this.addressCommune = addressCommune;
    this.addressCity = addressCity;
    this.phoneNumber = phoneNumber;
    this.email = email;
    this.birthDate = birthDate;
    this.addressType = addressType;
    this.mobilePhoneNumber = mobilePhoneNumber;
  }

  public String getDocumentNumber() {
    return documentNumber;
  }

  public String getDocumentVerificationDigit() {
    return documentVerificationDigit;
  }

  public String getName() {
    return name;
  }

  public String getTypeCode() {
    return typeCode;
  }

  public String getType() {
    return type;
  }

  public String getAddress() {
    return address;
  }

  public String getAddressCommune() {
    return addressCommune;
  }

  public String getAddressCity() {
    return addressCity;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public String getEmail() {
    return email;
  }

  public String getBirthDate() {
    return birthDate;
  }

  public String getAddressType() {
    return addressType;
  }

  public String getMobilePhoneNumber() {
    return mobilePhoneNumber;
  }

}
