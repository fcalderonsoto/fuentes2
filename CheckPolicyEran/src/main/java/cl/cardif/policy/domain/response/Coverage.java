package cl.cardif.policy.domain.response;

public class Coverage {

  private String personName;	
  private long coverageCode;
  private String coverageDesc;
  private String capital;
  private String grossPrime;

  public Coverage(
		  final String personName,
          final long coverageCode,
          final String coverageDesc,
          final String capital,
          final String grossPrime) {
	this.personName = personName;
    this.coverageCode = coverageCode;
    this.coverageDesc = coverageDesc;
    this.capital = capital;
    this.grossPrime = grossPrime;
  }


  public String getPersonName() {
	return personName;
  }
  public long getCoverageCode() {
    return coverageCode;
  }

  public String getCoverageDesc() {
    return coverageDesc;
  }

  public String getCapital() {
    return capital;
  }

  public String getGrossPrime() {
    return grossPrime;
  }
}
