package cl.cardif.policy.domain.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseCPE {
  
	@JsonProperty("check_policy_RSP")
	private CheckPolicyEranRSP checkPolicyEranRSP;

	@JsonProperty("Message")
	private Message message;

	public ResponseCPE(CheckPolicyEranRSP checkPolicyEranRSP, Message message) {
		this.checkPolicyEranRSP = checkPolicyEranRSP;
		this.message = message;
	}
}
