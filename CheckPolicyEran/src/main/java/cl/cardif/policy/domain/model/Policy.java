package cl.cardif.policy.domain.model;

import java.sql.Date;

public class Policy {

  private String insuredDni;
  private String operationNumber;
  private String policyNumber;
  private Date insuredDate;

  public Policy(
      final String insuredDni,
      final String operationNumber,
      final String policyNumber,
      final Date insuredDate) {
    this.insuredDni = insuredDni;
    this.operationNumber = operationNumber;
    this.policyNumber = policyNumber;
    this.insuredDate = insuredDate;
  }

  public String getInsuredDni() {
    return insuredDni;
  }

  public String getOperationNumber() {
    return operationNumber;
  }

  public String getPolicyNumber() {
    return policyNumber;
  }

  public Date getInsuredDate() {
    return insuredDate;
  }
}
