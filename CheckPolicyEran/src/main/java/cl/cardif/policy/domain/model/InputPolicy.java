package cl.cardif.policy.domain.model;

public class InputPolicy {

  private String insuredDni;
  private String operationNumber;
  private String policyNumber;
  private String insuredDate;

  public InputPolicy(
      final String insuredDni,
      final String operationNumber,
      final String policyNumber,
      final String insuredDate) {
    this.insuredDni = insuredDni;
    this.operationNumber = operationNumber;
    this.policyNumber = policyNumber;
    this.insuredDate = insuredDate;
  }

  public String getInsuredDni() {
    return insuredDni;
  }

  public String getOperationNumber() {
    return operationNumber;
  }

  public String getPolicyNumber() {
    return policyNumber;
  }

  public String getInsuredDate() {
    return insuredDate;
  }
}
