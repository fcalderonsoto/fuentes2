package cl.cardif.policy.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestCPE {
	@JsonProperty("check_policy_REQ")
	private CheckPolicyEranREQ checkPolicyEranREQ;

	public CheckPolicyEranREQ getCheckPolicyEranREQ() {
		return checkPolicyEranREQ;
	}

}
