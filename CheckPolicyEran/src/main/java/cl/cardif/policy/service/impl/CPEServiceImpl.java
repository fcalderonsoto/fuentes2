package cl.cardif.policy.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.cardif.policy.dao.CPECheckValidityDao;
import cl.cardif.policy.domain.request.RequestCPE;
import cl.cardif.policy.domain.response.CheckPolicyEranRSP;
import cl.cardif.policy.domain.response.Message;
import cl.cardif.policy.domain.response.ResponseCPE;
import cl.cardif.policy.exception.CPEException;
import cl.cardif.policy.service.CPEService;
import cl.cardif.policy.tools.CPEConstants;
import cl.cardif.policy.tools.CPEMessages;
import cl.cardif.policy.tools.CPETools;

@Service
public class CPEServiceImpl implements CPEService {

  private static Logger logger = LoggerFactory.getLogger(CPEServiceImpl.class);

  @Autowired CPETools cpeTools;

  @Autowired CPECheckValidityDao cpeCheckValidityDao;

  @Override
  public ResponseCPE validate(RequestCPE requestCP) {
    try {
        if (cpeTools.mandatoryInput(requestCP)) {	
		      if (requestCP.getCheckPolicyEranREQ().getPolicyType().equalsIgnoreCase(CPEConstants.FLAG)) {
		        CheckPolicyEranRSP checkPolicyEranRSP =
		            cpeCheckValidityDao.checkEnsureValidityEran(cpeTools.transform(requestCP));
		        return new ResponseCPE(checkPolicyEranRSP, new Message(CPEMessages.OK, 10));
		      } else {
		        logger.error(CPEMessages.ERROR_FLAG);
		        return new ResponseCPE(null, new Message(CPEMessages.ERROR_FLAG, 11));
		      }
        }else {
	        logger.error(CPEMessages.ERROR_MANDATORY);
	        return new ResponseCPE(null, new Message(CPEMessages.ERROR_MANDATORY, 12));
      }
    } catch (CPEException e) {
      logger.error(CPEMessages.ERROR_STORE_PROCEDURE + e.getMessage());
      return new ResponseCPE(null, new Message(CPEMessages.ERROR_STORE_PROCEDURE + e.getMessage(), 13));
    }
  }
}
