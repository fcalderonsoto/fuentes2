package cl.cardif.policy.dao.impl;

import cl.cardif.policy.dao.CPECheckValidityDao;
import cl.cardif.policy.domain.model.InputPolicy;
import cl.cardif.policy.domain.response.CheckPolicyEranRSP;
import cl.cardif.policy.domain.response.Coverage;
import cl.cardif.policy.domain.response.People;
import cl.cardif.policy.domain.response.Policy;
import cl.cardif.policy.exception.CPEException;
import cl.cardif.policy.tools.CPEConstants;
import cl.cardif.policy.tools.CPETools;
import oracle.jdbc.OracleTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CPECheckValidityDaoImpl implements CPECheckValidityDao {
  @Autowired CPETools cpeTools;

  @Override
  public CheckPolicyEranRSP checkEnsureValidityEran(InputPolicy inputPolicy) throws CPEException {
    CheckPolicyEranRSP checkPolicyEranRSP = null;
    try (Connection conn =
            ((DataSource)
                    ((Context) new InitialContext().lookup(CPEConstants.JAVA_COMP))
                        .lookup(CPEConstants.JNDI_ERAN))
                .getConnection();
        CallableStatement call = conn.prepareCall(CPEConstants.PRC_CHECK_VALIDITY)) {
      conn.setAutoCommit(false);
      call.setString(1, inputPolicy.getInsuredDni());
      call.setString(2, inputPolicy.getOperationNumber());
      call.setString(3, inputPolicy.getPolicyNumber());
      call.setString(4, inputPolicy.getInsuredDate());
      call.registerOutParameter(5, OracleTypes.NUMBER);
      call.registerOutParameter(6, OracleTypes.VARCHAR);
      call.registerOutParameter(7, OracleTypes.VARCHAR);
      call.registerOutParameter(8, OracleTypes.VARCHAR);
      call.registerOutParameter(9, OracleTypes.NUMBER);
      call.registerOutParameter(10, OracleTypes.VARCHAR);
      call.registerOutParameter(11, OracleTypes.CURSOR);
      call.registerOutParameter(12, OracleTypes.CURSOR);
      call.registerOutParameter(13, OracleTypes.CURSOR);
      call.execute();
      if (call.getInt(9) == 0) {
        List<Policy> policyList = setPolicies(call);
        List<People> peopleList = setPeople(call);
        List<Coverage> coverageList = setCoverages(call);
        checkPolicyEranRSP =
            new CheckPolicyEranRSP(
                call.getLong(5),
                call.getString(6),
                call.getString(7),
                call.getString(8),
                !policyList.isEmpty() ? policyList.get(0) : null,
                peopleList,
                coverageList);
      }else {
    	  throw new CPEException(call.getString(10), HttpStatus.INTERNAL_SERVER_ERROR, null);
      }
      conn.commit();
      conn.setAutoCommit(true);
    } catch (SQLException | NamingException e) {
      throw new CPEException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR, e);
    }
    return checkPolicyEranRSP;
  }

  private List<People> setPeople(CallableStatement call) throws SQLException {
	    List<People> peopleList = new ArrayList<>();
	    ResultSet rsPeople = (ResultSet) call.getObject(13);
	    People people;
	    while (rsPeople.next()) {
	    	people =
	          new People(
	              rsPeople.getString("InfoTitular_Rut"),
	              rsPeople.getString("InfoTitular_Digito"),
	              rsPeople.getString("InfoTitular_Nombre"),
	              rsPeople.getString("InfoTitular_Tipo"),
	              rsPeople.getString("TIPO_PER"),	              
	              rsPeople.getString("InfoTitular_Domicilio"),
	              rsPeople.getString("InfoTitular_Comuna"),
	              rsPeople.getString("InfoTitular_Ciudad"),
	              rsPeople.getString("InfoTitular_Telefono"),
	              rsPeople.getString("InfoTitular_Email"),
	              rsPeople.getString("InfoTitular_FechaNacimiento"),
	              rsPeople.getString("InfoTitular_TipoDomicilio"),
	              rsPeople.getString("InfoTitular_CodigoPersona"));
	      peopleList.add(people);
	    }
	    rsPeople.close();
		return peopleList;
	  }
  
  private List<Coverage> setCoverages(CallableStatement call) throws SQLException {
    List<Coverage> coverageList = new ArrayList<>();
    Coverage coverage;
    ResultSet rsCoverage = (ResultSet) call.getObject(12);
    while (rsCoverage.next()) {
      coverage =
          new Coverage(
        	  rsCoverage.getString("TIPO_PER"),
              rsCoverage.getLong("Lista_InfoTitular_InfoCobertura_Codigo"),
              rsCoverage.getString("Lista_InfoTitular_InfoCobertura_Descripcion"),
              cpeTools.formatDB(rsCoverage.getBigDecimal("Lista_InfoTitular_InfoCobertura_Capital")),
              cpeTools.formatDB(rsCoverage.getBigDecimal("Lista_InfoTitular_InfoCobertura_PrimaBruta")));
      coverageList.add(coverage);
    }
    rsCoverage.close();
    return coverageList;
  }

  private List<Policy> setPolicies(CallableStatement call) throws SQLException {
    List<Policy> policyList = new ArrayList<>();
    Policy policy;
    ResultSet rsPolicies = (ResultSet) call.getObject(11);
    while (rsPolicies.next()) {
      policy =
          new Policy(
              rsPolicies.getString("lprNumeroOperacion"),
              rsPolicies.getString("Mail"),
              rsPolicies.getString("AutorizaMail"),
              rsPolicies.getString("LprSocioRut"),
              rsPolicies.getString("SocioNombre"),
              rsPolicies.getString("CodPoliza"),
              rsPolicies.getString("CodCertificado"),
              cpeTools.dateToString(rsPolicies.getDate("FechaVigenciaIni")),
              cpeTools.dateToString(rsPolicies.getDate("FechaVigenciaFin")),
              cpeTools.dateToString(rsPolicies.getDate("FechaVigenciaCalc")),
              cpeTools.dateToString(rsPolicies.getDate("FechaEmision")),
              cpeTools.dateToString(rsPolicies.getDate("FechaVenta")),
              cpeTools.dateToString(rsPolicies.getDate("Periodo")),
              cpeTools.dateToString(rsPolicies.getDate("FechaCambioEstado")),
              rsPolicies.getString("PeriodicidadPago"),
              rsPolicies.getLong("NroAsegurado"),
              rsPolicies.getString("ProdPrimaAfec"),
              rsPolicies.getString("ProdPrimaExc"),
              rsPolicies.getString("ProdPrimaNeta"),
              rsPolicies.getString("ProdIva"),
              rsPolicies.getString("ProdPrimaBruta"),
              rsPolicies.getString("ProdPrbrutaMes"),
              rsPolicies.getString("ProdCodigo"),
              rsPolicies.getString("ProdDescripcion"),
              rsPolicies.getString("PlanCodigo"),
              rsPolicies.getString("PlanDescripcion"),
              rsPolicies.getString("ModalidadPago"),
              rsPolicies.getString("CertPlan"),
              rsPolicies.getString("Domicilio"),
              rsPolicies.getString("DomicilioNumero"),
              rsPolicies.getString("Comuna"),
              rsPolicies.getString("Ciudad"),
              rsPolicies.getString("Telefono"),
              rsPolicies.getString("Email"),
              rsPolicies.getString("TipoCredito"),
              rsPolicies.getLong("PlazoCredito"),
              cpeTools.formatNumber(rsPolicies.getBigDecimal("MontoCredito")),
              cpeTools.formatNumber(rsPolicies.getBigDecimal("TotTitPrimaAfecta")),
              cpeTools.formatNumber(rsPolicies.getBigDecimal("TotTitPrimaExcenta")),
              cpeTools.formatNumber(rsPolicies.getBigDecimal("TotTitPrimaNeta")),
              cpeTools.formatNumber(rsPolicies.getBigDecimal("TotTitIva")),
              cpeTools.formatNumber(rsPolicies.getBigDecimal("TotTitPrimaBruta")),
              rsPolicies.getString("CapitalAsegurado"),
              rsPolicies.getString("CampoLibre"),
              rsPolicies.getString("Mutuo"),
              rsPolicies.getString("PolizaColectiva"),
              rsPolicies.getLong("IndiceTmk"),
              rsPolicies.getString("CampanaTmk"),
              rsPolicies.getLong("EdadMaxPermanencia"),
              cpeTools.formatNumber(rsPolicies.getBigDecimal("Capital")),
              rsPolicies.getString("SaldoInsoluto"),
              rsPolicies.getString("TipoAsegurado"),
              cpeTools.dateToString(rsPolicies.getDate("MesUltimoPago")),
              cpeTools.dateToString(rsPolicies.getDate("MesUltimoRa")),
              cpeTools.dateToString(rsPolicies.getDate("FechaConvMoneda")),
              cpeTools.dateToString(rsPolicies.getDate("FechaFinOpe")),
              rsPolicies.getString("FechaEstadoOpev"),
              rsPolicies.getString("FechaEstadoOpec"),
              cpeTools.dateToString(rsPolicies.getDate("InicioCredito")),
              cpeTools.formatNumber(rsPolicies.getBigDecimal("PrimaCartPesos")),
              cpeTools.formatNumber(rsPolicies.getBigDecimal("PrimaCartUf")),
              cpeTools.formatNumber(rsPolicies.getBigDecimal("RasPrimaPeso")),
              rsPolicies.getString("RasEstado"),
              cpeTools.dateToString(rsPolicies.getDate("RasFechaConvMoneda")),
              rsPolicies.getString("PrimaCalculada"),
              cpeTools.formatNumber(rsPolicies.getBigDecimal("RasImpuestoPcalc")),
              cpeTools.formatNumber(rsPolicies.getBigDecimal("RasSobreprimaCalc")),
              rsPolicies.getString("PrimaPesos"),
              rsPolicies.getString("TipoProducto"),
              cpeTools.dateToString(rsPolicies.getDate("FechaBusqueda")),
              rsPolicies.getString("DataExtra1"),
              rsPolicies.getString("DataExtra2"),
              rsPolicies.getString("DataExtra3"),
              rsPolicies.getString("DataExtra4"),
              rsPolicies.getString("DataExtra5"),
              rsPolicies.getString("DataExtra6"),
              rsPolicies.getString("DataExtra7"),
              rsPolicies.getString("DataExtra8"),
              rsPolicies.getString("DataExtra9"),
              rsPolicies.getString("DataExtra10"));
      policyList.add(policy);
    }
    rsPolicies.close();
    return policyList;
  }
}
