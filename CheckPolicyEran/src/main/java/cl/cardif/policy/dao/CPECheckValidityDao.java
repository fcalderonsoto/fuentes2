package cl.cardif.policy.dao;

import cl.cardif.policy.domain.model.InputPolicy;
import cl.cardif.policy.domain.response.CheckPolicyEranRSP;
import cl.cardif.policy.exception.CPEException;

public interface CPECheckValidityDao {

  CheckPolicyEranRSP checkEnsureValidityEran(InputPolicy inputPolicy) throws CPEException;
}
